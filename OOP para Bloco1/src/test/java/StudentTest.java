import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void createValidStudent() {
        Student student = new Student(1190001, "Paulo");
        assertNotNull(student);
    }

    @Test
    void createStudentLongerNumberDigits() {
        assertThrows(IllegalArgumentException.class, () -> new Student(11900013, "Paulo Maio"));
    }

    @Test
    void createStudentNameIsEmpty()
    {
        assertThrows(IllegalArgumentException.class, ()
                -> new Student(1190001, ""));
    }

    @Test
    void createStudentNameIsFullOfSpaces() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1190001, " "));
    }


    @Test
    void createStudentInvalidValidNumberButInvalidName() {
        assertThrows(IllegalArgumentException.class, () -> new Student(1980398, "Bia"));
    }


    @Test
    void setNameValidCase(){

        String name = "Paulo";
        int number = 1111111;
        int grade = 12;

        Student s1 = new Student(name,number,grade);
        String result = s1.getName();
        assertEquals(name,result);
    }





    @Test
    void doEvaluation() {
        Student student = new Student(1190001, "Paulo");
        int expected = 18;
        boolean boolResult = student.doEvaluation(18);
        int result = student.getGrade();
        assertTrue(boolResult);
        assertEquals(expected, result);
    }

    @Test
    void compareByNumber() {
        Student student = new Student(1190001, "Paulo");
        Student other = new Student(1190002, "Vasco");
        int expected = -1;
        int result = student.compareByNumber(other);
        assertEquals(expected, result);
    }

    @Test
    void compareByGrade() {
        Student student = new Student(1190001, "Paulo");
        student.doEvaluation(11);
        Student other = new Student(1190002, "Vasco");
        other.doEvaluation(12);
        int expected = -1;
        int result = student.compareByGrade(other);
        assertEquals(expected, result);
    }





}