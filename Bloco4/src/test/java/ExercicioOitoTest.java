import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTest {

    @Test
    void nMultiplosComuns() {
        int inicio =1;
        int fim = 10;
        int [] multiplos = {2,3};
        int expected =1;
        int result = ExercicioOito.nMultiplosComuns(inicio,fim,multiplos);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosComunsTesteDois() {
        int inicio =1;
        int fim = -10;
        int [] multiplos = {2,3};
        int expected =0;
        int result = ExercicioOito.nMultiplosComuns(inicio,fim,multiplos);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosComunsTesteTres() {
        int inicio =1;
        int fim = 10;
        int [] multiplos = {0,3};
        int expected =0;
        int result = ExercicioOito.nMultiplosComuns(inicio,fim,multiplos);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosComunsTesteQuatro() {
        int inicio =13;
        int fim = 1;
        int [] multiplos = {2,3};
        int expected =2;
        int result = ExercicioOito.nMultiplosComuns(inicio,fim,multiplos);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosComunsCinco() {
        int inicio =1;
        int fim = 5;
        int [] multiplos = {2,3};
        int expected =0;
        int result = ExercicioOito.nMultiplosComuns(inicio,fim,multiplos);
        assertEquals(expected,result);

    }


    @Test
    void multiplosComuns() {
        int inicio =1;
        int fim = 12;
        int [] multiplos = {2,3};
        int [] expected = {6,12};
        int [] result = ExercicioOito.multiplosComuns(inicio,fim,multiplos);
        assertArrayEquals(expected,result);

    }


    @Test
    void multiplosComunsTesteDois() {
        int inicio =1;
        int fim = 12;
        int [] multiplos = {0,3};
        int [] expected = {};
        int [] result = ExercicioOito.multiplosComuns(inicio,fim,multiplos);
        assertArrayEquals(expected,result);

    }


    @Test
    void multiplosComunsTesteTres() {
        int inicio =1;
        int fim = 12;
        int [] multiplos = {11,5};
        int [] expected = {};
        int [] result = ExercicioOito.multiplosComuns(inicio,fim,multiplos);
        assertArrayEquals(expected,result);

    }

    @Test
    void multiplosComunsTesteQuatro() {
        int inicio =5;
        int fim = 13;
        int [] multiplos = {2,3};
        int [] expected = {6,12};
        int [] result = ExercicioOito.multiplosComuns(inicio,fim,multiplos);
        assertArrayEquals(expected,result);

    }











}