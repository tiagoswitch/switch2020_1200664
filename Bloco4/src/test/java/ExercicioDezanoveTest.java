import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezanoveTest {

    @Test
    void validarAMatrizDeJogoNovePorNoveTesteParaNull() {
        int[][] m = null;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarAMatrizDeJogoNovePorNove(m);
        assertEquals(expected, result);

    }

    @Test
    void validarAMatrizDeJogoNovePorNoveTesteDoisTesteParaVazio() {
        int[][] m = {{}};
        boolean expected = false;
        boolean result = ExercicioDezanove.validarAMatrizDeJogoNovePorNove(m);
        assertEquals(expected, result);

    }

    @Test
    void validarAMatrizDeJogoNovePorNoveTesteDoisTesteDois() {
        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};
        boolean expected = false;
        boolean result = ExercicioDezanove.validarAMatrizDeJogoNovePorNove(m);
        assertEquals(expected, result);

    }

    @Test
    void validarAMatrizDeJogoNovePorNoveTesteDoisTesteTres() {
        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        boolean expected = true;
        boolean result = ExercicioDezanove.validarAMatrizDeJogoNovePorNove(m);
        assertEquals(expected, result);
    }


    @Test
    void obterMatrizMascaraInicialTesteCasoNull() {
        int[][] m = null;
        int[][] expected = null;
        int[][] result = ExercicioDezanove.obterMatrizMascaraInicial(m);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizMascaraInicialTesteCasoVazio() {
        int[][] m = {{}};
        int[][] expected = null;
        int[][] result = ExercicioDezanove.obterMatrizMascaraInicial(m);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizMascaraInicialTesteCasoDimInvalidas() {
        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};

        int[][] expected = null;
        int[][] result = ExercicioDezanove.obterMatrizMascaraInicial(m);
        assertEquals(expected, result);
    }


    @Test
    void obterMatrizMascaraInicialTesteUm() {
        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int[][] expected = {{1, 1, 0, 0, 1, 0, 0, 0, 0},
                {1, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {1, 0, 0, 1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 1, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0, 1, 1}};
        ;
        int[][] result = ExercicioDezanove.obterMatrizMascaraInicial(m);
        assertArrayEquals(expected, result);
    }


    @Test
    void verificarQueDifereDeZero() {
        int n = 0;
        boolean expected = false;
        boolean result = ExercicioDezanove.verificarQueDifereDeZero(n);
        assertEquals(expected, result);

    }

    @Test
    void verificarQueDifereDeZeroTesteDois() {
        int n = 10;
        boolean expected = true;
        boolean result = ExercicioDezanove.verificarQueDifereDeZero(n);
        assertEquals(expected, result);

    }


    @Test
    void foraDosLimitesDeJogoTesteCasoNull() {
        int[][] m1 = null;
        int linha = 0;
        int coluna = 0;
        boolean expected = true;
        boolean result = ExercicioDezanove.foraDosLimitesDeJogo(m1, linha, coluna);
        assertEquals(expected, result);

    }

    @Test
    void foraDosLimitesDeJogoTesteCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int linha = -1;
        int coluna = 0;
        boolean expected = true;
        boolean result = ExercicioDezanove.foraDosLimitesDeJogo(m1, linha, coluna);
        assertEquals(expected, result);

    }

    @Test
    void foraDosLimitesDeJogoTesteDoisCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int linha = 6;
        int coluna = 15;
        boolean expected = true;
        boolean result = ExercicioDezanove.foraDosLimitesDeJogo(m1, linha, coluna);
        assertEquals(expected, result);

    }

    @Test
    void foraDosLimitesDeJogoTesteTresCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int linha = 2;
        int coluna = 2;
        boolean expected = false;
        boolean result = ExercicioDezanove.foraDosLimitesDeJogo(m1, linha, coluna);
        assertEquals(expected, result);

    }

    @Test
    void matrizActualizadaTesteCasoNull() {
        int[][] m1 = null;
        int linha = -1;
        int coluna = 0;
        int numero = 7;

        int[][] expected = null;

        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertEquals(expected, result);

    }

    @Test
    void matrizActualizadaTesteCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};
        int linha = -1;
        int coluna = 0;
        int numero = 7;

        int[][] expected = null;

        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertEquals(expected, result);

    }

    @Test
    void matrizActualizadaTesteDoisCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};
        int linha = -1;
        int coluna = 0;
        int numero = 7;

        int[][] expected = null;

        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertEquals(expected, result);

    }


    @Test
    void matrizActualizadaTesteTresCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};
        int linha = 2;
        int coluna = -2;
        int numero = 7;

        int[][] expected = null;

        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertEquals(expected, result);

    }

    @Test
    void matrizActualizadaTesteQuatroCasoDimInvalidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5}};
        int linha = -1;
        int coluna = 0;
        int numero = 7;

        int[][] expected = null;

        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertEquals(expected, result);

    }


    @Test
    void matrizActualizadaTesteUmCasoDimValidas() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};


        int linha = 0;
        int coluna = 2;
        int numero = 4;

        int[][] expected = {{5, 3, 4, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};


        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertArrayEquals(expected, result);

    }

    @Test
    void matrizActualizadaTesteCasoNumRepetido() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};


        int linha = 0;
        int coluna = 2;
        int numero = 7;

        int[][] expected = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};


        int[][] result = ExercicioDezanove.matrizActualizada(m1, linha, coluna, numero);
        assertArrayEquals(expected, result);

    }


    @Test
    void verificarSeExistemEspacosPorPreencher() {
        int[][] m1 = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        boolean expected = true;
        boolean result = ExercicioDezanove.verificarSeExistemEspacosPorPreencher(m1);
        assertTrue(result);

    }

    @Test
    void verificarSeExistemEspacosPorPreencherTesteDois() {
        int[][] m1 = {{5, 3, 6, 1, 7, 2, 8, 2, 3},
                {6, 4, 5, 1, 9, 5, 3, 8, 6},
                {1, 9, 8, 7, 4, 8, 2, 6, 5},
                {5, 5, 5, 5, 6, 5, 5, 5, 3},
                {4, 5, 7, 8, 6, 3, 7, 5, 1},
                {7, 3, 3, 3, 2, 5, 5, 4, 6},
                {3, 6, 3, 6, 6, 6, 2, 8, 6},
                {3, 3, 6, 4, 1, 9, 6, 6, 5},
                {4, 2, 4, 5, 8, 5, 4, 7, 9}};

        boolean expected = false;
        boolean result = ExercicioDezanove.verificarSeExistemEspacosPorPreencher(m1);
        assertEquals(expected, result);

    }


    @Test
    void validaJogadaTesteUmCasoDimensosDaMatrizInvalidas() {

        int[][] m =
                {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                        {0, 9, 8, 0, 0, 0, 0, 6, 0},
                        {8, 0, 0, 0, 6, 0, 0, 0, 3},
                        {4, 0, 0, 8, 0, 3, 0, 0, 1},
                        {7, 0, 0, 0, 2, 0, 0, 0, 6},
                        {0, 6, 0, 0, 0, 0, 2, 8, 0},
                        {0, 0, 0, 4, 1, 9, 0, 0, 5},
                        {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int linha = 1;
        int coluna = 2;
        int numero = 4;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }

    @Test
    void validarJogadaTesteDoisCasoDimensoesDaMatrizInvalidas() {

        int[][] m = null;

        int linha = 1;
        int coluna = 2;
        int numero = 4;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }

    @Test
    void validarJogadaTesteUmCasoDimDaLinhaInvalida() {

        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int linha = 19;
        int coluna = 2;
        int numero = 4;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }


    @Test
    void validarJogadaTesteUmCasoDimDaColunaInvalida() {

        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int linha = 1;
        int coluna = -5;
        int numero = 4;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }

    @Test
    void validarJogadaTesteUmCasoNumInvalido() {

        int[][] m =
                {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                        {6, 0, 0, 1, 9, 5, 0, 0, 0},
                        {0, 9, 8, 0, 0, 0, 0, 6, 0},
                        {8, 0, 0, 0, 6, 0, 0, 0, 3},
                        {4, 0, 0, 8, 0, 3, 0, 0, 1},
                        {7, 0, 0, 0, 2, 0, 0, 0, 6},
                        {0, 6, 0, 0, 0, 0, 2, 8, 0},
                        {0, 0, 0, 4, 1, 9, 0, 0, 5},
                        {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int linha = 1;
        int coluna = 2;
        int numero = -5;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }


    @Test
    void validarJogadaTesteDoisCasoNumInvalido() {

        int[][] m =
                {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                        {6, 0, 0, 1, 9, 5, 0, 0, 0},
                        {0, 9, 8, 0, 0, 0, 0, 6, 0},
                        {8, 0, 0, 0, 6, 0, 0, 0, 3},
                        {4, 0, 0, 8, 0, 3, 0, 0, 1},
                        {7, 0, 0, 0, 2, 0, 0, 0, 6},
                        {0, 6, 0, 0, 0, 0, 2, 8, 0},
                        {0, 0, 0, 4, 1, 9, 0, 0, 5},
                        {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int linha = 1;
        int coluna = 2;
        int numero = 10;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }

    @Test
    void validarJogadaTesteQuatroCasoNumInvalido() {

        int[][] m = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        int linha = 0;
        int coluna = 3;
        int numero = 7;
        boolean expected = false;
        boolean result = ExercicioDezanove.validarJogada(m, linha, coluna, numero);
        assertEquals(expected, result);
    }


}