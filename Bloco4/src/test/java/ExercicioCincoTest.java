import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {


    @Test
    void somaDosElemPares() {
        int n = 36897;
        int expected = 14;
        int result = ExercicioCinco.somaDosElemPares(n);
        assertEquals(expected,result);

    }

    @Test
    void somaDosElemParesTesteDois() {
        int n = 397;
        int expected = 0;
        int result = ExercicioCinco.somaDosElemPares(n);
        assertEquals(expected,result);

    }

    @Test
    void somaDosElemParesTesteTres() {
        int n = 3897;
        int expected = 8;
        int result = ExercicioCinco.somaDosElemPares(n);
        assertEquals(expected,result);

    }


    @Test
    void somaDosElemParesTesteQuatro() {
        int n = -123;
        int expected = -1;
        int result = ExercicioCinco.somaDosElemPares(n);
        assertEquals(expected,result);

    }



    @Test
    void somaDosElemImpares() {
        int n = 36897;
        int expected = 19;
        int result = ExercicioCinco.somaDosElemImpares(n);
        assertEquals(expected,result);

    }

    @Test
    void somaDosElemImparesTesteDois() {
        int n = 26888;
        int expected = 0;
        int result = ExercicioCinco.somaDosElemImpares(n);
        assertEquals(expected,result);

    }

    @Test
    void somaDosElemImparesTesteTres() {
        int n = -234;
        int expected = -1;
        int result = ExercicioCinco.somaDosElemImpares(n);
        assertEquals(expected,result);

    }






}