public class ExercicioDois {
    public static void main(String[] args) {
    }

    public static int[] devolveVetor(int n )
    {
        // Ver qual é o número de digitos para mais ser tarde ser o tamanho do array a devolver
        int comp = ExercicioUm.contarNumDigitos(n);
        int [] vector = new int[comp];

        for (int i = vector.length-1; i >=0 ; i--)
        {
            vector[i] = n%10;
            n= n/10;
        }
        return vector;
    }
}
