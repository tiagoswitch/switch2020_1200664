package com.company;

import java.util.Scanner;

/**
 *
 */
public class Bloco2 {


    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);

        //print_Digito(123);
        //distanciaEntrePontos(2,3,2,5);
        //classificacaoDoCubo(24);

        getHoras(7000);
        indicarPeriodoDoDia(7000);


        //exercicio7Main();

        classificarAprovados(0.79, 0.2, 0.4, 0.6, 0.8);

        //exercicio13Main();

        custoDeAluguer("C",1,0);



    }

    // metodo main com um switch case feito para executar as duas funções do exercício 7
    public static void exercicio7Main() {

        Scanner ler = new Scanner(System.in);

        System.out.println(" Insira a area a pintar : ");
        double area = ler.nextDouble();

        System.out.println(" Insira o salário diário que pretende oferecer : ");
        double salario = ler.nextDouble();

        System.out.println(" Insira o rendimento do pintor : ");
        int rendimentoPintor = ler.nextInt();

        System.out.println(" Insira o preço da tinta por L : ");
        double tintaL = ler.nextDouble();

        System.out.println(" Insira o rendimento da tinta : ");
        double rendimentoDaTinta = ler.nextDouble();


        System.out.println(" O que pretende calcular ?  ");
        System.out.println("-------------------------------- ");
        System.out.println(" 0 - Para o custo da mão de obra ");
        System.out.println(" 1 - Para o custo da tinta ");
        System.out.println(" 2- para o custo total ");
        int x;

        do {
            System.out.println("Insira a opcao que quer :");
            x = ler.nextInt();
        } while (x < 0 || x > 2);

        switch (x) {
            case 0: {
                double maoObra = maoDeObraPintores(area, salario, rendimentoPintor);
                System.out.println(" O custo da mão de obra é " + maoObra);
                break;
            }
            case 1: {
                double ct = custoTinta(area, tintaL, rendimentoDaTinta);
                System.out.println(" O custo da tinta é " + ct);
                break;
            }
            case 2: {
                double maoObra = maoDeObraPintores(area, salario, rendimentoPintor);
                double ct = custoTinta(area, tintaL, rendimentoDaTinta);

                double custo = maoObra + ct;
                System.out.println("O custo total é :" + custo);
                break;
            }
            default:
                System.out.println(" Operação inválida ");
                break;
        }


    }

    // metodo main para executar e testar manualmente as duas funções do exercício 13
    public static void exercicio13Main()
    {
        Scanner ler = new Scanner(System.in);

        System.out.println(" Insira area de grama que pretende: ");
        double aGrama = ler.nextDouble();

        System.out.println(" Insira número de arvores que pretende :");
        double nArvores = ler.nextDouble();

        System.out.println(" Insira número de Arbustos que pretende :");
        double nArbustos = ler.nextDouble();

        double horasTrabalho = getTempoJardinagem(aGrama,nArvores,nArbustos);
        double custoFinal = getCustoJardinagem(aGrama,nArvores,nArbustos);

        System.out.println(" A jardinagem necessita de " +horasTrabalho+ "horas de trabalho e custa cerca de " +custoFinal+ " euros !" );



    }


    // Exercício 1
    public static double mediaPesada(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        double resultado = (double) (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
        return resultado;

    }

    // Exercício 2
    public static String printDigito(int numero) {
        int digito1, digito2, digito3;

        if (numero < 100 || numero > 999) {
            return "O número " + numero + " não tem 3 digitos";
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if (numero % 2 == 0) {
                return "O número " + numero + "possui os digitos " + (digito1 + " " + digito2 + " " + digito3) + " e é par";
            } else {

                return "O número " + numero + "possui os digitos " + (digito1 + " " + digito2 + " " + digito3) + " e é impar";
            }

        }

    }

    public static int printDigito1(int numero) {
        int digito1 = (numero / 100) % 10;
        return digito1;

    }

    public static int printDigito2(int numero) {
        int digito2 = (numero / 10) % 10;
        return digito2;

    }

    public static int printDigito3(int numero) {
        int digito3 = numero % 10;
        return digito3;

    }

    // Exercício 3
    public static double distanciaEntrePontos(double x1, double x2, double y1, double y2) {

        double distancia = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        return distancia;

    }

    // Exercício 4
    public static double CalculoValores(double x) {
        if (x < 0) {
            return x;
        } else if (x == 0) {
            return 0;
        } else {
            return Math.pow(x, 2) - 2 * x;
        }

    }

    // Exercicio 5
    public static double areaParaVolumeEmCm2(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6);
            double volume = Math.pow(aresta, 3);

            return volume;


        } else {
            return -1;
        }

    }

    public static String classificacaoDoCubo(double volume) {


        if (volume <= 1) return " Classifica - se como Pequeno";
        else if (volume > 1 && volume <= 2) return " Classifica -se como Medio";
        else return " Classifica -se como Grande";

    }

    // Exercício 6
    // Uma função que obtem horas através as horas
    // e outra que diz qual é a altura do dia
    public static String getHoras(double totalSec) {

        double segundos;
        double minutos;
        double hora;
        String horasFinais;

        segundos = totalSec % 60;

        if ((totalSec / 60) > 60) {
            minutos = ((totalSec / 60) % 60);
            hora = (int) ((totalSec / 60) / 60);
        } else if ((totalSec / 60) == 60) {
            minutos = 0;
            hora = 1;
        } else {
            minutos = (int) (totalSec / 60);
            hora = 0;
        }
        horasFinais = String.format("%.0f", hora) + ":" + String.format("%.0f", minutos) + ":" + String.format("%.0f", segundos);
        return horasFinais;

    }


    public static String indicarPeriodoDoDia(double totalSegundos) {
        if (totalSegundos > 0 && totalSegundos < 86400) {

            if (totalSegundos >= 21600 && totalSegundos < 43201) {
                return "Bom dia!";
            } else if (totalSegundos > 43201 && totalSegundos < 72001) {
                return "Boa tarde!";
            } else if ((totalSegundos > 0 && totalSegundos < 21600) || (totalSegundos > 72001 && totalSegundos < 86400)) {
                return "Boa noite!";
            } else {
                return "";
            }

        } else return "Total de segundos nao valido";

    }


    // Exercício 7
    // Duas funções uma para a mão de obra e outra para a tinta
    // Existe um metodo void para executar as 2 funções
    public static double maoDeObraPintores(double areaDeTrabalhos, double salarioDiario, int rendimentoPorDia) {
        double nPintores = 0;
        rendimentoPorDia = 8 * 2;
        if (areaDeTrabalhos < 0) nPintores = 0;
        else {
            if (areaDeTrabalhos >= 0 && areaDeTrabalhos <= 100) {
                nPintores = 1;
            } else if (areaDeTrabalhos > 100 && areaDeTrabalhos <= 300) {
                nPintores = 2;
            } else if (areaDeTrabalhos > 300 && areaDeTrabalhos <= 1000) {
                nPintores = 3;
            } else nPintores = 4;
        }
        return Math.ceil(areaDeTrabalhos / (nPintores * rendimentoPorDia)) * salarioDiario;

    }

    public static double custoTinta(double area, double custoTintaL, double rendimentoTinta) {
        double custo = Math.ceil((area / rendimentoTinta)) * custoTintaL;
        return custo;
    }

    // Exercício 8
    public static String multiplosOuDivisores(double x, double y) {
        if (x == 0 || y == 0) return "operação invalida !";
        else {
            if (x == y) return "x é multiplo e divisor de y";
            else if (x % y == 0) return "x é multiplo de y";
            else if (y % x == 0) return "y é multiplo de x";
            else return "x não é multiplo nem divisor y";
        }

    }

    // Exercício 9
    // Utilizar as funções de imprimir digitos
    public static String verificarSeECrescente(int numero) {
        int digitoCentenas = 0;
        int digitoDezenas = 0;
        int digitoUnidades = 0;

        digitoCentenas = printDigito1(numero);
        digitoDezenas = printDigito2(numero);
        digitoUnidades = printDigito3(numero);

        if (digitoCentenas < digitoDezenas && digitoDezenas < digitoUnidades) return "A sequencia é crescente";
        else return "A sequencia não é crescente";
    }

    // Exercício 10
    public static double precoDeSaldoDeArtigo(double preco) {
        double precoFinal = 0;
        if (preco <= 0) return precoFinal = 0;
        else {
            if (preco > 200) {
                precoFinal = preco - preco * 0.60;
            } else if (preco > 100) {
                precoFinal = preco - preco * 0.4;
            } else if (preco > 50) {
                precoFinal = preco - preco * 0.3;
            } else precoFinal = preco - preco * 0.2;

            return precoFinal;

        }

    }


    // Exercício 11
    public static String classificarAprovados(double pAprovados, double turmaMa, double turmaFraca, double turmaRazoavel, double turmaBoa) {
        String resultado = "";

        if (pAprovados < 0 || pAprovados > 1) {
            resultado = "Valor Inválido!";
            return resultado;
        } else if (pAprovados < turmaMa) {
            resultado = "turma má";
            return resultado;
        } else if (pAprovados < turmaFraca) {
            resultado = "turma fraca";
            return resultado;
        } else if (pAprovados < turmaRazoavel) {
            resultado = "turma razoável";
            return resultado;
        } else if (pAprovados < turmaBoa) {
            resultado = "turma boa";
            return resultado;
        } else {
            resultado = "turma excelente";
            return resultado;
        }
    }


    //Exercício 12

    public static double emiteIndicePoluicao(double indice)
    {
        int nivel = 0;
        if(indice < 0) return nivel = -1;
        else
            {
                if (indice <= 0.3) nivel = 1;
                else if(indice <= 0.4) nivel =2;
                else if(indice <= 0.5) nivel = 3;
                else nivel = 4;
            }

        return nivel;

    }

    public static String imprimeAvisoComBaseNoNivel(double nivel)
    {
        String aviso = "";
        if(nivel <=0) aviso = "Indices têm que ser maior que zero !";
        else if(nivel <0.3 ) aviso ="É aceitável";
        else if( nivel <0.4) aviso = " As indústrias do 1º grupo são intimadas a suspenderem as suas atividades";
        else if(nivel < 0.5) aviso = " As indústrias do 1º e 2º grupo são intimadas a suspenderem as suas atividades";
        else aviso= "Os 3 grupos devem ser notificados a paralisarem as suas atividades.";

        return aviso;
        
    }


    // Exercicio 13
    // Uma função para saber o tempo necessário para executar as tarefas e outra para calcular o custo
    public static double getTempoJardinagem(double areaGrama, double nArvores, double nArbustos)
    {
        double tempoTotalEmS = areaGrama * 300 + nArvores* 600 + nArbustos*400;
        double hTrabalho = Math.ceil(tempoTotalEmS/ 3600);
        return hTrabalho;
    }

    public static double getCustoJardinagem(double areaGrama, double nArvores, double nArbustos)
    {
        double horasTrabalho =0;
        horasTrabalho = getTempoJardinagem(areaGrama, nArvores,nArbustos);
        double custoTotal = areaGrama * 10 + nArvores*20 + nArbustos* 15 + horasTrabalho*10;

        return custoTotal;

    }



    // Exercício 14
    public static double getMediaPercorrida(double distDia1, double distDia2, double distDia3, double distDia4, double distDia5)
    {
        double mediaEmMilhas = (distDia1 + distDia2 + distDia3 +distDia4 +distDia5) /5;
        double mediaEmKm = mediaEmMilhas * 1.609;

        return mediaEmKm;

    }


    // Exercicio 15

    public static String classificacaoDeTriangulosPorLado (double a, double b, double c)
    {
        String resultado ="";
        if( a<=0 || b<=0 || c<= 0 || a + b <= c  || b + c <= a || a+ c <= b ) resultado = "Não é triangulo";
        else
        {
            if(a == b && b== c) resultado= "O triangulo é equilátero";
            else if(a !=b && b!=c && a!=c) resultado = "O triangulo é escaleno";
            else resultado = "O triangulo é isósceles";

        }
        return resultado;


    }

    // Exercício 16

    public static String classificacaoDeTriangulosPorAngulos(double a, double b, double c)
    {
        String resultado = "";
        if(a + b + c !=180 || a<= 0 || b <= 0 || c<=0) resultado = "Não é triangulo";
        else {
            if(a == 90 || b == 90 || c ==90) resultado = "O triangulo é retangulo";
            else if(a <90 && b<90 && c<90) resultado = "O triangulo é acutangulo";
            else resultado= "O triangulo é obtusangulo";
        }
        return resultado;
    }


    // exercicio 17
    public static String getHoraChegada(int horaPartida, int minutosPartida, int horaDuracao, int minutosDuracao) {
        String dia = "";
        int chegada = horaPartida * 60 + horaDuracao * 60 + minutosDuracao;
        int hc = chegada / 60;
        int mc = chegada % 60;

        if (hc >= 24) {
            hc = hc - 24;
            dia = "amanha";

        } else dia = "hoje";

        return "Chegada ás " + hc + ":" + mc + " do dia de " + dia;
    }


    // Exercício 18

    public static String getHoraFimProcessamento(int hi, int mi, int si,int sproc)
    {
        int tempoInicial = hi*3600 + mi*60 +si;
        int tempoFinal = tempoInicial + sproc;

        int hf = tempoFinal / 3600;
        int mf = (tempoFinal % 3600) / 60;
        int sf = (tempoFinal % 3600) %60;

        return hf+":"+mf+":"+sf;

    }


    //Exercicio 19

    public static double getSalarioSemanal(int horas)
    {
        double pagamento =0;
        if(horas<0) pagamento=0;
        else if(horas <= 36) pagamento = horas * 7.5;
        else {
                int horasExtras= horas -36;

                if(horasExtras <= 5) pagamento = 36*7.5 + horasExtras * 10;
                else
                {
                    int horasExtraBonus = horas-41;
                    int horasExtra= horasExtras-horasExtraBonus;
                    pagamento = 36 * 7.5 + horasExtra * 10 + horasExtraBonus * 15;
                }

            }

        return pagamento;
    }


    // Exercício 20

    public static String custoDeAluguer(String tipoDeKit, double diaUtil, double deslocacaoEmKm)
    {

        String resultado ="";
        int custo =0;

        if((diaUtil==0 || diaUtil == 1) && ( tipoDeKit =="a" || tipoDeKit =="b"|| tipoDeKit =="c"|| tipoDeKit =="A" || tipoDeKit =="B" || tipoDeKit =="C"))
        {
            if ((tipoDeKit == "a" || tipoDeKit=="A" )&& diaUtil == 0) custo = (int) (30 + 2 * deslocacaoEmKm);
            else if ((tipoDeKit == "a" || tipoDeKit =="A")&& diaUtil == 1) custo = (int) (40 + 2 * deslocacaoEmKm);
            else if ((tipoDeKit == "b" || tipoDeKit =="B") && diaUtil == 0) custo = (int) (50 + 2 * deslocacaoEmKm);
            else if ((tipoDeKit == "b" || tipoDeKit == "B")&& diaUtil == 1) custo = (int) (70 + 2 * deslocacaoEmKm);
            else if ((tipoDeKit == "c" || tipoDeKit == "C") && diaUtil == 0) custo = (int) (100 + 2 * deslocacaoEmKm);
            else if ((tipoDeKit == "c"  || tipoDeKit == "C")&& diaUtil == 1) custo = (int) (140 + 2 * deslocacaoEmKm);
            else custo = -1;

            resultado = "O custo é de " +custo+" €";

        }
        else {

            resultado ="Parametros de entrada, dia util ou tipo de kit, não valido";

        }

        return resultado;
    }

}
