package com.company;

public class Bloco3Ex3 {

    public static void main(String[] args) {

        int[] numeros = {10, 9, 8, 7, 5,0};
        double s = getPercentagemPares(numeros);
        double m = getMediaImpares(numeros);
        System.out.println(s);
        System.out.println(m);


    }


    public static double getPercentagemPares(int[] numeros) {
        int nPares = 0;
        double percentagem = 0;
        int i = 0;
        while (numeros[i] > 0) {
            if (numeros[i] % 2 == 0) {
                double r = numeros[i];
                nPares++;
            }
            i++;
            percentagem = (double)nPares/i*100;
        }

        return percentagem;
    }

    public static double getMediaImpares(int [] numeros)
    {
        int nImpares = 0;
        double media = 0;
        double soma =0;
        int i = 0;
        while (numeros[i] > 0) {
            if (numeros[i] % 2 != 0) {
                double r = numeros[i];
                soma +=r;
                nImpares++;
            }
            i++;
            media = (double)soma/nImpares;
        }
        return media;

    }



}
