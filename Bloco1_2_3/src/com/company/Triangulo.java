package com.company;

public class Triangulo {

    public int cateto1=1;
    public int cateto2=1;

    public  Triangulo(int cateto1, int cateto2)
    {
        this.cateto1= cateto1;
        this.cateto2= cateto2;

    }
    public double getHipotenusa()
    {
        return Math.sqrt(Math.pow(cateto1,2) + Math.pow(cateto2,2));
    }
}
