public class ExercicioQuinze<v> {

    public static void main(String[] args) {

        int [][] v1 = {{1,1,1,1},{1,2,-1,2},{1,-1,2,1},{1,3,3,2}};
        int [][] v11 = {{1,2,3,4},{2,3,7,5},{7,8,9,10},{3,13,9,15}};
        int [][] v22= {{1,2,3},{4,5,6},{7,8,9}};



        int d= obterDeterminante(v1);
        double [][]v3 = obterMatrizInversa(v1);

        System.out.println(d);
        for (int i = 0; i <v3.length ; i++) {
            System.out.println();
            for (int j = 0; j <v3[i].length ; j++) {
                System.out.print(" " +v3[i][j]);
            }
        }


    }
    // Alínea a) retorna o menor valor de uma matriz
    public static int obterElemMenorValor(int[][] v) {
        if (v.length == 0) {
            return -1;
        }
        int menor = v[0][0];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (v[i][j] < menor) {
                    menor = v[i][j];
                }
            }
        }
        return menor;
    }

    // Alínea b) retorna o maior valor de uma matriz
    public static int obterElemMaiorValor(int[][] v) {
        if (v.length == 0) {
            return -1;
        }
        int maior = v[0][0];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {

                if (v[i][j] > maior) {
                    maior = v[i][j];
                }
            }
        }
        return maior;
    }

    // Alínea c) retorna o valor médio de uma matriz
    public static double obterValorMedio(int[][] v) {
        if (v.length == 0) {
            return 0;
        }
        double soma = 0;
        int cont = 0;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                soma = soma + v[i][j];
                cont++;
            }
        }
        return soma / cont;
    }

    // Alínea d) retorna o produto dos elementos de uma matriz
    public static int obterprodutoDosElem(int[][] v) {
        if (v.length == 0) {
            return -1;
        }
        int produto = 1;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                produto = produto * v[i][j];
            }
        }
        return produto;
    }


    // Método para ver se é válida
    public static boolean isValidMatrix(double[][] v) {
        boolean resultado = false;
        int width = v[0].length;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (v.length == 0 || v[0].length == 0) {
                    return false;
                } else if (v[i].length != width) {
                    return false;
                } else {
                    resultado = true;
                }

            }

        }
        return resultado;
    }


    // Método igual ao de cima só que pede matrizes com valores inteiros
    public static boolean eMatrizValida(int[][] v) {
        boolean resultado = false;
        if(v== null)
        {
            return false;
        }
        int width = v[0].length;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (v.length == 0 || v[0].length == 0) {
                    return false;
                } else if (v[i].length != width) {
                    return false;
                } else {
                    resultado = true;
                }

            }

        }
        return resultado;
    }



    public static boolean eMatrizQuadrada(int[][] v) {
        if (!(eMatrizValida(v))) {
            return false;

        } else if (v.length != v[0].length) {
            return false;
        } else {
            return true;
        }
    }

    public static int[] obterElemNaoRepetidos(int[][] v) {
        if (!(eMatrizValida(v))) {
            return null;
        }
        int[] conj = new int[v.length * v[0].length];
        int k = 0; // iterador para o novo vector
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                conj[k] = v[i][j];
                k++;
            }
        }
        return ExercicioDez.obterValoresNaoRepetidos(conj);
    }


    /*
    Alínea f) obter um array com os elementos primos de uma matriz
    1º - Ver se a matriz é válida
    2º- Usar o método do ex10 para ver se é primo
    3º - devolver o array só com os elementos que são primos

     */

    public static int[] obterElemPrimos(int[][] v) {
        // Ver se é válida a matriz
        if (!(eMatrizValida(v))) {
            return null;
        }
        // Novo array que vai ficar com todos os elementos que são primos
        // Tem que ter dim maxima de
        int[] arrayPrimos = new int[v.length * v[0].length];
        int k = 0;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (ExercicioDez.ePrimo(v[i][j])) // Utilizar o metodo boolean que verifica se um número é primo
                {
                    arrayPrimos[k] = v[i][j];
                    k++;
                }
            }
        }
        return ExercicioSeis.copiarNelem(arrayPrimos, k);
    }

    // Alínea g) Devolver a diagonal principal
    public static int[] obterDiagonalPrincipalDaMatriz(int[][] v) {
        if (!(eMatrizValida(v))) {
            return null;
        }
        int k = 0;
        int[] diagonalMatriz = new int[v[0].length];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (i == j) {
                    diagonalMatriz[k] = v[i][j];
                    k++;
                }
            }
        }
        return diagonalMatriz;
    }

    // Alínea h) Devolver a Diagonal Secundária
    public static int[] obterDiagonalSecundariaDaMatriz(int[][] v) {
        if (!(eMatrizValida(v))) {
            return null;
        }
        int k = 0;
        int[] diagonalSecundariaMatriz = new int[v[0].length];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if ((i + j) == ((v[0].length) - 1)) {
                    diagonalSecundariaMatriz[k] = v[i][j];
                    k++;
                }
            }
        }
        return diagonalSecundariaMatriz;
    }

    /*
    Alínea i) Ver a matriz é identidade

    Utilizar a função ver se é válida e utilizar a função para ver se a matriz é quadrada
    Depois ver se em i= j só aparecem 1s
    e nos restantes 0

     */

    public static boolean eMatrizIdentidade(int[][] v) {
        boolean resultado = false;
        if (!(eMatrizValida(v))) {
            return false;
        } else if (!(eMatrizQuadrada(v)) || v.length == 0) {
            return false;
        } else {
            for (int i = 0; i < v.length; i++) {
                for (int j = 0; j < v[i].length; j++) {
                    if (i == j) {
                        if (v[i][j] == 1) resultado = true;
                        else return false;
                    } else if (i != j) {

                        if (v[i][j] == 0) resultado = true;
                        else return false;
                    } else resultado = false;
                }
            }
        }
        return resultado;
    }

    /*
    Alínea j) obter a matriz inversa

    Primeiro calcular o determinante
    E depois a adjuinta
     */

    /**
     * metodo obterMatrizDosMenores
     * @param v
     * @param linha
     * @param coluna
     * @return RETORNA UMA MATRIZ COM TODOS OS ELEMENTOS MENOS OS SELECCIONADOS
     */

    public static int[][] obterMatrizDosMenores(int[][] v, int linha, int coluna) {
        if(v== null||!(eMatrizValida(v)) || v.length <=1 )
        {
            return null;
        }
        int [][] temp = new int[v.length - 1][v.length - 1];
        int indLinha = 0; // índice da linha
        int indColuna = 0; //índice da coluna
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                //  Colocar na temporária os valores excepto os que são pedidos
                if (i != linha && j != coluna) {
                    temp[indLinha][indColuna++] = v[i][j];
                    // A linha está cheia
                    // limpa -se o valor da coluna
                    if (indColuna == v.length - 1) {
                        indColuna = 0;
                        indLinha++;
                    }
                }
            }
        }
        return temp;
    }


    public static Integer obterDeterminante(int[][] v) {
        if (!(eMatrizQuadrada(v))) return null;
        else if(v.length==1)
        {
            return v[0][0];
        }
        else if (v.length == 2)
        {
            return v[0][0] * v[1][1] - v[0][1] * v[1][0];
        }
        else {
            double determinante = 0;
            for (int i = 0; i < v[0].length; i++)
                determinante += Math.pow(-1, i) * v[0][i]
                        * obterDeterminante(obterMatrizDosMenores(v, 0, i));
            return(int) determinante;
        }
    }

    /**
     *
     * @param v
     * @return
     */

    public static int[][] obterMatrizAdjunta(int[][] v) {
        int[][] resultado;
        if (!(eMatrizQuadrada(v))) // Se não for quadrada não é válida
        {
            return null;
        } else if (v.length == 1) {
            resultado = new int[1][1];
            resultado[0][0] = 1;
            return resultado;
        } else {
            resultado = new int[v.length][v[0].length];
            for (int i = 0; i < v.length; i++) {
                for (int j = 0; j < v[i].length; j++) {
                    // Agora obter a matriz dos coeficientes
                    int[][] temp = obterMatrizDosMenores(v, i, j);
                    double sinal = Math.pow(-1, (i + j));
                    // O sinal do adj[j][i] é posivivo quando i+j é par
                    resultado[i][j] = (int) (sinal * obterDeterminante(temp));
                }
            }
        }
        return resultado;
    }

    public static double arredondarNumero(double n, int casasDecimas)
    {
        double resultado;
        resultado = (double) (Math.round(n*Math.pow(10,casasDecimas)) / (Math.pow(10,casasDecimas)));
        return resultado;

    }



    public static double [][] obterMatrizInversa(int [][]v) {
        if(!(eMatrizQuadrada(v)) || v.length==0)
        {
            return null;
        }
        else {
            int determinante = obterDeterminante(v);
            if(determinante==0){
                return null;
            }
            int[][] temp = obterMatrizAdjunta(v);
            int [][]tempTransposta = obterMatrizTransposta(temp);
            double[][] inversa = new double[v.length][v[0].length];
            for (int i = 0; i < v.length; i++) {
                for (int j = 0; j < v[i].length; j++) {
                    inversa[i][j] =arredondarNumero((float)(tempTransposta[i][j])/(float) (determinante),2);

                }
            }
            return inversa;
        }
    }

    // Calcular a matriz transposta
    public static int[][] obterMatrizTransposta(int[][] v) {
        if(v == null || !(eMatrizValida(v))|| v.length==0)
        {
            return null;
        }
        int[][] transposta = new int[v[0].length][v.length];
        for (int i = 0; i < v.length; i++)
            for (int j = 0; j < v[i].length; j++)
                transposta[j][i] = v[i][j];
        return transposta;
    }




}


































