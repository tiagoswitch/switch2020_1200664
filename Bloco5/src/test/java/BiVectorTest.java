import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BiVectorTest {


    @Test
    void TestNullCaseOnBiVector() {
        int[][] matrix = null;
        assertThrows(NullPointerException.class, () -> {
            BiVector v1 = new BiVector(matrix);
        });
    }

    @Test
    void addElemToBiVectorInvalidRowCase() {

        int [][] matrix ={{1,2},{1,2}};
        int elem = 1;
        int row=10;
        BiVector result = new BiVector(matrix);
        assertThrows(IndexOutOfBoundsException.class, () -> {
           result.addElem(elem,row);
        });
    }


    @Test
    void addElemToBiVector() {

     int [][] matrix ={{1,2},{1,2}};
     int elem = 1;
     int row=1;
     BiVector result = new BiVector(matrix);
     result.addElem(elem,row);
     result.addElem(1,0);
     int [][] m1= {{1,2,1},{1,2,1}};
     BiVector expected = new BiVector(m1);
        assertArrayEquals(expected.toMatrix(), result.toMatrix());
    }


    @Test
    void removeElem() {
        int [][] r ={{1,2},{1,2}};
        BiVector result = new BiVector(r);
        int elem = 2;
        result.removeTheFirstElem(elem);
        int [][] exp= {{1,},{1,2}};
        BiVector expected = new BiVector(exp);
        assertArrayEquals(expected.toMatrix(), result.toMatrix());

    }

    @Test
    void removeANoneExistingElem() {
        int [][] r ={{1,2},{1,2}};
        BiVector result = new BiVector(r);
        int elem = 5;
        assertThrows(IllegalArgumentException.class, () -> {
            result.removeTheFirstElem(elem);
        });
    }

    @Test
    void removeElementFromAEmptyBiVector() {
        int [][] r ={{}};
        BiVector result = new BiVector(r);
        int elem = 5;
        assertThrows(IllegalArgumentException.class, () -> {
            result.removeTheFirstElem(elem);
        });
    }


    @Test
     void testToThrowIllegalArgumentIfEmptyMatrix() {
        int[][] matrix = {};
        BiVector givenMatrix = new BiVector(matrix);

        assertThrows(IllegalArgumentException.class, givenMatrix::getBiggestElemInBiVector);
    }

    @Test
    void getBiggestElemInBiVectorTestOne() {
        int [][] m = {{4,2},{3,6}};
        BiVector matrix = new BiVector(m);
        int expected=6;
        int result = matrix.getBiggestElemInBiVector();
        assertEquals(expected,result);
    }


    @Test
    void getSmallestElementInBiVectorTestOne() {
        int [][] m = {{4,2},{3,6}};
        BiVector matrix = new BiVector(m);
        int expected=2;
        int result = matrix.getSmallestElementInBiVector();
        assertEquals(expected,result);
    }


    @Test
    void getAvgFromAnEmptyMatrix() {

        int[][] matrix = {};
        BiVector givenMatrix = new BiVector(matrix);

        assertThrows(IllegalArgumentException.class, givenMatrix::getAvgFromMatrix);

    }


    @Test
    void getAvgFromMatrix() {

        int [][] m = {{1,2},{3,4}};
        BiVector matrix = new BiVector(m);
        double expected=2.5;
       double result = matrix.getAvgFromMatrix();
    }


    @Test
    void getInternalRowSumsFromAEmptyMatrix() {
        int[][] matrix = {};
        BiVector givenMatrix = new BiVector(matrix);

        assertThrows(IllegalArgumentException.class, givenMatrix::getInternalRowSums);

    }

    @Test
    void testgetInternalRowSums() {
        int[][] matrix = {{1,2,3},{1,2,3},{1,2,3}};
        BiVector givenMatrix = new BiVector(matrix);
        int[]expected = {6,6,6};
        Vector result = givenMatrix.getInternalRowSums();
        assertArrayEquals(expected,result.toArray());
    }

    @Test
    void getInternalColumnSums() {
        int[][] matrix = {};
        BiVector givenMatrix = new BiVector(matrix);
        assertThrows(IllegalArgumentException.class, givenMatrix::getInternalColumnSums);

    }

    @Test
    void testGetInternalColumnSums() {
        int[][] matrix = {{1,2},{1}};
        BiVector givenMatrix = new BiVector(matrix);
        int [] exp = {2,2};
        Vector expected = new Vector(exp);
        Vector result = givenMatrix.getInternalColumnSums();
        assertTrue(expected.equals(result));


    }

    @Test
    void testTwoGetInternalColumnSums() {
        int[][] matrix = {{1,2,3},{1,2,4},{1}};
        BiVector givenMatrix = new BiVector(matrix);
        int [] exp = {3,4,7};
        Vector expected = new Vector(exp);
        Vector result = givenMatrix.getInternalColumnSums();
        assertTrue(expected.equals(result));


    }


    @Test
    void getIndexFromAnEmptyMatrix() {

        int[][] matrix = {};
        BiVector givenMatrix = new BiVector(matrix);
        assertThrows(IllegalArgumentException.class, givenMatrix::getIndexWithTheBiggestSumRow);

    }


    @Test
    void getIndexWithTheBiggestSumRow() {

        int [][] m = {{1,2},{4,5,6},{7,8,9}};
        BiVector r = new BiVector(m);
        int result = r.getIndexWithTheBiggestSumRow();
        int expected = 2;
        assertEquals(result,expected);

    }


    @Test
    void testIfAnEmptyMatrixIsSquared() {

        int[][] m = {{}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.isSquaredMatrix();
        assertFalse(result);
    }

    @Test
    void testOneToReturnFalseIfNotSquareMatrix() {

        int[][] m = {{1,2},{1}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.isSquaredMatrix();
        assertFalse(result);
    }

    @Test
    void testTwoToReturnTrueIfSquareMatrix() {

        int[][] m = {{1,2},{1,2}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.isSquaredMatrix();
        assertTrue(result);
    }

    @Test
    void testOneReturnFalseIfEmptyMatrix() {

        int[][] m = {{}};
        BiVector r = new BiVector(m);
        boolean result = r.isSquaredAndSimetricMatrix();
        boolean expected = false;
        assertEquals(expected,result);
    }

    @Test
    void testTwoReturnFalseIfIsNotSquaredMatrix() {

        int[][] m = {{1,2,3},{1,2,3}};
        BiVector r = new BiVector(m);
        boolean result = r.isSquaredAndSimetricMatrix();
        boolean expected = false;
        assertEquals(expected,result);
    }

    @Test
    void testThreeReturnFalseIfIsNotASimetricMatrix() {

        int[][] m = {{1,2,3},{1,2,3},{1,2,3}};
        BiVector r = new BiVector(m);
        boolean result = r.isSquaredAndSimetricMatrix();
        boolean expected = false;
        assertEquals(expected,result);
    }

    @Test
    void testFourReturnTrueIfIsASquaredAndSimetricMatrix() {

        int[][] m = {{1,5,9},{5,3,8},{9,8,7}};
        BiVector r = new BiVector(m);
        boolean result = r.isSquaredAndSimetricMatrix();
        boolean expected = true;
        assertEquals(expected,result);
    }


    @Test
    void testOnecountNotNullElementsOnMainDiagonalOnANotSquaredMatrix() {
        int[][]m = {{1,2,3},{1,2,3}};
        BiVector matrix = new BiVector(m);
        int expected = -1;
        int result = matrix.countNotNullElementsOnMainDiagonal();
        assertEquals(expected,result);

    }

    @Test
    void testTwoCountNotNullElementsOnMainDiagonal() {
        int[][]m = {{1,2,3},{1,2,3},{1,2,3}};
        BiVector matrix = new BiVector(m);
        int expected = 3;
        int result = matrix.countNotNullElementsOnMainDiagonal();
        assertEquals(expected,result);
    }

    @Test
    void testOnecheckIfTheMainAndSecondDiagonalAreEqualFromAnEmptyArray() {

        int[][] m ={{}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.checkIfTheMainAndSecondDiagonalAreEqual();
        boolean expected = false;
        assertEquals(expected,result);
    }

    @Test
    void testTwoReturnFalseIfTheMainAndSecondDiagonalAreNotEqual() {

        int[][] m ={{1,2},{1,2}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.checkIfTheMainAndSecondDiagonalAreEqual();
        boolean expected = false;
        assertEquals(expected,result);
    }

    @Test
    void testThreeReturnTrueIfTheMainAndSecondDiagonalAreEqual() {

        int[][] m ={{1,2,1},{4,1,7},{1,9,1}};
        BiVector matrix = new BiVector(m);
        boolean result = matrix.checkIfTheMainAndSecondDiagonalAreEqual();
        boolean expected = true;
        assertEquals(expected,result);
    }


    @Test
    void TestOneGetElemFromAnEmptyMatrix() {
        int[][] matrix = {};
        BiVector result = new BiVector(matrix);
        assertThrows(IllegalArgumentException.class, () -> {
            result.getElemLargerThanAvgNumberOfDigits();
        });
    }

    @Test
    void TestTwoGetElemLargerThanAvgNumberOfDigits() {
        int[][] m = {{1,1,2},{1,1234567,3},{2,1,3,46789}};
        BiVector matrix = new BiVector(m);
        Vector result = matrix.getElemLargerThanAvgNumberOfDigits();
        int[] expected ={1234567,46789};
        assertArrayEquals(expected, result.toArray());
    }


    @Test
    void getElemWithlargerEvenPercentageFromAnEmptyMatrix() {
    int[][] m= {};
    BiVector result = new BiVector(m);
    assertThrows(IllegalArgumentException.class,() ->{
        result.getElemWithlargerEvenPercentageThanAvgEvenPercentagem();
    });
    }

    @Test
    void testEquals() {

        int[][] m1 = {{1,2,3},{4,5,6},{7,8,9}};
        int [][] m2 = {{1,2,3},{4,5,6},{7,8,10}};
        BiVector original = new BiVector(m1);
        BiVector copy = new BiVector(m2);
        assertFalse(original.equals(copy));
    }

    @Test
    void testOneGetElemWithlargerEvenPercentageThanAvgEvenPercentagem() {
        int[][] m1 = {{12,11,3466},{46,53,6},{72,71,95788}};
        BiVector matrix = new BiVector(m1);
        Vector result = matrix.getElemWithlargerEvenPercentageThanAvgEvenPercentagem();
        int [] expected = {12,3466,46,6,72};
        assertArrayEquals(expected, result.toArray());

    }


    @Test
    void reverseMatrixRowsFromAnEmptyMatrix() {
        int[][] m = {};
        BiVector result = new BiVector(m);
        assertThrows(IllegalArgumentException.class,() ->{
            result.reverseMatrixRows();
        });
    }

    @Test
    void testOnereverseMatrixRows() {
        int[][] m = {{1,3},{445},{7,8,9}};
        BiVector result = new BiVector(m);
        int[][] expected = {{3,1},{445},{9,8,7}};
        result.reverseMatrixRows();
        assertArrayEquals(expected, result.biVector);
    }


    @Test
    void testOnereverseMatrixColumnsFromAEmptyMatrix() {
        int[][] m = {};
        BiVector result = new BiVector(m);
        assertThrows(IllegalArgumentException.class, () -> {
            result.reverseMatrixColumns();
        });
    }

    @Test
    void testTwoTeverseMatrixColumns() {
        int[][] m = {{1,2},{2,35},{3,3}};
        BiVector result = new BiVector(m);
        int [][] expected = {{3,3},{2,35},{1,2}};
        result.reverseMatrixColumns();
        assertArrayEquals(expected,result.toMatrix());
    }


    @Test
    void testOneRotateAnEmptyMatrix() {
     int [][] m = {};
     BiVector result = new BiVector(m);
     assertThrows(IllegalArgumentException.class,() -> {
        result.rotateLeftNinetyDegrees();
     });

    }

    @Test
    void testTwoRotateRigthNinetyDegreesFromAnEmptyMatrix() {
        int[][] m ={};
        BiVector result = new BiVector(m);
        assertThrows(IllegalArgumentException.class,() -> {
           result.rotateRigthNinetyDegrees();
        });
    }

    @Test
    void testOneRotateHundredEigthy() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        BiVector matrix = new BiVector(biArray);
        int[][] expected = {
                {3465, 5, 334534},
                {24533, 6, -224444},
                {122, 344, 34}};
        matrix.rotateRigthHundredAndEigthyDegrees();
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void testOneRotateRigthNinetyDegrees() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        BiVector matrix = new BiVector(biArray);
        int[][] expected = {
                {334534, -224444, 34},
                {5, 6, 344},
                {3465, 24533, 122}};
        matrix.rotateRigthNinetyDegrees();
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void testOneRotateLeftNinetyDegrees() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        BiVector matrix = new BiVector(biArray);
        int[][] expected = {
                {122, 24533, 3465},
                {344, 6, 5},
                {34, -224444, 334534}};
        matrix.rotateLeftNinetyDegrees();
        assertArrayEquals(expected, matrix.toMatrix());
    }

}