import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void retornaComPares() {
        int [] a = {1,2,3,4,5,6,7,8};
        int [] expected ={2,4,6,8};
        int [] result = ExercicioQuatro.retonaOsPares(a);
        assertArrayEquals(expected,result);
    }

    @Test
    void retornaComParesTesteDois() {
        int [] a = {1,2,3,4,5};
        int [] expected ={2,4};
        int [] result = ExercicioQuatro.retonaOsPares(a);
        assertArrayEquals(expected,result);
    }

    @Test
    void retornaComParesTesteTres() {
        int [] a = {1,1,3,5,7};
        int [] expected ={};
        int [] result = ExercicioQuatro.retonaOsPares(a);
        assertArrayEquals(expected,result);
    }

    @Test
    void retornaComParesTesteQuatro() {
        int [] a = {};
        int [] expected ={};
        int [] result = ExercicioQuatro.retonaOsPares(a);
        assertArrayEquals(expected,result);
    }


    @Test
    void retornaComImpares() {
        int [] a = {1,2,3,4,5,6,7,8};
        int [] expected ={1,3,5,7};
        int [] result = ExercicioQuatro.retornaOsImpares(a);
        assertArrayEquals(expected,result);

    }

    @Test
    void retornaComImparesTesteDois() {
        int [] a = {1,2,3,4};
        int [] expected ={1,3};
        int [] result = ExercicioQuatro.retornaOsImpares(a);
        assertArrayEquals(expected,result);

    }

    @Test
    void retornaComImparesTesteTres() {
        int [] a = {6,2,4,4,4,6,2,8};
        int [] expected ={};
        int [] result = ExercicioQuatro.retornaOsImpares(a);
        assertArrayEquals(expected,result);

    }

    @Test
    void retornaComImparesTesteQuatro() {
        int [] a = {};
        int [] expected ={};
        int [] result = ExercicioQuatro.retornaOsImpares(a);
        assertArrayEquals(expected,result);
    }

}