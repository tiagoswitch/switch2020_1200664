import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteTest {

    @Test
    void obterMatrizMultPorConst() {

        double [][]m1 = {{1,2,3},{6,7,8},{6,5,4}};
        int c = 2;
        double [][] expected = {{2,4,6},{12,14,16},{12,10,8}};
        double [][] result = ExercicioDezassete.obterMatrizMultPorConst(m1,c);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterMatrizMultPorConstTesteDois() {

        double [][]m1 = {{1,2,3},{6,7,8,0},{6,5,4}};
        int c = 2;
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMatrizMultPorConst(m1,c);
        assertEquals(expected,result);

    }

    @Test
    void obterMatrizMultPorConstTesteTres() {

        double [][]m1 = {{1,2},{6,7}};
        int c = 5;
        double [][] expected = {{5.0,10.0},{30.0,35.0}};
        double [][] result = ExercicioDezassete.obterMatrizMultPorConst(m1,c);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterMatrizMultPorConstTesteQuatro() {

        double [][]m1 = {{1,2},{6,7}};
        int c = 0;
        double [][] expected = {{0,0},{0,0}};
        double [][] result = ExercicioDezassete.obterMatrizMultPorConst(m1,c);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterMatrizMultPorConstTesteCinco() {

        double [][]m1 = {{},{}};
        int c = 5;
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMatrizMultPorConst(m1,c);
        assertEquals(expected,result);

    }


    @Test
    void obterSomaDeMatrizes() {
        double [][] m1 = {{},{}};
        double [][]m2 = {{},{}};
        int [][] expected = null;
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterSomaDeMatrizesTesteDois() {
        double [][] m1 = {{1,2},{1}};
        double [][]m2 = {{1,2},{1,2}};
        int [][] expected = null;
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterSomaDeMatrizesTesteTres() {
        double [][] m1 = {{1,2},{1,2}};
        double [][]m2 = {{1,2,3},{1,2,3}};
        double [][] expected = null;
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterSomaDeMatrizesTesteQuatro() {
        double [][] m1 = {{1,2},{1,2}};
        double [][]m2 = {{1,2},{1,2}};
        double [][] expected = {{2,4},{2,4}};
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterSomaDeMatrizesTesteCinco() {
        double [][] m1 = {{1},{1}};
        double [][]m2 = {{1},{1}};
        double [][] expected = {{2.0},{2.0}};
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterSomaDeMatrizesTesteSeis() {
        double [][] m1 = {{1,2,3},{1,2,3},{1,1,1}};
        double [][]m2 = {{1,2,3},{1,4,5},{2,2,2}};
        double [][] expected = {{2,4,6},{2,6,8},{3,3,3}};
        double[][] result = ExercicioDezassete.obterSomaDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }


    @Test
    void obterMultiplicacaoDeMatrizes() {

        double [][]m1={{}};
        double [][]m2={{}};
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertEquals(expected,result);

    }

    @Test
    void obterMultiplicacaoDeMatrizesTesteDois() {

        double [][]m1={{},{}};
        double [][]m2= {{},{}};
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertEquals(expected,result);

    }

    @Test
    void obterMultiplicacaoDeMatrizesTesteTres() {

        double [][]m1={{1,2,3},{1,2,3}};
        double [][]m2= {{1,2,3},{1,2,3}};
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertEquals(expected,result);

    }

    @Test
    void obterMultiplicacaoDeMatrizesTesteQuatro() {

        double [][]m1={{1,2},{1,2,3}};
        double [][]m2= {{1,2},{1,2}};
        double [][] expected = null;
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertEquals(expected,result);

    }

    @Test
    void obterMultiplicacaoDeMatrizesTesteCinco() {

        double [][]m1={{1,2,3}};
        double [][]m2= {{4},{5},{6}};
        double [][] expected = {{32.0}};
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterMultiplicacaoDeMatrizesTesteSeis() {


        double [][]m2= {{4},{5},{6}};
        double [][]m3 = {{1,2,3}};
        double [][] expected = {{4,8,12},{5,10,15},{6,12,18}};
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m2,m3);
        assertArrayEquals(expected,result);

    }


    @Test
    void obterMultiplicacaoDeMatrizesTesteSete() {

        double[][]m1={{1,2},{1,2}};
        double [][]m2= {{1,1},{1,1}};
        double [][] expected = {{3,3},{3,3}};
        double [][] result = ExercicioDezassete.obterMultiplicacaoDeMatrizes(m1,m2);
        assertArrayEquals(expected,result);

    }







}