public class ExercicioOito {

    public static void main(String[] args) {



    }

    /*
    Primeiro calcular o nº de múltiplos
    e depois criar um array  com os multiplos
     */

    public static int nMultiplosComuns (int inicio, int fim, int[] multiplos)
    {
        int temp =0;
        int cont =0;
        // testar os limites que são inseridos
        if( inicio > fim)
        {
            temp=fim;
            fim= inicio;
            inicio = temp;
        }
        if(inicio<0 || fim <0|| multiplos.length==0 ||multiplos[0]<=0 || multiplos[1] <=0)
        {
            // retornar a zero e impedir de fazer a divisão
            return 0;
        }

        for (int i = inicio; i <=fim ; i++) {
            if (i % multiplos[0] == 0 && i % multiplos[1] == 0) {
                cont++;
            }
        }
            return cont;
    }

    public static int[] multiplosComuns(int inicio, int fim, int[] multiplos)
    {
        // iterador para o novo array
        int k =0;

        // dimensão do novo array
        int dim = nMultiplosComuns(inicio,fim,multiplos);
        int [] multiplosComuns = new int [dim];

        if(multiplosComuns.length==0)
        {
            return multiplosComuns;
        }

        for (int i = inicio; i <=fim ; i++) {
            if (i % multiplos[0] == 0 && i % multiplos[1] == 0) {
                multiplosComuns[k]=i;
                k++;
            }
        }
        return multiplosComuns;
    }


}
