import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCatorzeTest {

    @Test
    void eMatrizRectangular() {
        int [][]v={{}};
        boolean expected = false;
        boolean result = ExercicioCatorze.eMatrizRectangular(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizRectangularTesteDois() {
        int [][]v={{1,2},{1,2}};
        boolean expected = false;
        boolean result = ExercicioCatorze.eMatrizRectangular(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizRectangularTesteTres() {
        int [][]v={{1,2},{1,2,3}};
        boolean expected = false;
        boolean result = ExercicioCatorze.eMatrizRectangular(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizRectangularTesteQuatro() {
        int [][]v={{1,2},{1,2},{1,2}};
        boolean expected = true;
        boolean result = ExercicioCatorze.eMatrizRectangular(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizRectangularTesteCinco() {
        int [][]v={{1},{1}};
        boolean expected = true;
        boolean result = ExercicioCatorze.eMatrizRectangular(v);
        assertEquals(expected,result);

    }



}