package Sudoku;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCellTest {


    @Test
    void setInvalidValue() {

        int value = -1;
        boolean isfixed = false;

        assertThrows(IllegalArgumentException.class, () -> {
            SudokuCell s = new SudokuCell(value, isfixed);
        });

    }

    @Test
    void testOneChangeToAnInvalidValue() {
        SudokuCell cell = new SudokuCell(1, false);
        int valueToChange = 10;
        int expected = 1;
        cell.changeAValue(valueToChange);
        int result = cell.getValue();
        assertEquals(expected, result);
    }

    @Test
    void testTwoChangeAFixValue() {
        SudokuCell cell = new SudokuCell(1, true);
        int valueToChange = 5;
        int expected = 1;
        cell.changeAValue(5);
        int result = cell.getValue();
        assertEquals(expected, result);
    }


    @Test
    void testThreeChangeAValue() {
        SudokuCell cell = new SudokuCell(1, false);
        int valueToChange = 5;
        int expected = 5;
        cell.changeAValue(5);
        int result = cell.getValue();
        assertEquals(expected, result);
    }


}