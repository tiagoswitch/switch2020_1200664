import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuinzeTest {

    @Test
    void obterElemMenorValor() {
        int [][]v = {{4,2,3},{1,5,7}};
        int expected = 1;
        int result = ExercicioQuinze.obterElemMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMenorValorTesteDois() {
        int [][]v = {{4,2,3},{5,5,7},{3,4,5}};
        int expected = 2;
        int result = ExercicioQuinze.obterElemMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMenorValorTesteTres() {
        int [][]v = {{4,2,3},{1,5,7}};
        int expected = 1;
        int result = ExercicioQuinze.obterElemMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMenorValorTesteQuatro() {
        int [][]v = {};
        int expected = -1;
        int result = ExercicioQuinze.obterElemMenorValor(v);
        assertEquals(expected,result);

    }


    @Test
    void obterElemMaiorValor() {

        int [][]v = {};
        int expected = -1;
        int result = ExercicioQuinze.obterElemMaiorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMaiorValorTesteDois() {

        int [][]v = {{1,2,3},{3,4,5}};
        int expected = 5;
        int result = ExercicioQuinze.obterElemMaiorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMaiorValorTesteTres() {

        int [][]v = {{1,2,3},{3,4,5},{6,8,9}};
        int expected = 9;
        int result = ExercicioQuinze.obterElemMaiorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemMaiorValorTesteQuatro() {

        int [][]v = {{1,2,3},{3,4,5},{3,3,3},{2,6,7}};
        int expected = 7;
        int result = ExercicioQuinze.obterElemMaiorValor(v);
        assertEquals(expected,result);
    }


    @Test
    void obterValorMedio() {
        int [][]v = {{1,2,3},{3,4,6},{4,5,8}};
        double expected = 4.0;
        double result = ExercicioQuinze.obterValorMedio(v);
        assertEquals(expected,result,0.1);
    }

    @Test
    void obterValorMedioTesteDois() {
        int [][]v = {{1,2,3},{3,4,6}};
        double expected = 3.2;
        double result = ExercicioQuinze.obterValorMedio(v);
        assertEquals(expected,result,0.1);
    }

    @Test
    void obterValorMedioTesteTres() {
        int [][]v = {};
        double expected = 0;
        double result = ExercicioQuinze.obterValorMedio(v);
        assertEquals(expected,result,0.1);
    }


    @Test
    void obterprodutoDosElem() {
        int [][]v = {{1,2},{3,4},{1,2}};
        int expected =48;
        int result = ExercicioQuinze.obterprodutoDosElem(v);

    }

    @Test
    void obterprodutoDosElemTesteDois() {
        int [][]v = {{1,2,3},{3,4,3},{1,2,1}};
        int expected =432;
        int result = ExercicioQuinze.obterprodutoDosElem(v);

    }

    @Test
    void obterprodutoDosElemTesteTres() {
        int [][]v = {{1,2},{3,4},{1,-1}};
        int expected =-48;
        int result = ExercicioQuinze.obterprodutoDosElem(v);

    }


    @Test
    void obterElemNaoRepetidos() {

        int [][]v = {{1,3,3},{3,4,5},{5,6,7}};
        int [] expected = {1,3,4,5,6,7};
        int [] result = ExercicioQuinze.obterElemNaoRepetidos(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterElemNaoRepetidosTesteDois() {

        int [][]v = {{1,2,3,4},{3,4,5},{5,6,7}};
        int [] expected = null;
        int [] result = ExercicioQuinze.obterElemNaoRepetidos(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterElemNaoRepetidosTesteTres() {

        int [][]v = {{1,1,3},{3,3,3},{5,5,5}};
        int [] expected = {1,3,5};
        int [] result = ExercicioQuinze.obterElemNaoRepetidos(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterElemNaoRepetidosTesteQuatro() {

        int [][]v = {{1,2,3},{3,4,5},{5,6,7,6}};
        int [] expected =null;
        int [] result = ExercicioQuinze.obterElemNaoRepetidos(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterElemNaoRepetidosTesteCinco() {

        int [][]v = {{1,2,3,4},{3,4,5,5},{5,6,7,6}};
        int [] expected = {1,2,3,4,5,6,7};
        int [] result = ExercicioQuinze.obterElemNaoRepetidos(v);
        assertArrayEquals(expected,result);
    }


    @Test
    void obterElemPrimos() {
        int [][]v = {{1,2,4},{2,7,11},{4,13,6}};
        int [] expected = {2,2,7,11,13};
        int [] result = ExercicioQuinze.obterElemPrimos(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterElemPrimosTesteDois() {
        int[][]v = {{1,2,4},{2,7,11,2},{4,13,6}};
        int [] expected = null;
        int [] result = ExercicioQuinze.obterElemPrimos(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemPrimosTesteTres() {
        int [][]v = {{},{},{}};
        int [] expected = null;
        int [] result = ExercicioQuinze.obterElemPrimos(v);
        assertEquals(expected,result);

    }

    @Test
    void obterElemPrimosTesteQuatro() {
        int [][]v = {{10,4,4},{20,4,12},{4,12,6}};
        int [] expected = {};
        int [] result = ExercicioQuinze.obterElemPrimos(v);
        assertArrayEquals(expected,result);

    }


    @Test
    void obterDiagonalPrincipalDaMatriz() {
        int [][]v = {{1,2,4},{1,2,4,5},{1,2}};
        int [] expected = null;
        int []result = ExercicioQuinze.obterDiagonalPrincipalDaMatriz(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDiagonalPrincipalDaMatrizTesteDois() {
        int [][]v = {{},{},{}};
        int [] expected = null;
        int []result = ExercicioQuinze.obterDiagonalPrincipalDaMatriz(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDiagonalPrincipalDaMatrizTesteTres() {
        int [][]v = {{1,2,3},{2,2,2},{3,3,3}};
        int [] expected = {1,2,3};
        int []result = ExercicioQuinze.obterDiagonalPrincipalDaMatriz(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterDiagonalPrincipalDaMatrizTesteQuatro() {
        int [][]v = {{1,2,3},{2,2,2},{3,3,3},{1,2,3}};
        int [] expected = {1,2,3};
        int []result = ExercicioQuinze.obterDiagonalPrincipalDaMatriz(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterDiagonalSecundariaDaMatrizTesteUm() {
        int [][]v = {{1,2,3},{2,2,2},{3,3,3,6},{1,2,3}};
        int [] expected = null;
        int []result = ExercicioQuinze.obterDiagonalSecundariaDaMatriz(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDiagonalSecundariaDaMatrizTesteDois() {
        int [][]v = {{},{},{}};
        int [] expected = null;
        int []result = ExercicioQuinze.obterDiagonalSecundariaDaMatriz(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDiagonalSecundariaDaMatrizTesteTres() {
        int [][]v = {{1,2,3},{2,2,2},{3,3,3},{1,2,3}};
        int [] expected = {3,2,3};
        int []result = ExercicioQuinze.obterDiagonalSecundariaDaMatriz(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterDiagonalSecundariaDaMatrizTesteQuatro() {
        int [][]v = {{1,2,4},{2,4,2},{4,3,3}};
        int [] expected = {4,4,4};
        int []result = ExercicioQuinze.obterDiagonalSecundariaDaMatriz(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void eMatrizIdentidade() {

        int [][]v = {{},{}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteDois() {

        int [][]v = {{1,2,3},{1,2,4,6}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteTres() {

        int [][]v = {{1,2},{1,2},{1,2}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteQuatro() {

        int [][]v = {{1,2},{1,2}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteCinco() {

        int [][]v = {{1,0},{0,0}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteSeis() {

        int [][]v = {{1,0},{0,1}};
        boolean expected = true;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteSete() {

        int [][]v = {{1,0,0},{0,1,0},{0,0,1}};
        boolean expected = true;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }

    @Test
    void eMatrizIdentidadeTesteOito() {

        int [][]v = {{1,0,0},{0,0,0},{0,0,1}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizIdentidade(v);
        assertEquals(expected,result);
    }


    @Test
    void eMatrizQuadrada() {
        int [][]v = {{1,2},{1}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizQuadrada(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizQuadradaTesteDois() {
        int [][]v = {{1,2,3},{1,2,3}};
        boolean expected = false;
        boolean result = ExercicioQuinze.eMatrizQuadrada(v);
        assertEquals(expected,result);

    }

    @Test
    void eMatrizQuadradaTesteTres() {
        int [][]v = {{1,2},{1,2}};
        boolean expected = true;
        boolean result = ExercicioQuinze.eMatrizQuadrada(v);
        assertEquals(expected,result);

    }


    @Test
    void eMatrizQuadradaTesteQuatro() {
        int [][]v = {{1,2,2},{1,3,2},{1,2,3}};
        boolean expected = true;
        boolean result = ExercicioQuinze.eMatrizQuadrada(v);
        assertEquals(expected,result);

    }

    @Test
    void obterMatrizCoeficientes() {
        int[][]v = {{1,2,3},{1,2,3},{1,2,3}};
        int linha = 0;
        int coluna = 0;
        int [][]expected={{2,3},{2,3}};
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteDois() {
        int [][]v = {{1,2},{1,2}};
        int linha = 0;
        int coluna = 0;

        int [][]expected={{2}};
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteTres() {
        int [][]v = {{1,2},{1,2}};
        int linha = 1;
        int coluna = 1;

        int [][]expected={{1}};
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteQuatro() {
        int [][] v= {{}};
        int linha = 1;
        int coluna = 1;
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteCinco() {
        int [][] v= null;
        int linha = 1;
        int coluna = 1;
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }


    @Test
    void obterMatrizCoeficientesTesteSeis() {
        int [][] v= {{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}};;
        int linha = 0;
        int coluna =0;
        int [][] expected = {{2,3,4},{2,3,4},{2,3,4}};
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteSete() {
        int [][] v= {{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}};;
        int linha = 1;
        int coluna =2;
        int [][] expected = {{1,2,4},{1,2,4},{1,2,4}};
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizCoeficientesTesteOito() {
        int [][] v= {{1}};;
        int linha = 1;
        int coluna =2;
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizDosMenores(v,linha,coluna);
        assertArrayEquals(expected,result);
    }



    @Test
    void obterDeterminante() {

        int[][] v= null;
        //Integer expected = null;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertNull(result);
    }

    @Test
    void obterDeterminanteTesteDois() {
        int [][] v = {{1,2},{1,2,3},{1,2,4}};
        Integer expected = null;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertEquals(expected,result);
    }

    @Test
    void obterDeterminanteTesteTres() {
        int [][] v = {{1,2},{1,2},{1,2}};
        Integer expected = null;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertEquals(expected,result);
    }

    @Test
    void obterDeterminanteTesteQuatro() {

        int [][] v = {{1,2},{1,2}};
        Integer expected = 0;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertEquals(expected,result);
    }

    @Test
    void obterDeterminanteTesteCinco() {

        int [][] v = {{1,2,3},{9,4,5},{4,3,7}};
        Integer expected = -40;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertEquals(expected,result);
    }

    @Test
    void obterDeterminanteTesteSeis() {

        int [][] v = {{1,2,3,6},{9,4,5,7},{4,3,7,8},{5,0,0,9}};
        Integer expected = -495;
        Integer result = ExercicioQuinze.obterDeterminante(v);
        assertEquals(expected,result);
    }

    @Test
    void obterMatrizAdjunta() {
        int[][] v = null;
        int[][] expected = null;
        int[][] result = ExercicioQuinze.obterMatrizAdjunta(v);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizAdjuntaTesteDois() {
        int [][] m = {{1,2},{1}};
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizAdjunta(m);
        assertEquals(expected,result);

    }

    @Test
    void obterMatrizAdjuntaTesteTres() {
        int [][] m = {{1,2},{1,2},{1,2}};
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizAdjunta(m);
        assertEquals(expected,result);

    }
    @Test
    void obterMatrizAdjuntaTesteQuatro() {
        int [][] m = {{1,0,2},{0,3,0},{4,0,5}};
        int [][] expected = {{15,0,-12},{0,-3,0},{-6,0,3}};
        int [][] result = ExercicioQuinze.obterMatrizAdjunta(m);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterMatrizAdjuntaTesteCinco() {
        int [][] m = {{1,4,3},{1,3,4},{4,5,7}};
        int [][] expected = {{1,9,-7},{-13,-5,11},{7,-1,-1}};
        int [][] result = ExercicioQuinze.obterMatrizAdjunta(m);
        assertArrayEquals(expected,result);

    }


    @Test
    void obterMatrizInversa() {
        int [][]v = null;
        double[][] expected = null;
        double [][] result = ExercicioQuinze.obterMatrizInversa(v);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizInversaTesteDois() {
        int [][]v ={{1,2,3},{1,2,3},{1,2,3}};
        double [][] expected = null;
        double [][] result = ExercicioQuinze.obterMatrizInversa(v);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizInversaTesteTres() {
        int [][]v ={{}};
        double [][] expected = null;
        double [][] result = ExercicioQuinze.obterMatrizInversa(v);
        assertEquals(expected, result);
    }

    @Test
    void obterMatrizInversaTesteQuatro() {
        int [][]v ={{2,1},{5,3}};
        double[][] expected = {{3,-1},{-5,2}};
        double [][] result = ExercicioQuinze.obterMatrizInversa(v);
        assertArrayEquals(expected, result);
    }

    @Test
    void obterMatrizInversaTesteCinco() {
        int [][]v ={{1,2,3},{0,1,4},{0,0,1}};
        double[][] expected ={{1,-2,5},{0,1,-4},{0,0,1}};
        double [][] result = ExercicioQuinze.obterMatrizInversa(v);
        assertArrayEquals(expected, result);
    }

    @Test
    void obterMatrizInversaTesteSeis() {
        int [][] v1 = {{1,1,1,1},{1,2,-1,2},{1,-1,2,1},{1,3,3,2}};
        double[][] expected ={{2.33, -0.33, -0.33, -0.67},{0.44,-0.11,-0.44,0.11},{-0.11,-0.22,0.11,0.22},{-1.67,0.67,0.67,0.33}};
        double [][] result = ExercicioQuinze.obterMatrizInversa(v1);
        assertArrayEquals(expected,result);
    }


    @Test
    void obterMatrizTransposta() {
        int [][] v= null;
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizTransposta(v);
        assertEquals(expected,result);
    }

    @Test
    void obterMatrizTranspostaTesteDois() {
        int [][] v= {{}};
        int [][] expected = null;
        int [][] result = ExercicioQuinze.obterMatrizTransposta(v);
        assertEquals(expected,result);
    }

    @Test
    void obterMatrizTranspostaTesteTres() {
        int [][] v={{1,2},{3,1}};
        int [][] expected = {{1,3},{2,1}};
        int [][] result = ExercicioQuinze.obterMatrizTransposta(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizTranspostaQuatro() {
        int [][] v= {{1,2,3},{4,5,6}};
        int [][] expected = {{1,4},{2,5},{3,6}};
        int [][] result = ExercicioQuinze.obterMatrizTransposta(v);
        assertArrayEquals(expected,result);
    }

    @Test
    void obterMatrizTranspostaCinco() {
        int [][] v= {{1,2,3},{4,5,6},{7,8,9}};
        int [][] expected = {{1,4,7},{2,5,8},{3,6,9}};
        int [][] result = ExercicioQuinze.obterMatrizTransposta(v);
        assertArrayEquals(expected,result);
    }



}