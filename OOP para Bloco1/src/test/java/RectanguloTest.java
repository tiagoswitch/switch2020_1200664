import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectanguloTest {

    @Test
    void getPerimetro() {
        int width = -1;
        int height = 4;
        assertThrows(IllegalArgumentException.class, () -> {
            new Rectangulo(width, height);
        });

    }

    @Test
    void getPerimetroTesteDois() {
        int width = 1;
        int height = -4;
        assertThrows(IllegalArgumentException.class, () -> {
            new Rectangulo(width, height);
        });

    }

    @Test
    void getPerimetroTesteTres() {
        int width = 5;
        int height = 2;
        int expected =14;
        Rectangulo r1 = new Rectangulo(width,height);
        int result = r1.getPerimetro();
        assertEquals(expected,result);
    }


    @Test
    void getPerimetroTesteParaValido() {
        int width = 1;
        int height = 4;
        int expected = 10;
        Rectangulo r1 = new Rectangulo(width,height);

        int result = r1.getPerimetro();
        assertEquals(result,expected);
    }




}