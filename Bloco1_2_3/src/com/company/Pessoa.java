package com.company;

public class Pessoa {

    private double altura= 1.9;
    private double sombra = 0.5;

    public Pessoa(double altura,double sombra)
    {
        this.altura = altura;
        this.sombra= sombra;
    }

    public double getAltura()
    {
        return altura;
    }

    public double getSombra()
    {
        return sombra;
    }

}
