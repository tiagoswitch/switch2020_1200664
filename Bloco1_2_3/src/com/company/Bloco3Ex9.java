package com.company;
import java.util.Scanner;
public class Bloco3Ex9 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double [] salario = {1000,500,700,500};
        double [] horas ={10,3,3,5};

        double [] salarioPago = Bloco3Ex9.salarioMensal(salario,horas);
        for(int j=0; j<salarioPago.length;j++)
        {
            if(salario[j]>0) System.out.println(salarioPago[j]);
        }
    }

    public static double [] salarioMensal(double []salarioBase, double []horas) {

        int j =0;
        double[] salarioPago = new double[salarioBase.length];



            for (int i = 0; i < salarioBase.length; i++)
            {

                salarioPago[i] = salarioBase[i] + horas[i] * (salarioBase[i] * 0.02);

                if(horas[i]<0)return salarioPago;
        }
        return salarioPago;
    }

    public static double mediaDeSalariosPagos(double []salarioPago)
    {
        double soma =0;
        double media =0;
        for(int i =0; i < salarioPago.length; i++)
        {
            soma= soma + salarioPago[i];

        }
        media = soma/salarioPago.length;
        if(salarioPago.length == 0) media =0;
        return media;
    }

}


