package com.company;

public class Bloco3Ex7 {


    public static void main(String[] args) {





    }

    public static boolean isCapicua(long n) {
        long numero = n;
        long reversed = 0;
        while (n > 0) {
            reversed = reversed * 10 + n % 10;
            n = n / 10;
        }
        if (reversed == numero) return true;
        else return false;
    }



    public static boolean isArmstrong(long n) {
        long numero = n;
        double cubo = 0;
        while (n > 0) {
            cubo = cubo + Math.pow(n % 10, 3);
            n = n / 10;
        }
        if (cubo == numero) return true;
        else return false;

    }


    public static int firstCapicua(int inicio, int fim)
    {
        if(inicio>fim)
        {
            int temp = 0;

            inicio = temp;
            fim =inicio;
            temp = fim;
        }
        int inst =0;
        for(int i =inicio; i<= fim; i++)
        {
            if(isCapicua(i))
            {
                inst= i;
                return inst;
            }
        }
        return inst;
    }

    public static int maiorCapicua(int inicio, int fim)
    {
        if(inicio>fim)
        {
            int temp = 0;

            inicio = temp;
            fim =inicio;
            temp = fim;
        }

        int maior =0;
        for(int i =inicio; i<= fim; i++)
        {
            if(isCapicua(i))
            {
                if(i>maior)
                {
                    maior=i;
                }

            }
        }
        return maior;
    }

    public static int nCapicuas(int inicio, int fim)
    {

        if(inicio>fim)
        {
            int temp = 0;

            inicio = temp;
            fim =inicio;
            temp = fim;
        }

        int cont=0;
        for(int i =inicio; i<=fim; i++)
        {
            if(isCapicua(i))
            {
                cont++;
            }

        }
        return cont;
    }

    public static int firstArmstrong(int inicio, int fim)
    {

        if(inicio>fim)
        {
            int temp = 0;

            inicio = temp;
            fim =inicio;
            temp = fim;
        }

        int inst = 0;
        for (int i = inicio; i <= fim; i++) {
            if(isArmstrong(i))
            {
                inst = i;
                break;
            }
        }
        return inst;
    }

    public static int nArmstrong(int inicio, int fim)
    {
        int cont =0;
        if(inicio>fim)
        {
            int temp = 0;

            inicio = temp;
            fim =inicio;
            temp = fim;
        }
        for(int i =inicio;i<=fim;i++)
        {
            if(isArmstrong(i))
            {
                cont++;
            }
        }
        return cont;

    }

}