import java.util.Scanner;

public class ExercicioDezanove {

    public static final int comprimento = 9; // comprimento de um tabuleiro


    public static void main(String[] args) {
        jogarSudoko();
    }

    /**
     * Método para jogar Sudoku
     */

    public static void jogarSudoko() {
        Scanner sc = new Scanner(System.in);

        int[][] matrizSudoku = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};


        int number;
        int columm;
        int row;
        do {
            imprimirMatriz(matrizSudoku);
            System.out.println();
            System.out.println("Insira o número da linha ");
            row = sc.nextInt();
            System.out.println("Insira o número da Coluna");
            columm = sc.nextInt();
            System.out.println("Insira o número que quer introduzir ");
            number = sc.nextInt();

            if (validarNumeroInseridoNoSudoku(number) && !foraDosLimitesDeJogo(matrizSudoku, row, columm)) {
                matrizSudoku = matrizActualizada(matrizSudoku, row, columm, number);
            }


        } while (verificarSeExistemEspacosPorPreencher(matrizSudoku));

        imprimirMatriz(matrizSudoku);
        System.out.println("Terminou !");

    }


    /**
     * Método para imprimir a matriz
     *
     * @param matriz
     */

    public static void imprimirMatriz(int[][] matriz) {

        for (int i = 0; i < matriz.length; i++) {
            System.out.println();
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print("| " + matriz[i][j] + " |");
            }

        }
    }

    /**
     * Método que verifica que uma matriz tem dimensões válidas
     *
     * @param matriz
     * @return retorna true se a matriz é válida
     */

    public static boolean validarAMatrizDeJogoNovePorNove(int[][] matriz) {
        if (matriz == null) return false;
        else if (matriz.length == comprimento && matriz[0].length == comprimento) return true;
        else return false;
    }


    /**
     * Método que verifica se um número é diferente de zero
     *
     * @param i1
     * @return retorna true se for diferente de zero
     */

    public static boolean verificarQueDifereDeZero(int i1) {
        if (i1 != 0) return true;
        else return false;
    }

    /**
     * Função que retorna uma matriz máscara com 0s e 1s e dado um número, quando é diferente de zero a matriz mascara retorna 1s e no resto zeros
     *
     * @param matrizBaseParaSudoku que vai procurar os numeros que são diferentes de zero e coloca noutra matriz com um 1
     * @return Matriz Mascara com 0s e 1s
     */

    public static int[][] obterMatrizMascaraInicial(int[][] matrizBaseParaSudoku) {
        if (matrizBaseParaSudoku == null || !validarAMatrizDeJogoNovePorNove(matrizBaseParaSudoku)) return null;
        int[][] matrizInicial = new int[comprimento][comprimento]; // MatrizQuadrada

        for (int i = 0; i < matrizBaseParaSudoku.length; i++) {
            for (int j = 0; j < matrizBaseParaSudoku[i].length; j++) {
                if (verificarQueDifereDeZero(matrizBaseParaSudoku[i][j])) // Se for diferente de zero é numero presente na matriz
                    matrizInicial[i][j] = 1;
            }
        }
        return matrizInicial;
    }

    /**
     * Método que valida o número que vai ser colocado na matriz
     *
     * @param numero
     * @return retorna se é válido
     */

    public static boolean validarNumeroInseridoNoSudoku(int numero) {
        if (numero > 0 && numero <= 9) return true;
        else return false;
    }

    /**
     * Método que verifica se a coluna e a linha que são inseridas estão inválidas na matriz do sudoku
     *
     * @param matrizDeJogo do sudoku
     * @param linha
     * @param coluna
     * @return
     */

    public static boolean foraDosLimitesDeJogo(int[][] matrizDeJogo, int linha, int coluna) {
        if (matrizDeJogo == null) return true;
        else if (linha > matrizDeJogo.length || linha < 0 || coluna < 0 || coluna > matrizDeJogo[0].length) return true;
        else return false;
    }


    /**
     * Método que escreve um número na matriz. Se o número, a linha e a coluna forem inválidas devolve igual
     *
     * @param matrizDeJogo
     * @param linha
     * @param coluna
     * @param numero
     * @return matriz do sudoku actualizada
     */

    public static int[][] matrizActualizada(int[][] matrizDeJogo, int linha, int coluna, int numero) {
        if (matrizDeJogo == null || foraDosLimitesDeJogo(matrizDeJogo, linha, coluna) || !validarAMatrizDeJogoNovePorNove(matrizDeJogo)) {
            return null;
        }
        if (!verificarQueDifereDeZero(matrizDeJogo[linha][coluna])
                && validarNumeroInseridoNoSudoku(numero)
                && validarJogada(matrizDeJogo, linha, coluna, numero)) {

            matrizDeJogo[linha][coluna] = numero;
        }
        return matrizDeJogo;

    }

    /**
     * Método que verifica se existem espaços para preencher
     *
     * @param matrizDeJogo do sudoku
     * @return true se existem espaços para preencher
     */
    public static boolean verificarSeExistemEspacosPorPreencher(int[][] matrizDeJogo) {
        boolean espacosPorPreencher = false;
        for (int i = 0; i < matrizDeJogo.length && !espacosPorPreencher; i++) {
            for (int j = 0; j < matrizDeJogo[i].length && !espacosPorPreencher; j++) {
                if (!verificarQueDifereDeZero(matrizDeJogo[i][j])) {
                    espacosPorPreencher = true;
                }
            }
        }
        return espacosPorPreencher;
    }


    //Alínea d)

    /**
     * Método para Método que retorna se é válido escrever um número numa determinada linha,
     * coluna ou num bloco de 3x3
     *
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param numero
     * @return retorna true se pode colocar o número
     */

    public static boolean validarJogada(int[][] matrizJogo, int linha, int coluna, int numero) {
        if (matrizJogo == null || foraDosLimitesDeJogo(matrizJogo, linha, coluna)
                || !validarAMatrizDeJogoNovePorNove(matrizJogo) || !validarNumeroInseridoNoSudoku(numero)) {
            return false;
        }
        boolean decisaoDaJogada = false;
        if (!verificarQueDifereDeZero(matrizJogo[linha][coluna])) {
            if (validaJogadaPorLinha(matrizJogo, linha, coluna, numero) && validaJogadaPorLinha(matrizJogo, linha, coluna, numero)
                    && validaJogadaPorColuna(matrizJogo, linha, coluna, numero)) {
                decisaoDaJogada = true;
            }
        } else {
            decisaoDaJogada = false;
        }
        return decisaoDaJogada;
    }

    public static boolean validaJogadaPorLinha(int[][] matrizJogo, int linha, int coluna, int numero) {
        boolean resultado = true;
        if (matrizJogo == null || !(validarAMatrizDeJogoNovePorNove(matrizJogo))) return false;
        for (int i = 0; i < matrizJogo.length; i++) {
            if (matrizJogo[linha][i] == numero) {
                resultado = false;
            }
        }
        return resultado;
    }

    public static boolean validaJogadaPorColuna(int[][] matrizJogo, int linha, int coluna, int numero) {

        boolean resultado = true;
        if (!(validarAMatrizDeJogoNovePorNove(matrizJogo))) return false;
        for (int i = 0; i < matrizJogo[0].length; i++) {
            if (matrizJogo[i][coluna] == numero) {
                resultado = false;
            }
        }
        return resultado;
    }


    public static boolean validaJogadaPorBloco(int[][] matriz, int linha, int coluna, int numero) {

        boolean eValido = true;
        int rowMinimum = (linha / 3) * 3;
        int columnMinium = (coluna / 3) * 3;
        for (int i = rowMinimum; i <= rowMinimum + 3 && eValido; i++) {
            for (int j = columnMinium; j <= columnMinium + 3 && eValido; j++) {
                if (matriz[i][j] == numero) {
                    eValido = false;
                }
            }

        }
        return eValido;
    }
}
