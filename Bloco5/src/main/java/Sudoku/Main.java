package Sudoku;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        int[][] initialMatrix = {{0, 8, 7, 6, 0, 3, 2, 4, 0},
                {0, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 0, 0, 0}};


        int number;
        int columm;
        int row;
        SudokuBoard grid = new SudokuBoard(initialMatrix);
        do {


            System.out.println("Insira o número da linha ");
            row = sc.nextInt();
            System.out.println("Insira o número da Coluna");
            columm = sc.nextInt();
            System.out.println("Insira o número que quer introduzir ");
            number = sc.nextInt();


            grid.addValueGivenCoordinates(row, columm, number);
            int[][] finalArray = grid.toArray();
            System.out.println();
            for (int i = 0; i < finalArray.length; i++) {
                System.out.println();
                for (int j = 0; j < finalArray[i].length; j++) {
                    System.out.print(" " + finalArray[i][j] + " ");
                }
            }
            System.out.println();
            System.out.println("-----------------------------");

        } while (!grid.checkIfTheGridIsFull());
        grid.toArray();
        System.out.println("Terminou !");
    }
}
