package Sudoku;

public class SudokuBoard {
    private final SudokuCell[][] board = new SudokuCell[9][9];


    public SudokuBoard(int[][] initialMatrix) {
        this.fillSudokuBoard(initialMatrix);
    }


    public void fillSudokuBoard(int[][] gameBoard) {
        int[][] sudokuGameBoard = getMaskSudokuGridToStartPlaying(gameBoard);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                boolean isFixed = sudokuGameBoard[i][j] == 1;
                this.board[i][j] = new SudokuCell(gameBoard[i][j], isFixed);
            }
        }
    }

    private int[][] getMaskSudokuGridToStartPlaying(int[][] matrix) {
        int[][] initialMatrix = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (matrix[i][j] != 0) // Se for diferente de zero é numero presente na matriz
                    initialMatrix[i][j] = 1;
            }
        }
        return initialMatrix;
    }


    private int[][] copyMatrix(int[][] matrix) {
        int[][] copiedMatrix = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                copiedMatrix[i][j] = matrix[i][j];
            }
        }
        return copiedMatrix;
    }


    /**
     * Método para verificar se as linhas e as colunas são válidas
     *
     * @param row
     * @param column
     * @return True se estiverem dentro dos parametros
     */

    private boolean areValidCoordinates(int row, int column) {

        return (row >= 0 && row < 9)
                && (column >= 0 && column < 9);
    }

    /**
     * Método para obter o valor da célula dando a linha e coluna
     *
     * @param row
     * @param column
     * @return
     */

    public int getBoardValue(int row, int column) {
        return this.board[row][column].getValue();

    }

    /**
     * Método para Imprimir a matriz
     *
     * @return uma matriz de inteiros com os valores
     */
    public int[][] toArray() {

        int[][] finalArray = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                finalArray[i][j] = board[i][j].getValue();
            }
        }
        return finalArray;
    }


    /**
     * Verifica se já não existem células por preencher
     *
     * @return true se estiver completo
     */

    public boolean checkIfTheGridIsFull() {
        boolean isFull = true;
        for (int i = 0; i < this.board.length && isFull; i++) {
            for (int j = 0; j < this.board[i].length && isFull; j++) {
                if (this.board[i][j].getValue() == 0) {
                    isFull = false;
                }
            }
        }
        return isFull;
    }


    /**
     * Método para verificar se a jogada é válida por linha, coluna e bloco
     *
     * @param row
     * @param column
     * @param value
     * @return true se for válido
     */

    public boolean checkIsValidMove(int row, int column, int value) {
        boolean isMoveValid = this.validateMoveByRow(row, column, value)
                && this.validateMoveByColumn(row, column, value)
                && this.validateMoveByBlock(row, column, value);

        return isMoveValid;
    }


    private boolean validateMoveByRow(int row, int column, int value) {
        boolean flag = true;
        for (int i = 0; i < this.board.length; i++) {
            if (board[row][i].getValue() == value) {
                flag = false;
            }
        }
        return flag;
    }

    private boolean validateMoveByColumn(int row, int column, int value) {
        boolean flag = true;
        for (int i = 0; i < this.board.length; i++) {
            if (board[i][column].getValue() == value) {
                flag = false;
            }
        }
        return flag;
    }

    private boolean validateMoveByBlock(int row, int column, int value) {
        boolean isValid = true;
        int indexRow = (row / 3) * 3;
        int indexColumn = (column / 3) * 3;

        for (int i = indexRow; i < indexRow + 3 && isValid; i++) {
            for (int j = indexColumn; j < indexColumn + 3 && isValid; j++) {
                if (this.board[i][j].getValue() == value)
                    isValid = false;
            }
        }
        return isValid;
    }

    /**
     * Método para verificar se o valor está fixo na matriz
     *
     * @param row
     * @param column
     * @return True se está fixo
     */
    private boolean checkIFTheValueISFixed(int row, int column) {
        return this.board[row][column].isFixed();
    }

    /**
     * Método para adicionar um valor a uma célula dado a linha e a coluna
     * Se a linha e coluna forem válidas, se ainda houver espaço para preencher
     * e se a jogada for válida, o número é adicionado
     *
     * @param row
     * @param column
     * @param value
     */

    public void addValueGivenCoordinates(int row, int column, int value) {
        if (areValidCoordinates(row, column)
                && !checkIFTheValueISFixed(row, column)
                && checkIsValidMove(row, column, value)
                && (value > 0 && value <= 9)) {
            this.board[row][column].changeAValue(value);
        }
    }

    public void clearBoard() {
        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                if (!board[i][j].isFixed()) {
                    this.board[i][j].clear();
                }
            }
        }
    }

}














