package com.company;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class Bloco2Test {

    @Test
    void printDigito1() {

        int n = 100;
        int expected =1;
        int result = Bloco2.printDigito1(n);
        assertEquals(expected,result);
    }


    @Test
    void printDigito2() {

        int n = 100;
        int expected =0;
        int result = Bloco2.printDigito2(n);
        assertEquals(expected,result);
    }


    @Test
    void printDigito3()
    {
        int n = 123;
        int expected =3;
        int result = Bloco2.printDigito3(n);
        assertEquals(expected,result);

    }


    @Test
    void mediaPesada()
    {
        int nota1 = 10;
        int nota2 = 12;
        int nota3 = 10;
        int peso1 = 50;
        int peso2 = 25;
        int peso3 = 25;

        double expected = 10.5;
        double result = (double) Bloco2.mediaPesada(nota1,nota2,nota3,peso1,peso2,peso3);

        assertEquals(expected,result,0.01);

    }


    @Test
    void print_Digito() {

        int n= 102;
        int d1=1;
        int d2=0;
        int d3=2;

        String expected = "O número " + n + "possui os digitos " +(d1 + " " + d2 + " " +d3 )+ " e é par";
        String result= Bloco2.printDigito(n);

        assertEquals(expected,result);



    }


    @Test
    void testPrint_Digito_2Digitos() {

        int n=10;
        String expected = "O número " +n +" não tem 3 digitos";
        String result = Bloco2.printDigito(n);
        assertEquals(expected,result);

    }


    @Test
    void distanciaEntrePontos() {
        double x1 = 3;
        double x2 = 4;
        double y1 = 5;
        double y2 = 8;

        double expected = Math.sqrt(10);
        double result = Bloco2.distanciaEntrePontos(x1,x2,y1,y2);

        assertEquals(expected,result,0.1);

    }

    @Test
    void testDistanciaEntrePontosCasoZero()
    {
        double x1 = 1;
        double x2 = 1;
        double y1 = 4;
        double y2 = 4;

        double expected = 0;
        double result = Bloco2.distanciaEntrePontos(x1,x2,y1,y2);

        assertEquals(expected,result,0.01);

    }

    @Test
    void calculoValores() {

        double  x=2;
        double expected = 0;
        double result = Bloco2.CalculoValores(x);

        assertEquals(expected,result);

    }

    @Test
    void calculoValoresNegativos() {
        double  x=-2;
        double expected = -2;
        double result = Bloco2.CalculoValores(x);

        assertEquals(expected,result);
    }


    @Test
    void calculoValoresParaCasoNulo()
    {
        double  x=0;
        double expected = 0;
        double result = Bloco2.CalculoValores(x);

        assertEquals(expected,result);

    }

    @Test
    void areaParaVolume() {
        double area = 6;
        double aresta =1;
        double expected = 1;
        double result = Bloco2.areaParaVolumeEmCm2(area);

        assertEquals(expected,result,0.01);

    }

    @Test
    void testAreaParaVolume() {
        double area =24;
        double aresta =2;
        double expected =8;
        double result = Bloco2.areaParaVolumeEmCm2(area);

        assertEquals(expected, result,0.01);

    }

    @Test
    void classificacaoDoCubo() {
        double x =1;
        String expected= " Classifica - se como Pequeno";
        String result = Bloco2.classificacaoDoCubo(x);
        assertEquals(expected,result);

    }

    @Test
    void testClassificacaoDoCubo2()
    {
        double x =1.5;
        String expected = " Classifica -se como Medio";
        String result = Bloco2.classificacaoDoCubo(x);
        assertEquals(expected,result);

    }

    @Test
    void testClassificacaoDoCubo3()
    {

        double x =4;
        String expected= " Classifica -se como Grande";
        String result = Bloco2.classificacaoDoCubo(x);
        assertEquals(expected,result);


    }


    @Test
    void getHoras() {

        double sec = 6000;
        String expected = "1:40:0";
        String result = Bloco2.getHoras(sec);

        assertEquals(expected,result);

    }

    @Test
    void testGetHoras2()
    {
        double sec = 3599;
        String expected = "0:59:59";
        String result = Bloco2.getHoras(sec);
        assertEquals(expected,result);
    }


    @Test
    void indicarPeriodoDoDia()
    {
        int sec = 87000;
        String expected = "Total de segundos nao valido";
        String result = Bloco2.indicarPeriodoDoDia(sec);

        assertEquals(expected,result);

    }


    @Test
    void testIndicarPeriodoDoDia2() {
        int sec = 3800;
        String expected = "Boa noite!";
        String result = Bloco2.indicarPeriodoDoDia(sec);

        assertEquals(expected,result);

    }

    @Test
    void testIndicarPeriodoDoDia3() {
        int sec = 43222;
        String expected = "Boa tarde!";
        String result = Bloco2.indicarPeriodoDoDia(sec);

        assertEquals(expected,result);

    }

    @Test
    void testIndicarPeriodoDoDia4() {
        int sec = 21600;
        String expected = "Bom dia!";
        String result = Bloco2.indicarPeriodoDoDia(sec);

        assertEquals(expected,result);

    }


    @Test
    void maoDeObraPintores() {
        int rendimentoPintor = 8*2;
        double area =20;
        double salarioPintor = 64;

        double expected = 128;
        double result = Bloco2.maoDeObraPintores(area,salarioPintor,rendimentoPintor);
        assertEquals(expected,result,0.01);


    }

    @Test
    void custoDaTinta() {
        double custoDaTintaPorL = 6;
        double area =20;
        double rendimentoDaTinta = 3;

        double expected = 42;
        double result = Bloco2.custoTinta(area,custoDaTintaPorL,rendimentoDaTinta);
        assertEquals(expected,result,0.01);


    }


    @Test
    void multiplosOuDivisores() {

        double x = 2;
        double y = 2;
        String expected= "x é multiplo e divisor de y";
        String result =Bloco2.multiplosOuDivisores(x,y) ;

        assertEquals(expected,result);

    }

    @Test
    void multiplosOuDivisoresCaso2() {

        double x = 4;
        double y = 2;
        String expected= "x é multiplo de y";
        String result =Bloco2.multiplosOuDivisores(x,y) ;

        assertEquals(expected,result);

    }

    @Test
    void multiplosOuDivisoresCaso3() {

        double x = 2;
        double y = 4;
        String expected= "y é multiplo de x";
        String result =Bloco2.multiplosOuDivisores(x,y) ;

        assertEquals(expected,result);

    }

    @Test
    void multiplosOuDivisoresCaso4() {

        double x = 2;
        double y = 5;
        String expected= "x não é multiplo nem divisor y";
        String result =Bloco2.multiplosOuDivisores(x,y) ;

        assertEquals(expected,result);

    }


    @Test
    void testMultiplosOuDivisorescaso5() {

        double x = 2;
        double y = 0;
        String expected= "operação invalida !";
        String result =Bloco2.multiplosOuDivisores(x,y) ;

        assertEquals(expected,result);
    }


    @Test
    void verificarSeECrescente() {
        int n = 123;
        String expected = "A sequencia é crescente";
        String result = Bloco2.verificarSeECrescente(n);
        assertEquals(expected,result);

    }

    @Test
    void verificarSeECrescenteCaso2() {
        int n = 132;
        String expected = "A sequencia não é crescente";
        String result = Bloco2.verificarSeECrescente(n);
        assertEquals(expected,result);

    }


    @Test
    void precoDeSaldoDeArtigo() {
        double p = 210;
        double expected = 84;
        double result = Bloco2.precoDeSaldoDeArtigo(p);
        assertEquals(expected,result);


    }

    @Test
    void precoDeSaldoDeArtigoCaso2() {
        double p = 200;
        double expected = 120;
        double result = Bloco2.precoDeSaldoDeArtigo(p);
        assertEquals(expected,result);


    }


    @Test
    void precoDeSaldoDeArtigoCaso3() {
        double p = 70;
        double expected = 49;
        double result = Bloco2.precoDeSaldoDeArtigo(p);
        assertEquals(expected,result);


    }

    @Test
    void precoDeSaldoDeArtigoCaso4() {
        double p = 40;
        double expected = 32;
        double result = Bloco2.precoDeSaldoDeArtigo(p);
        assertEquals(expected,result);


    }

    @Test
    void precoDeSaldoDeArtigoCaso5() {
        double p = -5;
        double expected = 0;
        double result = Bloco2.precoDeSaldoDeArtigo(p);
        assertEquals(expected,result);


    }


    @Test
    void classificarAprovados() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = 0.5;
        String expected ="turma razoável";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }

    @Test
    void classificarAprovadosCaso2() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = 0.3;
        String expected ="turma fraca";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }

    @Test
    void classificarAprovadosCaso3() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = 0.15;
        String expected ="turma má";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }

    @Test
    void classificarAprovadosCaso4() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = 0.7;
        String expected ="turma boa";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }

    @Test
    void classificarAprovadosCaso5() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = -1;
        String expected ="Valor Inválido!";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }

    @Test
    void classificarAprovadosCaso6() {

        double tma =0.2;
        double tfraca=0.4;
        double trazoavel =0.6;
        double tboa =0.8;
        double aprovados = 0.95;
        String expected ="turma excelente";
        String result = Bloco2.classificarAprovados(aprovados,tma,tfraca,trazoavel,tboa);

        assertEquals(expected,result);
    }


    @Test
    void emiteIndicePoluicao() {

        double indice = 0.3;
        double expected = 1;
        double result = Bloco2.emiteIndicePoluicao(indice);
        assertEquals(expected,result);

    }

    @Test
    void emiteIndicePoluicaoCaso2() {

        double indice = 0.35;
        double expected = 2;
        double result = Bloco2.emiteIndicePoluicao(indice);
        assertEquals(expected,result);

    }

    @Test
    void emiteIndicePoluicaoCaso3() {

        double indice = 0.45;
        double expected = 3;
        double result = Bloco2.emiteIndicePoluicao(indice);
        assertEquals(expected,result);

    }

    @Test
    void emiteIndicePoluicaoCaso4() {

        double indice = 0.55;
        double expected = 4;
        double result = Bloco2.emiteIndicePoluicao(indice);
        assertEquals(expected,result);

    }


    @Test
    void imprimeAvisoComBaseNoNivel() {

        double nivel = -2;
        String expected = "Indices têm que ser maior que zero !";
        String result = Bloco2.imprimeAvisoComBaseNoNivel(nivel);
        assertEquals(expected,result);
    }

    @Test
    void imprimeAvisoComBaseNoNivelCaso2() {

        double nivel = 0.2;
        String expected = "É aceitável";
        String result = Bloco2.imprimeAvisoComBaseNoNivel(nivel);
        assertEquals(expected,result);
    }

    @Test
    void imprimeAvisoComBaseNoNivelCaso3() {

        double nivel = 0.35;
        String expected = " As indústrias do 1º grupo são intimadas a suspenderem as suas atividades";
        String result = Bloco2.imprimeAvisoComBaseNoNivel(nivel);
        assertEquals(expected,result);
    }

    @Test
    void imprimeAvisoComBaseNoNivelCaso4() {

        double nivel = 0.41;
        String expected = " As indústrias do 1º e 2º grupo são intimadas a suspenderem as suas atividades";
        String result = Bloco2.imprimeAvisoComBaseNoNivel(nivel);
        assertEquals(expected,result);
    }

    @Test
    void imprimeAvisoComBaseNoNivelCaso5() {

        double nivel = 0.51;
        String expected = "Os 3 grupos devem ser notificados a paralisarem as suas atividades.";
        String result = Bloco2.imprimeAvisoComBaseNoNivel(nivel);
        assertEquals(expected,result);
    }

    @Test
    void getTempoJardinagem()
    {
        double aGrama =0;
        double nArvores=1;
        double nArbustos=0;
        double expected =1;
        double result = Bloco2.getTempoJardinagem(aGrama,nArvores,nArbustos);

        assertEquals(expected,result);

    }

    @Test
    void getTempoJardinagemCaso2()
    {
        double aGrama =3;
        double nArvores=2;
        double nArbustos=4;
        double expected =2;
        double result = Bloco2.getTempoJardinagem(aGrama,nArvores,nArbustos);

        assertEquals(expected,result);

    }


    @Test
    void getCustoJardinagem()
    {
        double aGrama =3;
        double nArvores=2;
        double nArbustos=4;
        double expected = 150;
        double result = Bloco2.getCustoJardinagem(aGrama,nArvores,nArbustos);


    }

    @Test
    void getCustoJardinagemCaso2()
    {
        double aGrama =3;
        double nArvores=0;
        double nArbustos=4;
        double expected = 100;
        double result = Bloco2.getCustoJardinagem(aGrama,nArvores,nArbustos);


    }


    @Test
    void getMediaPercorrida() {
        double d1=20;
        double d2 =20;
        double d3 =20;
        double d4 = 20;
        double d5 =20;
        double expected =32.18;
        double result = Bloco2.getMediaPercorrida(d1,d2,d3,d4,d5);
        assertEquals(expected,result,0.01);
    }


    @Test
    void classificacaoDeTriangulosPorLado() {
        double a =5;
        double b=0;
        double c =5;
        String expected= "Não é triangulo";
        String result = Bloco2.classificacaoDeTriangulosPorLado(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void classificacaoDeTriangulosPorLadoCaso2() {
        double a =5;
        double b=5;
        double c =5;
        String expected= "O triangulo é equilátero";
        String result = Bloco2.classificacaoDeTriangulosPorLado(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void classificacaoDeTriangulosPorLadoCaso3() {
        double a =5;
        double b=6;
        double c =5;
        String expected= "O triangulo é isósceles";
        String result = Bloco2.classificacaoDeTriangulosPorLado(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void classificacaoDeTriangulosPorLadoCaso4() {
        double a =5;
        double b=3;
        double c =7;
        String expected= "O triangulo é escaleno";
        String result = Bloco2.classificacaoDeTriangulosPorLado(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void classificacaoDeTriangulosPorLadoCaso5() {
        double a =5;
        double b=1;
        double c =7;
        String expected= "Não é triangulo";
        String result = Bloco2.classificacaoDeTriangulosPorLado(a,b,c);
        assertEquals(expected,result);

    }


    @Test
    void classificacaoDeTriangulosPorAngulos() {
        double a1 = 60;
        double a2 = 0;
        double a3 =60;
        String expected = "Não é triangulo";
        String result = Bloco2.classificacaoDeTriangulosPorAngulos(a1,a2,a3);
        assertEquals(expected, result);

    }

    @Test
    void classificacaoDeTriangulosPorAngulosCaso2() {
        double a1 = 60;
        double a2 = 60;
        double a3 =60;
        String expected = "O triangulo é acutangulo";
        String result = Bloco2.classificacaoDeTriangulosPorAngulos(a1,a2,a3);
        assertEquals(expected, result);

    }

    @Test
    void classificacaoDeTriangulosPorAngulosCaso3() {
        double a1 = 60;
        double a2 = 90;
        double a3 =300;
        String expected = "Não é triangulo";
        String result = Bloco2.classificacaoDeTriangulosPorAngulos(a1,a2,a3);
        assertEquals(expected, result);

    }


    @Test
    void classificacaoDeTriangulosPorAngulosCaso4() {
        double a1 = 60;
        double a2 = 90;
        double a3 = 30;
        String expected = "O triangulo é retangulo";
        String result = Bloco2.classificacaoDeTriangulosPorAngulos(a1,a2,a3);
        assertEquals(expected, result);

    }


    @Test
    void classificacaoDeTriangulosPorAngulosCaso5() {
        double a1 = 60;
        double a2 = 100;
        double a3 =20;
        String expected = "O triangulo é obtusangulo";
        String result = Bloco2.classificacaoDeTriangulosPorAngulos(a1,a2,a3);
        assertEquals(expected, result);

    }


    @Test
    void getHoraChegada() {

        int horaPartida= 10;
        int minutosPartida=0;
        int horaDuracao=1;
        int minutosDuracao=0;

        String expected = "Chegada ás 11:0 do dia de hoje";
        String result = Bloco2.getHoraChegada(horaPartida,minutosPartida,horaDuracao,minutosDuracao);
        assertEquals(expected, result);

        //Chegada ás" + hc + ":" + mc + "do dia de" + dia;
    }

    @Test
    void getHoraChegadaCaso2() {

        int horaPartida= 22;
        int minutosPartida=0;
        int horaDuracao=2;
        int minutosDuracao=30;

        String expected = "Chegada ás 0:30 do dia de amanha";
        String result = Bloco2.getHoraChegada(horaPartida,minutosPartida,horaDuracao,minutosDuracao);
        assertEquals(expected, result);


    }


    @Test
    void getHoraFimProcessamento() {

        int hi= 10;
        int mi= 30;
        int si =0;
        int sproc= 3660;
        String expected = "11:31:0";
        String result = Bloco2.getHoraFimProcessamento(hi,mi,si,sproc);

        assertEquals(expected,result);

    }

    @Test
    void getHoraFimProcessamentoCaso2() {

        int hi= 10;
        int mi= 30;
        int si =0;
        int sproc= 61;
        String expected = "10:31:1";
        String result = Bloco2.getHoraFimProcessamento(hi,mi,si,sproc);

        assertEquals(expected,result);

    }


    @Test
    void getSalarioSemanal() {

        int h = -1;
        double expected =0;
        double result = Bloco2.getSalarioSemanal(h);
        assertEquals(expected, result,0.01);
    }

    @Test
    void getSalarioSemanalCaso2() {

        int h = 41;
        double expected =320;
        double result = Bloco2.getSalarioSemanal(h);
        assertEquals(expected, result,0.01);
    }


    @Test
    void getSalarioSemanalCaso3() {

        int h = 42;
        double expected =335;
        double result = Bloco2.getSalarioSemanal(h);
        assertEquals(expected, result,0.01);
    }


    @Test
    void custoDeAluguer() {

        String tipoKit= "d";
        double diaUtil =1;
        double deslocacao = 0;

        String expected ="Parametros de entrada, dia util ou tipo de kit, não valido";
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }

    @Test
    void custoDeAluguerCaso2() {

        String tipoKit= "a";
        double diaUtil = 3;
        double deslocacao = 0;

        String expected ="Parametros de entrada, dia util ou tipo de kit, não valido";
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }

    @Test
    void custoDeAluguerCaso3() {

        String tipoKit="a";
        double diaUtil =1;
        double deslocacao = 0;



        String expected ="O custo é de 40 €" ;
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }

    @Test
    void custoDeAluguerCaso4() {

        String tipoKit="A";
        double diaUtil =0;
        double deslocacao = 2;

        String expected ="O custo é de 34 €" ;
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }


    @Test
    void custoDeAluguerCaso5() {

        String tipoKit="b";
        double diaUtil =0;
        double deslocacao = 0;

        String expected ="O custo é de 50 €" ;
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }


    @Test
    void custoDeAluguerCaso6() {

        String tipoKit="C";
        double diaUtil =1;
        double deslocacao = 0;

        String expected ="O custo é de 140 €" ;
        String result = Bloco2.custoDeAluguer(tipoKit,diaUtil,deslocacao);

        assertEquals(expected,result);

    }




}