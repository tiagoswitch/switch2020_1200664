package com.company;

import java.util.Scanner;

public class Bloco3Ex15
{
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        // O exercício pede valores do teclado, caso contrário podia fazer uma função a receber um array
        // e a retornar um de Strings

    double n =0;
    String resultado ="";

    do{
        System.out.print("Insira uma nota ");
        n= sc.nextDouble();

        if(n<0) resultado="Insere uma nota válida";
        else if(n<=4) resultado="Mau";
        else if(n<=9)resultado="Mediocre";
        else if(n<=13)resultado="Suficiente";
        else if(n<=17)resultado = "Bom";
        else resultado="Muito bom";

        System.out.print(" - isso é "+ resultado);
        System.out.println();
    }while (n>=0);


    }



}
