package com.company;

public class Bloco3Ex17 {
    public static void main(String[] args) {

    double []p = {6,0,46,0,1};
    double q = 100;

    String s= quantAdequadaRacao(p,q);
        System.out.println(s);

    }

    public static String quantAdequadaRacao(double[] peso, double quantidadeComida) {
        String classificação = "";
        String resposta = "";
        String resultado = "";



            for (int i = 0; i < peso.length; i++) {
                if (peso[i] < 10) classificação = "pequena";
                else if (peso[i] <= 25) classificação = "media";
                else if (peso[i] <= 45) classificação = "grande";
                else classificação = "gigante";


                if (classificação == "pequena" && quantidadeComida == 100) resposta = "é adequada";
                else if (classificação == "media" && quantidadeComida == 250) resposta = "é adequada";
                else if (classificação == "grande" && quantidadeComida == 350) resposta = "é adequada";
                else if (classificação == "gigante" && quantidadeComida == 450) resposta = "é adequada";
                else resposta = "não é adequada";

                resultado = resultado + "Para uma raça " + classificação + " a quantidade " + quantidadeComida + " " + resposta + " ; ";
                if(peso[i]<=0)return resultado;

            }

        return resultado;
    }

}