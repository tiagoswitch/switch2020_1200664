public class Rectangulo {

    private int comprimento =1;
    private int largura =1;


    public Rectangulo(int comprimento, int largura)
    {
        if(!eMedidaValida(comprimento))
        {
            throw new IllegalArgumentException("O comprimento tem que ser válida ");
        }
        if(!eMedidaValida(largura))
        {
            throw new IllegalArgumentException("A largura tem que ser válida");
        }


        this.comprimento = comprimento;
        this.largura = largura;

    }

    public int getPerimetro()
    {
        return 2*comprimento + 2*largura;
    }

    public boolean eMedidaValida(int medida)
    {
        return medida >=0;
    }

}
