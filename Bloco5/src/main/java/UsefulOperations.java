/*
Classe com métodos que foram realizados noutros Blocos
 */
public class UsefulOperations {


    public static void main(String[] args) {


    }

    /**
     * Método para contar os digitos
     *
     * @param value
     * @return
     */

    public static int countDigits(int value) {
        if (value == 0) {
            return 1;
        }
        int count = 0;
        while (value > 0) {
            value = value / 10;
            count++;
        }
        return count;
    }


    public static int countEvenDigits(int value) {
        int cont = 0;
        while (value > 0) {
            if ((value % 10) % 2 == 0) cont++;
            value = (value / 10);
        }
        return cont;
    }

    public static int countOddDigits(int value) {
        int cont = 0;
        while (value > 0) {
            if ((value % 10) % 2 != 0) cont++;
            value = (value / 10);
        }
        return cont;
    }

    /**
     * Método para calcular a média do número de algarismos de cada elemento do vector
     *
     * @param array
     * @return
     */

    public static double getAvgOfNumberOfDigits(int[] array) {
        double avg = 0;
        double sum = 0;
        int numDigits = 0;
        if (array == null || array.length == 0) {
            return 0;
        } else {
            for (int i = 0; i < array.length; i++) {
                numDigits = countDigits(array[i]);
                sum = sum + numDigits;
            }
            avg = sum / array.length;
        }
        return avg;
    }


    public static boolean isArmstrong(int element) {
        long numero = element;
        double cubo = 0;
        while (element > 0) {
            cubo = cubo + Math.pow(element % 10, 3);
            element = element / 10;
        }
        return cubo == numero;

    }


    public static double obterValorMedio(int[][] v) {
        double soma = 0;
        int cont = 0;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                soma = soma + v[i][j];
                cont++;
            }
        }
        return soma / cont;
    }


    public static int getBiggestValueOfMatrix(int[][] matrix) {
        int max = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempMax = getBiggestValueOfArray(matrix[i]);
                if (tempMax > max) {
                    max = tempMax;
                }
            }
        }
        return max;
    }

    public static int getBiggestValueOfArray(int[] numberArray) {
        int max = numberArray[0];
        for (int i = 1; i < numberArray.length; i++) {
            if (numberArray[i] > max) {
                max = numberArray[i];
            }
        }
        return max;
    }

    public static int getSmallestValueOfMatrix(int[][] matrix) {
        int smallest = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempSmallest = getSmallestValueOfArray(matrix[i]);
                if (tempSmallest < smallest) {
                    smallest = tempSmallest;
                }
            }
        }
        return smallest;
    }

    public static int getSmallestValueOfArray(int[] numberArray) {
        int smallest = numberArray[0];
        for (int i = 1; i < numberArray.length; i++) {
            if (numberArray[i] < smallest) {
                smallest = numberArray[i];
            }
        }
        return smallest;
    }

    public static int arrayInternalSum(int[] array) {
        int soma = 0;
        for (int i = 0; i < array.length; i++) {
            soma = soma + array[i];
        }
        return soma;
    }


    /**
     * Método para verificar se todas as linhas têm o mesmo número de colunas
     *
     * @param v
     * @return
     */
    public static boolean allRowsHaveTheSameNofColumns(int[][] v) {
        boolean resultado = false;
        if (v == null) {
            return false;
        }
        int width = v[0].length;
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (v.length == 0 || v[0].length == 0) {
                    return false;
                } else if (v[i].length != width) {
                    return false;
                } else {
                    resultado = true;
                }
            }
        }
        return resultado;
    }

    public static int[][] obterMatrizTransposta(int[][] v) {
        if (v == null || !(allRowsHaveTheSameNofColumns(v)) || v.length == 0) {
            return null;
        }
        int[][] transposta = new int[v[0].length][v.length];
        for (int i = 0; i < v.length; i++)
            for (int j = 0; j < v[i].length; j++)
                transposta[j][i] = v[i][j];
        return transposta;
    }

    public static int[] obterDiagonalPrincipalDaMatriz(int[][] v) {
        int k = 0;
        int[] diagonalMatriz = new int[v[0].length];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if (i == j) {
                    diagonalMatriz[k] = v[i][j];
                    k++;
                }
            }
        }
        return diagonalMatriz;
    }


    // Alínea h) Devolver a Diagonal Secundária
    public static int[] obterDiagonalSecundariaDaMatriz(int[][] v) {
        int k = 0;
        int[] diagonalSecundariaMatriz = new int[v[0].length];
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                if ((i + j) == ((v[0].length) - 1)) {
                    diagonalSecundariaMatriz[k] = v[i][j];
                    k++;
                }
            }
        }
        return diagonalSecundariaMatriz;
    }

    public static int[] reverseRow(int[] array) {
        int[] tempArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            tempArray[tempArray.length - 1 - i] = array[i];
        }
        return tempArray;
    }

    public static int[][] reverseColumnInMatrix(int[][] matrix) {
        int[][] tempArray = new int[matrix.length][matrix[0].length];
        int index = 0;
        for (int i = matrix.length - 1; i >= 0; i--) {
            for (int j = 0; j < matrix[i].length; j++) {
                tempArray[i][j] = matrix[index][j];
            }
            index++;
        }
        return tempArray;
    }
}
