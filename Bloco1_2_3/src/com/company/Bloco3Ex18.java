package com.company;

public class Bloco3Ex18 {

    public static void main(String[] args) {

        System.out.println(verificaCC(13572628, 4));
    }

    public static boolean verificaCC(long numeros, int digitoControlo)
    {

        double soma =0;
        double digito =0;

        numeros= numeros*10 + digitoControlo;

        for(int i =1; i<=9;i++)
        {
            digito = numeros % 10;
            soma = soma + digito*i;

            numeros= numeros/10;
        }

        if((soma ) % 11 ==0)return true;
        else return false;

    }
}
