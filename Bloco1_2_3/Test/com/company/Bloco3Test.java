package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco3Test {


    @Test
    void getFactorial() {
        int n =3;
        int expected =6;
        int result = Bloco3Ex1.getFactorial(n);
        assertEquals(expected,result,0.01);

    }

    @Test
    void GetFactorialTesteDois() {
        int n =-1;
        int expected =1;
        int result = Bloco3Ex1.getFactorial(n);
        assertEquals(expected,result,0.01);

    }

    @Test
    void getPercentEMedias() {
        int nAlunos = 4;
        double[] notas ={10,9,10,9};
        String expected = "Positivos 50 Negativos 9";
        String result = Bloco3Ex2.getPercentEMedias(notas,nAlunos);
        assertEquals(expected,result);
    }

    @Test
    void getPercentEMediasTestedois() {
        int nAlunos = 6;
        double[] notas ={10,9,11,9,12,9};
        String expected = "Positivos 50 Negativos 9";
        String result = Bloco3Ex2.getPercentEMedias(notas,nAlunos);
        assertEquals(expected,result);
    }


    @Test
    void getPercentagemPares() {

        int[] numeros = {10, 9, 8, 7, 5,0};
        double expected = 40.0;
        double result = Bloco3Ex3.getPercentagemPares(numeros);
        assertEquals(expected,result);

    }
    @Test
    void getPercentagemParesTesteDois() {

        int[] numeros = {0, 9, 8, 7, 5,0};
        double expected = 0.0;
        double result = Bloco3Ex3.getPercentagemPares(numeros);
        assertEquals(expected,result);

    }


    @Test
    void getMediaImpares() {

        int[] numeros = {10, 9, 8, 7, 5,0};
        double expected = 7.0;
        double result = Bloco3Ex3.getMediaImpares(numeros);
        assertEquals(expected,result);

    }

    @Test
    void getMediaImparesTeste2() {

        int[] numeros = {0, 9, 8, 7, 5,0};
        double expected = 0.0;
        double result = Bloco3Ex3.getMediaImpares(numeros);
        assertEquals(expected,result);

    }

    @Test
    void getMediaImparesTeste3() {

        int[] numeros = {10, 9, 8, 7, 5, 3,0};
        double expected = 6.0;
        double result = Bloco3Ex3.getMediaImpares(numeros);
        assertEquals(expected,result);

    }


    @Test
    void nMultiploDeTres() {
        int inicio =1;
        int fim =6;
        double expected = 2;
        double result = Bloco3Ex4.nMultiploDeTres(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void nMultiploDeTresTesteDois() {
        int inicio =1;
        int fim =2;
        double expected = 0;
        double result = Bloco3Ex4.nMultiploDeTres(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nMultiplosDeN() {
        int inicio =1;
        int fim =7;
        int n =2;
        double expected = 3;
        double result = Bloco3Ex4.nMultiplosDeN(inicio,fim,n);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosDeNTesteDois() {
        int inicio=1;
        int fim =3;
        int n =4;
        double expected = 0;
        double result = Bloco3Ex4.nMultiplosDeN(inicio,fim,n);
        assertEquals(expected,result);

    }


    @Test
    void nMultiplosDeTresECinco() {

        int inicio = 1;
        int fim = 15;
        double expected = 1;
        double result = Bloco3Ex4.nMultiplosDeTresECinco(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void nMultiplosDeTresECincoTesteDois() {

        int inicio = 1;
        int fim = 4;
        double expected = 0;
        double result = Bloco3Ex4.nMultiplosDeTresECinco(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nMultiplosDeDoisNum() {
        int inicio=2;
        int fim=8;
        int n =2;
        int m = 4;
        double expected = 2;
        double result = Bloco3Ex4.nMultiplosDeDoisNum(inicio,fim,n,m);
        assertEquals(expected,result);


    }

    @Test
    void nMultiplosDeDoisNumTesteDois() {
        int inicio = 1;
        int fim =3;
        int n =2;
        int m = 4;
        double expected = 0;
        double result = Bloco3Ex4.nMultiplosDeDoisNum(inicio,fim,n,m);
        assertEquals(expected,result);

    }


    @Test
    void somaMultiplosDeDoisNum() {

        int inicio = 1;
        int fim =12;
        int n =2;
        int m = 4;
        double expected = 24;
        double result = Bloco3Ex4.somaMultiplosDeDoisNum(inicio,fim,n,m);
        assertEquals(expected,result);


    }

    @Test
    void somaMultiplosDeDoisNumTesteDois() {

        int inicio = 2;
        int fim =9;
        int n =2;
        int m = 4;
        double expected = 12;
        double result = Bloco3Ex4.somaMultiplosDeDoisNum(inicio,fim,n,m);
        assertEquals(expected,result);


    }

    @Test
    void somaMultiplosDeDoisNumTesteTres() {

        int inicio = 2;
        int fim = 30;
        int n =17;
        int m = 11;
        double expected = 0;
        double result = Bloco3Ex4.somaMultiplosDeDoisNum(inicio,fim,n,m);
        assertEquals(expected,result);


    }


    @Test
    void somaDePares() {
        int inicio =1;
        int fim = 5;
        double expected = 6.0;
        double result = Bloco3Ex5.somaDePares(inicio,fim);
        assertEquals(expected,result);

    }

    void somaDeParesTeste2() {
        int inicio =5;
        int fim = 10;
        double expected = 24.0;
        double result = Bloco3Ex5.somaDePares(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nDePares() {
        int inicio =5;
        int fim = 10;
        double expected = 3.0;
        double result = Bloco3Ex5.nDePares(inicio,fim);
        assertEquals(expected,result);


    }

    @Test
    void nDeParesTesteDois() {
        int inicio =3;
        int fim = 3;
        double expected = 0.0;
        double result = Bloco3Ex5.nDePares(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void somaDeImpares() {
        int inicio =1;
        int fim = 5;
        double expected = 9.0;
        double result = Bloco3Ex5.somaDeImpares(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void somaDeImparesTesteDois() {
        int inicio =10;
        int fim = 13;
        double expected = 11+13;
        double result = Bloco3Ex5.somaDeImpares(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nDeImpares() {
        int inicio =1;
        int fim =5;
        double expected = 3.0;
        double result = Bloco3Ex5.nDeImpares(inicio,fim);
        assertEquals(expected,result);
    }

    @Test
    void nDeImparesTesteDois() {
        int inicio =2;
        int fim =2;
        double expected = 0.0;
        double result = Bloco3Ex5.nDeImpares(inicio,fim);
        assertEquals(expected,result);
    }


    @Test
    void somaDeMultiplosDeUmIntervalo() {
        int inicio =2;
        int fim =5;
        int n=2;
        double expected = 6.0;
        double result = Bloco3Ex5.somaDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);

    }

    @Test
    void somaDeMultiplosDeUmIntervaloTesteDois() {
        int inicio =2;
        int fim =5;
        int n=7;
        double expected = 0.0;
        double result = Bloco3Ex5.somaDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);

    }


    @Test
    void produtoDeMultiplosDeUmIntervalo() {
        int inicio =2;
        int fim =7;
        int n=2;
        double expected = 48.0;
        double result = Bloco3Ex5.produtoDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);

    }

    @Test
    void produtoDeMultiplosDeUmIntervaloTesteDois() {
        int inicio =2;
        int fim =7;
        int n=11;
        double expected = 0.0;
        double result = Bloco3Ex5.produtoDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);

    }


    @Test
    void mediaDeMultiplosDeUmIntervalo() {

        int inicio =2;
        int fim =7;
        int n=2;
        double expected = 4.0;
        double result = Bloco3Ex5.mediaDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);


    }

    @Test
    void mediaDeMultiplosDeUmIntervaloTesteDois() {

        int inicio =2;
        int fim =7;
        int n=11;
        double expected = 0.0;
        double result = Bloco3Ex5.mediaDeMultiplosDeUmIntervalo(inicio,fim,n);
        assertEquals(expected,result);


    }


    @Test
    void mediaDeDeDoisMultiplosNumIntervalo() {

        int inicio =2;
        int fim =8;
        int n=2;
        int m =3;
        double expected = 4.6;
        double result = Bloco3Ex5.mediaDeDeDoisMultiplosNumIntervalo(inicio,fim,n,m);
        assertEquals(expected,result);

    }

    @Test
    void mediaDeDeDoisMultiplosNumIntervaloTeste2() {

        int inicio =2;
        int fim =8;
        int n=11;
        int m =13;
        double expected = 0;
        double result = Bloco3Ex5.mediaDeDeDoisMultiplosNumIntervalo(inicio,fim,n,m);
        assertEquals(expected,result);

    }


    @Test
    void nAlgarismos() {
        int n = 234;
        int expected=3;
        int result = Bloco3Ex6.nAlgarismos(n);

        assertEquals(expected,result);

    }

    @Test
    void nAlgarismosTesteDois() {
        int n = 0;
        int expected=1;
        int result = Bloco3Ex6.nAlgarismos(n);

        assertEquals(expected,result);

    }


    @Test
    void nAlgarismosPares() {
        int n = 1;
        int expected=0;
        int result = Bloco3Ex6.nAlgarismosPares(n);

        assertEquals(expected,result);

    }

    @Test
    void nAlgarismosParesTesteDois() {
        int n = 124;
        int expected=2;
        int result = Bloco3Ex6.nAlgarismosPares(n);

        assertEquals(expected,result);

    }


    @Test
    void nAlgarismosImpares() {

        int n = 124115;
        int expected=4;
        int result = Bloco3Ex6.nAlgarismosImpares(n);

        assertEquals(expected,result);


    }

    @Test
    void nAlgarismosImparesTesteDois() {

        int n = 224;
        int expected=0;
        int result = Bloco3Ex6.nAlgarismosImpares(n);

        assertEquals(expected,result);


    }


    @Test
    void somaAlgarismos() {

        int n = 224;
        int expected=8;
        int result = Bloco3Ex6.somaAlgarismos(n);

        assertEquals(expected,result);


    }

    @Test
    void somaAlgarismosTesteDois() {

        int n = 100;
        int expected=1;
        int result = Bloco3Ex6.somaAlgarismos(n);

        assertEquals(expected,result);

    }


    @Test
    void somaAlgarismosPares() {
        int n = 124;
        int expected=6;
        int result = Bloco3Ex6.somaAlgarismosPares(n);

        assertEquals(expected,result);

    }
    @Test
    void somaAlgarismosParesTesteDois() {
        int n = 111;
        int expected=0;
        int result = Bloco3Ex6.somaAlgarismosPares(n);

        assertEquals(expected,result);

    }


    @Test
    void somaAlgarismosImpares() {

        int n = 222;
        int expected=0;
        int result = Bloco3Ex6.somaAlgarismosImpares(n);

        assertEquals(expected,result);

    }

    @Test
    void somaAlgarismosImparesTesteDois() {

        int n = 1235;
        int expected=9;
        int result = Bloco3Ex6.somaAlgarismosImpares(n);

        assertEquals(expected,result);

    }


    @Test
    void mediaDeAlgarismos() {

        int n = 1234;
        double expected=2.5;
        double result = Bloco3Ex6.mediaDeAlgarismos(n);

        assertEquals(expected,result);


    }

    @Test
    void mediaDeAlgarismosTesteDois() {

        int n = 123402;
        double expected=2;
        double result = Bloco3Ex6.mediaDeAlgarismos(n);

        assertEquals(expected,result);


    }


    @Test
    void mediaDeAlgarismosPares() {

        int n = 17353;
        double expected=0.0;
        double result = Bloco3Ex6.mediaDeAlgarismosPares(n);

        assertEquals(expected,result);

    }

    @Test
    void mediaDeAlgarismosParesTesteDois() {

        int n = 12346;
        double expected=4;
        double result = Bloco3Ex6.mediaDeAlgarismosPares(n);

        assertEquals(expected,result);

    }


    @Test
    void mediaDeAlgarismosImpares() {

        int n = 12346;
        double expected=2;
        double result = Bloco3Ex6.mediaDeAlgarismosImpares(n);

        assertEquals(expected,result);

    }

    @Test
    void mediaDeAlgarismosImparesCasoDois() {

        int n = 22846;
        double expected=0;
        double result = Bloco3Ex6.mediaDeAlgarismosImpares(n);

        assertEquals(expected,result);

    }


    @Test
    void ordemInversa() {
        long n = 22846;
        double expected=64822;
        double result = Bloco3Ex6.ordemInversa(n);

        assertEquals(expected,result);


    }

    @Test
    void ordemInversaTesteDois() {
        long n = 1;
        double expected=1;
        double result = Bloco3Ex6.ordemInversa(n);

        assertEquals(expected,result);


    }


    @Test
    void isCapicua() {

        long n = 121;
        boolean expected= true;
        boolean result = Bloco3Ex7.isCapicua(n);

        assertEquals(expected,result);

    }

    @Test
    void isCapicuaCasoDois() {

        long n = 122;
        boolean expected= false;
        boolean result = Bloco3Ex7.isCapicua(n);

        //OU USAMOS ASSERT TRUE
        assertEquals(expected,result);

    }


    @Test
    void isArmstrong() {
        long n = 122;
        boolean expected= false;
        boolean result = Bloco3Ex7.isArmstrong(n);

        assertEquals(expected,result);
    }

    @Test
    void isArmstrongTesteDois() {
        long n = 153;
        boolean expected= true;
        boolean result = Bloco3Ex7.isArmstrong(n);

        assertEquals(expected,result);
    }


    @Test
    void firstCapicua() {
        int inicio = 119;
        int fim = 132;
        int expected= 121;
        int result = Bloco3Ex7.firstCapicua(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void firstCapicuaTesteDois() {
        int inicio = 122;
        int fim = 130;
        int expected= 0;
        int result = Bloco3Ex7.firstCapicua(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void maiorCapicua() {
        int inicio = 119;
        int fim = 142;
        int expected= 141;
        int result = Bloco3Ex7.maiorCapicua(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void maiorCapicuaTesteDois() {
        int inicio = 122;
        int fim = 130;
        int expected= 0;
        int result = Bloco3Ex7.maiorCapicua(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nCapicuas() {

        int inicio = 122;
        int fim = 130;
        int expected= 0;
        int result = Bloco3Ex7.nCapicuas(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nCapicuasTesteDois() {

        int inicio = 119;
        int fim = 142;
        int expected= 3;
        int result = Bloco3Ex7.nCapicuas(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void firstArmstrong() {

        int inicio = 150;
        int fim = 200;
        int expected= 153;
        int result = Bloco3Ex7.firstArmstrong(inicio,fim);
        assertEquals(expected,result);

    }

    @Test
    void firstArmstrongTesteDois() {

        int inicio = 160;
        int fim = 300;
        int expected= 0;
        int result = Bloco3Ex7.firstArmstrong(inicio,fim);
        assertEquals(expected,result);

    }


    @Test
    void nArmstrong() {

        int inicio = 10;
        int fim =372;
        int expected =3;
        int result =Bloco3Ex7.nArmstrong(inicio,fim);

    }

    @Test
    void nArmstrongTesteDois() {

        int inicio = 160;
        int fim =360;
        int expected =0;
        int result =Bloco3Ex7.nArmstrong(inicio,fim);

    }


    @Test
    void devolveOmenor() {

        int []n = {2,3,4,7,8,9,1};
        int escolhido =7;
        int expected=2;
        int result = Bloco3Ex8.devolveOmenorSeMaiorQueaSoma(n,escolhido);
        assertEquals(expected,result);

    }


    @Test
    void devolveOmenorTesteDois() {

        int []n = {2,3,4,7,8,9,1,4};
        int escolhido =33;
        int expected=1;
        int result = Bloco3Ex8.devolveOmenorSeMaiorQueaSoma(n,escolhido);
        assertEquals(expected,result);

    }


    @Test
    void salarioMensal() {

        double [] s={100,100,100,100,100};
        double [] h={5,5,5,5,5};

        double[] expected={110,110,110,110,110};
        double [] result = Bloco3Ex9.salarioMensal(s,h);
        assertArrayEquals(expected,result);

    }

    @Test
    void salarioMensalTesteDois() {

        double [] s={1200,700,800,850,770};
        double [] h={5,5,5,5,6};

        double[]expected={1320,770,880,935,862.4};
        double [] result = Bloco3Ex9.salarioMensal(s,h);
        assertArrayEquals(expected,result);

    }


    @Test
    void mediaDeSalariosPagos() {

        double [] s={1100,400, 550, 660, 700};
        double expected =682;
        double result = Bloco3Ex9.mediaDeSalariosPagos(s);
        assertEquals(expected,result);

    }

    @Test
    void mediaDeSalariosPagosTesteDois() {

        double [] s={1100,400, 550, 0, 700,250};
        double expected =500;
        double result = Bloco3Ex9.mediaDeSalariosPagos(s);
        assertEquals(expected,result);

    }


    @Test
    void mediaDeSalariosPagosTesteTres() {

        double [] s={0,0,0,0,0};
        double expected =0;
        double result = Bloco3Ex9.mediaDeSalariosPagos(s);
        assertEquals(expected,result);

    }


    @Test
    void devolveManeirasdeObterUmNum() {

        int n =15;
        String expected = "Existem 3 maneiras 5+10,6+9,7+8,";
        String result = Bloco3Ex11.devolveManeirasdeObterUmNum(n);
        assertEquals(expected,result);

    }

    @Test
    void devolveManeirasdeObterUmNumTesteDois() {

        int n =25;
        String expected = "Número inválido";
        String result = Bloco3Ex11.devolveManeirasdeObterUmNum(n);
        assertEquals(expected,result);

    }

    @Test
    void testDevolveManeirasdeObterUmNum()
    {
        int n=15;
        String expected = "Existem 3 maneiras 5+10,6+9,7+8,";
        String result = Bloco3Ex11.devolveManeirasdeObterUmNum(n);
        assertEquals(expected,result);

    }


    @Test
    void testDevolveManeirasdeObterUmNumTesteDois()
    {
        int n=10;
        String expected = "Existem 6 maneiras 0+10,1+9,2+8,3+7,4+6,5+5,";
        String result = Bloco3Ex11.devolveManeirasdeObterUmNum(n);
        assertEquals(expected,result);

    }




    @Test
    void formulaResolvente() {
        double a=1;
        double b =4;
        double c=4;

        String expected ="A equação tem raíz dupla que é -2.0";
        String result = Bloco3Ex12.formulaResolvente(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void formulaResolventeTesteDois() {
        double a=1;
        double b =0;
        double c=1;

        String expected ="A Equação não tem soluções reais";
        String result = Bloco3Ex12.formulaResolvente(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void formulaResolventeTesteDoisTesteTres() {
        double a=0;
        double b =1;
        double c=1;

        String expected ="A equação não é de segundo grau";
        String result = Bloco3Ex12.formulaResolvente(a,b,c);
        assertEquals(expected,result);

    }

    @Test
    void formulaResolventeTesteDoisTesteQuatro() {
        double a=1;
        double b =-5;
        double c=6;

        String expected ="As soluções são 2.0 e 3.0.";
        String result = Bloco3Ex12.formulaResolvente(a,b,c);
        assertEquals(expected,result);

    }


    @Test
    void taxaDeCambio() {
        double[] euros={10,10,10,10};
        String opCambio="J";
        double [] expected = {0,0,0,0};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }

    @Test
    void taxaDeCambioTesteDois() {
        double[] euros={10,10,10,10};
        String opCambio="D";
        double [] expected = {15.34,15.34,15.34,15.34};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }

    @Test
    void taxaDeCambioTesteTres() {
        double[] euros={10,10,10,10};
        String opCambio="L";
        double [] expected = {7.74,7.74,7.74,7.74};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }

    @Test
    void taxaDeCambioTesteQuatro() {
        double[] euros={10,10,10,10};
        String opCambio="I";
        double [] expected = {1614.8,1614.8,1614.8,1614.8};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }

    @Test
    void taxaDeCambioTesteCinco() {
        double[] euros={10,10,10,10};
        String opCambio="CS";
        double [] expected = {95.93,95.93,95.93,95.93};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }

    @Test
    void taxaDeCambioTestesSeis() {
        double[] euros={10,10,10,10};
        String opCambio="FS";
        double [] expected = {16.009999999999998,16.009999999999998,16.009999999999998,16.009999999999998};
        double [] result = Bloco3Ex14.taxaDeCambio(euros,opCambio);
        assertArrayEquals(expected,result);

    }


    @Test
    void salarioLiquido() {
        double n=-1;
        double expected =-1;
        double result = Bloco3Ex16.salarioLiquido(n);
        assertEquals(expected,result);

    }
    @Test
    void salarioLiquidoTesteDois() {
        double n=100;
        double expected =90;
        double result = Bloco3Ex16.salarioLiquido(n);
        assertEquals(expected,result);

    }

    @Test
    void salarioLiquidoTesteTres() {
        double n=600;
        double expected =510;
        double result = Bloco3Ex16.salarioLiquido(n);
        assertEquals(expected,result);

    }

    @Test
    void salarioLiquidoTesteQuatro() {
        double n=1200;
        double expected =960;
        double result = Bloco3Ex16.salarioLiquido(n);
        assertEquals(expected,result);

    }


    @Test
    void verificaCC() {

        long n = 14850445;
        int dc= 0;
        boolean expected = true;
        boolean result = Bloco3Ex18.verificaCC(n,dc);

        assertEquals(expected, result);

    }

    @Test
    void verificaCCTesteDois() {

        long n = 12345678;
        int dc= 1;
        boolean expected = false;
        boolean result = Bloco3Ex18.verificaCC(n,dc);

        assertEquals(expected, result);

    }


    @Test
    void organizarParesADireita() {
        int n = 123456;
        int expected=531642;
        int result= Bloco3Ex19.organizarParesADireita(n);
        assertEquals(expected,result);
        
    }

    @Test
    void organizarParesADireitaTesteDois() {
        int n = 27654;
        int expected=57462;
        int result= Bloco3Ex19.organizarParesADireita(n);
        assertEquals(expected,result);

    }

    @Test
    void organizarParesADireitaTesteTres() {
        int n = 135;
        int expected=531;
        int result= Bloco3Ex19.organizarParesADireita(n);
        assertEquals(expected,result);

    }

    @Test
    void organizarParesADireitaTesteQuatro() {
        int n = 246;
        int expected=642;
        int result= Bloco3Ex19.organizarParesADireita(n);
        assertEquals(expected,result);

    }


    @Test
    void classificarN() {
        int n =-2;
        String expected = "Insere um positivo";
        String result = Bloco3Ex20.classificarN(n);
        assertEquals(expected,result);

    }

    @Test
    void classificarNTesteDois() {
        int n =6;
        String expected = " É Perfeito porque 6= 1+2+3";
        String result = Bloco3Ex20.classificarN(n);
        assertEquals(expected,result);

    }

    @Test
    void classificarNTesteTres() {
        int n =9;
        String expected = " É Reduzido porque 9>1+3";
        String result = Bloco3Ex20.classificarN(n);
        assertEquals(expected,result);

    }

    @Test
    void classificarNTesteQuatro() {
        int n =13;
        String expected = " É Reduzido porque 13>1";
        String result = Bloco3Ex20.classificarN(n);
        assertEquals(expected,result);

    }







}