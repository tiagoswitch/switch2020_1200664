import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvaliacaoFormativaITest {

    @org.junit.jupiter.api.Test
    void stringParaArrayDeChars() {

        String palavra="";
        char[] expected={};
        char [] result= AvaliacaoFormativaI.stringParaArrayDeChars(palavra);
        assertArrayEquals(expected,result);

    }

    void stringParaArrayDeCharsTesteDois() {

        String palavra="d";
        char[] expected={'d'};
        char [] result= AvaliacaoFormativaI.stringParaArrayDeChars(palavra);
        assertArrayEquals(expected,result);

    }


    @Test
    void calcularJanelaParaCorrespondencia() {
        String palavra1="";
        String palavra2="";
        double expected = 0;
        double result = AvaliacaoFormativaI.calcularJanelaParaCorrespondencia(palavra1,palavra2);
        assertEquals(expected,result);

    }

    @Test
    void calcularJanelaParaCorrespondenciaTesteDois() {
        String palavra1="amarrar";
        String palavra2="amarrar";
        double expected = 2;
        double result = AvaliacaoFormativaI.calcularJanelaParaCorrespondencia(palavra1,palavra2);
        assertEquals(expected,result);

    }


    @Test
    void calcularJanelaParaCorrespondenciaTesteTres() {
        String palavra1="amarrar";
        String palavra2="amarrado";
        int expected = 3;
        double result = AvaliacaoFormativaI.calcularJanelaParaCorrespondencia(palavra1,palavra2);
        assertEquals(expected,result);

    }


    @Test
    void validarMedidasDeProcura() {
        char[] p1={};
        char[] p2={};
        int medida= 4;
        boolean expected = false;
        boolean result = AvaliacaoFormativaI.validarMedidasDeProcura(medida,p1,p2);
        assertEquals(expected,result);

    }

    @Test
    void validarMedidasDeProcuraTesteDois() {
        char[] p1={'a','b','t'};
        char[] p2={'f','h','h'};
        int medida= 2;
        boolean expected = true;
        boolean result = AvaliacaoFormativaI.validarMedidasDeProcura(medida,p1,p2);
        assertEquals(expected,result);

    }

    @Test
    void validarMedidasDeProcuraTesteTres() {
        char[] p1={'a','b','t'};
        char[] p2={'f','h','h'};
        int medida= -3;
        boolean expected = false;
        boolean result = AvaliacaoFormativaI.validarMedidasDeProcura(medida,p1,p2);
        assertEquals(expected,result);

    }
    @Test
    void validarMedidasDeProcuraTesteQuatro() {
        char[] p1={'a','b','t'};
        char[] p2={'f','h','h'};
        int medida= 5;
        boolean expected = false;
        boolean result = AvaliacaoFormativaI.validarMedidasDeProcura(medida,p1,p2);
        assertEquals(expected,result);

    }



    @Test
    void calcularNumDeCaracteresCorrespondentes() {

        String s1= "bar";
        String s2="mar";
        int expected=2;
        int result = AvaliacaoFormativaI.calcularNumDeCaracteresCorrespondentes(s1,s2);
        assertEquals(expected,result);

    }

    @Test
    void calcularNumDeCaracteresCorrespondentesTesteDois() {

        String s1= "";
        String s2="mar";
        int expected=-1;
        int result = AvaliacaoFormativaI.calcularNumDeCaracteresCorrespondentes(s1,s2);
        assertEquals(expected,result);

    }







}