package com.company;

import java.util.Scanner;

public class Bloco3Ex1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Insira um nº :");
        int n = sc.nextInt();

        int r = getFactorial(n);
        System.out.println(r);

    }

    // Exercicio 1
    public static int getFactorial(int num) {
        int x,res = 1;
        for(x=num; x>1; x--){
            res = res*x;
        }
        return res;
    }



}
