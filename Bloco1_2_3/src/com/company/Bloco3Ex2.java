package com.company;
import java.util.Scanner;

public class Bloco3Ex2 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int nalunos=0;

        do
            {
                System.out.println("Insira o n de alunos : ");
                nalunos = sc.nextInt();
            }while(nalunos<=0);


        double[] notas = new double[nalunos];
        for (int j = 0; j < notas.length; j++) {
            System.out.println("Insira uma nota");
            notas[j] = sc.nextInt();
        }

        String r = getPercentEMedias(notas, nalunos);


        System.out.println(r);

    }

    public static String getPercentEMedias(double[] notas, int nalunos) {
        double somaNegativos = 0;
        double nNegativos = 0;
        double nPositivos = 0;
        for (int i = 0; i < nalunos; i++) {

            double nota = notas[i];
            if (nota >= 10) {
                nPositivos += 1;
            } else {
                nNegativos += 1;
                somaNegativos += nota;
            }
        }
        int mediaNegativos = (int) (somaNegativos / nNegativos);
        int percentagemPos = (int) (nPositivos / nalunos * 100);

        String result = "Positivos " + percentagemPos + " Negativos " + mediaNegativos;
        return result;

    }

}
