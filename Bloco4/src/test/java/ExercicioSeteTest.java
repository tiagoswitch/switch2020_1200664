import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void multiplosDeTresNumInt() {
        int inicio =3;
        int fim =10;
        int [] expected ={3,6,9};
        int[] result = ExercicioSete.multiplosDeTresNumInt(inicio,fim);
        assertArrayEquals(expected,result);

    }

    @Test
    void multiplosDeTresNumIntTesteDois() {
        int inicio =11;
        int fim =4;
        int [] expected ={6,9};
        int[] result = ExercicioSete.multiplosDeTresNumInt(inicio,fim);
        assertArrayEquals(expected,result);

    }

    @Test
    void multiplosDeTresNumIntTesteTres() {
        int inicio =13;
        int fim =14;
        int [] expected ={};
        int[] result = ExercicioSete.multiplosDeTresNumInt(inicio,fim);
        assertArrayEquals(expected,result);

    }


    @Test
    void multiplosDeNnumInt() {
        int inicio =1;
        int fim =10;
        int n =2;
        int[]expected = {2,4,6,8,10};
        int[] result = ExercicioSete.multiplosDeNnumInt(inicio,fim,n);
        assertArrayEquals(expected,result);

    }


    @Test
    void multiplosDeNnumIntTesteDois() {
        int inicio =1;
        int fim =10;
        int n =0;
        int[]expected = {};
        int[] result = ExercicioSete.multiplosDeNnumInt(inicio,fim,n);
        assertArrayEquals(expected,result);
}


    @Test
    void multiplosDeNnumIntTesteTres() {
        int inicio =1;
        int fim =10;
        int n =3;
        int[]expected = {3,6,9};
        int[] result = ExercicioSete.multiplosDeNnumInt(inicio,fim,n);
        assertArrayEquals(expected,result);

    }

}