import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @Test
    void contarTamanho() {
        int n = 12561;
        int expected =5;
        int result= ExercicioUm.contarNumDigitos(n);
        assertEquals(expected,result);

    }

    @Test
    void contarTamanhoTesteDois() {
        int n=1;
        int expected =1;
        int result= ExercicioUm.contarNumDigitos(n);
        assertEquals(expected,result);

    }

    @Test
    void contarTamanhoTesteTres() {
        int n=123;
        int expected =3;
        int result= ExercicioUm.contarNumDigitos(n);
        assertEquals(expected,result);

    }

    @Test
    void contarTamanhoTesteQuatro() {
        int n=-1;
        int expected =0;
        int result= ExercicioUm.contarNumDigitos(n);
        assertEquals(expected,result);

    }





}