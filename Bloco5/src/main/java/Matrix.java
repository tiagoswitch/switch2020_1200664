import java.util.Arrays;

public class Matrix {

    private final Vector[] matrix;

    /**
     * Construtor público em que o array fica vazio.
     */
    public Matrix() {
        Vector[] newVector = new Vector[0];
        this.matrix = newVector;
    }

    /**
     * Construtor que permite inicializar o array com alguns vectores do tipo Vector
     *
     * @param initialMatrix
     */

    public Matrix(Vector[] initialMatrix) {
        if (initialMatrix == null) {
            throw new NullPointerException("A Matrix não pode ser null");
        }
        Vector[] newVector = new Vector[initialMatrix.length];
        for (int i = 0; i < initialMatrix.length; i++) {
            newVector[i] = initialMatrix[i];
        }
        this.matrix = newVector;
    }

    /**
     * Construtor que permite inicializar o array com alguns vectores do tipo int []
     *
     * @param newMatrix
     */
    public Matrix(int[][] newMatrix) {
        if (newMatrix == null) {
            throw new NullPointerException(" A matriz não pode ser nula ");
        }
        Vector[] newVector = new Vector[newMatrix.length];
        for (int i = 0; i < newMatrix.length; i++) {
            Vector vector = new Vector(newMatrix[i]);
            newVector[i] = vector;
        }
        this.matrix = newVector;
    }

    /**
     * Método para retornar em formato int [][]
     * @return
     */

    public int[][] toMatrix() {
        int[][] tempMatrix = new int[this.matrix.length][];
        for (int i = 0; i < this.matrix.length; i++) {
            tempMatrix[i] = new int[this.matrix[i].length()];
            for (int j = 0; j < this.matrix[i].length(); j++) {
                tempMatrix[i][j] = this.matrix[i].getValueFromIndex(j);
            }
        }
        return tempMatrix;
    }

    public int length() {
        int count = 0;
        for (Vector elements : this.matrix) {
            count++;
        }
        return count;
    }

    private boolean checkIfisEmpty() {
        return this.matrix.length == 0;
    }

    private void isEmptyMatrix()
    {
        if(this.matrix.length==0)
        {
            throw new IllegalArgumentException("A MATRIZ NÃO PODE SER VAZIA");
        }

    }


    private boolean isInsideBounds(int row) {
        return row >= 0 && row < matrix.length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(o == null || this.getClass() != o.getClass()) return false;
        Matrix otherMatrix = (Matrix) o;
        if(this.matrix.length != otherMatrix.matrix.length) return false;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix1 = (Matrix) o;
        return Arrays.equals(matrix, matrix1.matrix);
    }


    @Override
    public int hashCode() {
        return Arrays.hashCode(matrix);
    }

    /**
     * Método para adicionar um valor a uma linha específica
     * Se a linha for inválida, o método gera uma excepção
     *
     * @param row
     * @param element
     */
    public void addElementInRow(int row, int element) {
        if (matrix.length == 0) {
            this.matrix[0].addElem(element);
        }
        if (!isInsideBounds(row)) {
            throw new IndexOutOfBoundsException("Linha Inválida !");
        }
        this.matrix[row].addElem(element);
    }


    /**
     * Método para verificar se o valor está na matriz
     * @param value
     * @return
     */
    private boolean valueIsInTheMatrix(int value) {
        boolean isFound = false;
        for (int i = 0; i < this.matrix.length && !isFound; i++) {
            Vector row = this.matrix[i];
            if (row.checkIfElemIsInArray(value)) {
                isFound = true;
            } else {
                isFound = false;
            }
        }
        return isFound;
    }

    /**
     * Método para remover o primeiro valor que encontra da matriz
     * Se não o encontrar, gera uma excepção
     *
     * @param value
     */

    public void removeElementInMatrix(int value) {
        if (checkIfisEmpty()) {
            throw new IllegalArgumentException("The matrix is empty");
        }
            boolean hasChanged = false;
            for (int i = 0; i < this.matrix.length && !hasChanged; i++) {
                int firstLength = this.matrix[i].length();
                if (matrix[i].checkIfElemIsInArray(value))
                {
                    this.matrix[i].removefirstElem(value);
                    hasChanged = true;
                }
            }
       }


    /**
     * Método para encontrar o maior valor
     * @return
     */
    public int biggestOnMatrix()
    {
        isEmptyMatrix();
        int highest;
        Vector biggest = new Vector();
        for (Vector row: this.matrix) {
            biggest.addElem(row.getTheBiggestValue());
        }
        highest=biggest.getTheBiggestValue();
        return highest;
    }

    /**
     * Método para obter o valor mais
     * @return
     */
    public int smallestOnMatrix()
    {
        isEmptyMatrix();
        int smallest;
        Vector smallVector = new Vector();
        for (Vector row: this.matrix) {
            smallVector.addElem(row.getTheLowestValue());
        }
        smallest=smallVector.getTheLowestValue();
        return smallest;
    }

    private int valueInPosition(int row, int column)
    {
        return this.matrix[row].getValueFromIndex(column);
    }



    private int getSumOfAllValues() {
        int sum = 0;

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length(); j++) {
                sum += this.matrix[i].getValueFromIndex(j);
            }
        }
        return sum;
    }

    private int getNumberOfMatrixElements() {
        int number = 0;

        for (int i = 0; i < this.matrix.length; i++) {
            number += this.matrix[i].length();
        }
        return number;
    }

    /**
     * ét
     * @return
     */
    public double avgOfMatrixValues()
    {
        isEmptyMatrix();
        double avg = (double) getSumOfAllValues() / getNumberOfMatrixElements();

        return avg;
    }

    /**
     * Método que retorna num VECTOR as somas internas das suas respectivas linhas
     * @return
     */
    public Vector getInternalSumsOfMatrixRows()
    {
        isEmptyMatrix();
        Vector internalSums = new Vector();
        for (Vector row: this.matrix) {
            internalSums.addElem(row.getSumOfArrayValues());
        }
        return internalSums;
    }








}











