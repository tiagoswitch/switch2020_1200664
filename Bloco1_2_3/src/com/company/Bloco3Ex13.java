package com.company;

import java.util.Scanner;

public class Bloco3Ex13 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n=0;
        String classificação = "";

        do{

            System.out.println("Insira o código");
            n = sc.nextInt();


            if(n == 1) classificação="Alimento não perceptível";
            else if(n>=2 && n<=4)classificação="Alimento perceptível";
            else if(n>=5 && n<=6)classificação="Vestuário";
            else if(n==7)classificação="Higiene Pessoal";
            else if(n>=8 && n<=15)classificação="Limpeza e utensílios domésticos";
            else classificação= "Código inválido";

            System.out.print("O "+n+" corresponde a "+classificação);
            System.out.println();


        }while(n>0);



    }




}
