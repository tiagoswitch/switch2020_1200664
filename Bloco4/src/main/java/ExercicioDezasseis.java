public class ExercicioDezasseis {
    public static void main(String[] args) {

    }

    /**
     * Recebe uma matriz e devolve o determinante
     * Já tive que utilizar a função no Exercício 15
     * Reutilizei as funções para verificar se era quadrada e determinar cofactores
     * @param v
     * @return
     */

    public static Integer obterDeterminantePorLaplace(int[][] v) {
        if (!(ExercicioQuinze.eMatrizQuadrada(v))) return null;
        else if(v.length==1)
        {
            return v[0][0];
        }
        else if (v.length == 2)
        {
            return v[0][0] * v[1][1] - v[0][1] * v[1][0];
        }
        else {
            double determinante = 0;
            for (int i = 0; i < v[0].length; i++)
                determinante += Math.pow(-1, i) * v[0][i]
                        * obterDeterminantePorLaplace(ExercicioQuinze.obterMatrizDosMenores(v, 0, i));
            return(int) determinante;
        }
    }
}


