import java.util.Arrays;

public class Vector {

    // Atributes
    private int[] vector;

    // Construtor
    public Vector() {
        this.vector = new int[0];
    }

    // Construtor
    public Vector(int[] array) {
        if (!isValid(array)) {
            throw new IllegalArgumentException(" O vector não pode ser nulo");
        }
        this.vector = copyArray(array, array.length);
    }

    private static int[] convertDigitsToArray(int number) {
        int arraySize = UsefulOperations.countDigits(number);
        int[] array = new int[arraySize];
        for (int i = arraySize - 1; i >= 0; i--) {
            array[i] = number % 10;
            number = number / 10;
        }
        return array;
    }

    private static boolean checkIfIsIncreasingSeqGivenTheSize(int elem, int size) {
        boolean hasAIncreasingSequence = true;
        int[] array = convertDigitsToArray(elem);
        int count = UsefulOperations.countDigits(elem);
        for (int i = 0, j = i + 1; i < size - 1 && j <= count - 1 && hasAIncreasingSequence; i++, j++) {
            if (array[i] >= array[j]) {
                hasAIncreasingSequence = false;
            }
        }
        return hasAIncreasingSequence;
    }

    public int[] toArray() {
        int[] finalArray = new int[this.vector.length];
        System.arraycopy(this.vector, 0, finalArray, 0, this.vector.length);
        return finalArray;
    }

    /**
     * Método para verificar se o array é válido
     *
     * @param array
     * @return retorna false se o array for vazio ou nulo
     */
    public boolean isValid(int[] array) {
        boolean isValid = true;
        isValid = array != null;
        return isValid;
    }

    /**
     * Método que recebe um array e um valor para o comprimento (size)
     * Copia um array e devolve-o com o comprimento indicado no size
     *
     * @return
     */

    public int[] copyArray(int[] array, int size) {
        if (array == null || size < 0) {
            return null;
        }
        int[] copy = new int[size];
        for (int i = 0; i < size; i++) {
            copy[i] = array[i];
        }
        return copy;
    }

    /**
     * Método para adicionar um valor
     *
     * @param element
     */

    public void addElem(int element) {
        int[] newArray = new int[this.vector.length + 1];
        for (int i = 0; i < this.vector.length; i++) {
            newArray[i] = this.vector[i];
        }
        newArray[newArray.length - 1] = element;
        this.vector = newArray;
    }

    public int length() {
        int count = 0;
        for (int ignored :
                this.vector) {
            count++;
        }
        return count;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.vector == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        boolean isEquals = true;
        Vector other = (Vector) obj;
        if (other.length() == this.vector.length) {
            for (int i = 0; i < this.vector.length && isEquals; i++) {
                isEquals = this.vector[i] == other.vector[i];
            }
        } else {
            return false;
        }
        return isEquals;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(vector);
    }

    /**
     * Método para gerar uma excepção quando o array é vazio
     */

    private void isEmpty() {
        if (this.vector.length == 0) {
            throw new IllegalArgumentException("Vector is Empty");
        }
    }

    //Alínea d)

    private void isNull() {
        if (vector == null) {
            throw new IllegalArgumentException("Vector can't be NUll");
        }
    }

    /**
     * Método para verificar se um elemento está contido no vector
     *
     * @param element
     * @return
     */
    public boolean checkIfElemIsInArray(int element) {
        boolean isPresent = false;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == element) {
                isPresent = true;
            }
        }
        return isPresent;
    }

    /**
     * Método que recebe um elemento e remove -o do vector.
     *
     * @param elemToRemove
     */
    public void removefirstElem(int elemToRemove) {
        isEmpty();
        if(!checkIfElemIsInArray(elemToRemove))
        {
            throw new IllegalArgumentException();
        }

        int[] temp = new int[vector.length - 1];
        int k = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] != elemToRemove) {
                temp[k] = this.vector[i];
                k++;
            }
        }
        // Actualizar o vector
        this.vector = temp;
    }

    // Alínea f)

    /**
     * Retorna um valor do elemento indicado pela sua posição
     *
     * @param index
     * @return
     */
    public int getValueFromIndex(int index) {
        if (checkIfIndexIsOutOfBounds(index)) {
            throw new IllegalArgumentException(" Index out of bounds");
        }
        return this.vector[index];
    }

    private boolean checkIfIndexIsOutOfBounds(int index) {
        return index < 0 || index >= this.vector.length;
    }

    /**
     * Método para verificar o comprimento do array
     *
     * @return
     */

    public int getNumberOfElemInArray() {
        return this.vector.length;
    }

    /**
     * Método para obter o maior elemento do vector
     * Se o vector for vazio lança uma excepção
     * Se o vector só tiver 1 elemento, esse é o maior
     * Se tiver mais, vai comparando valor a valor
     *
     * @return retorna o maior valor
     */

    public int getTheBiggestValue() {
        isEmpty();
        if (this.vector.length == 1) {
            return vector[0];
        }
        int biggest = vector[0];
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] > biggest) {
                biggest = vector[i];
            }
        }
        return biggest;
    }

    /**
     * * Método para obter o menor elemento do vector
     * * Se o vector for vazio lança uma excepção
     * * Se o vector só tiver 1 elemento, esse é o menor
     * * Se tiver mais, vai comparando valor a valor
     *
     * @return
     */
    public int getTheLowestValue() {
        isEmpty();
        if (this.vector.length == 1) {
            return vector[0];
        }
        int lowest = vector[0];
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] < lowest) {
                lowest = vector[i];
            }
        }
        return lowest;
    }

    /**
     * Método para calcular a média dos elementos
     *
     * @return
     */

    public double getAverageFromArray() {
        isEmpty();
        int soma = 0;
        for (int i = 0; i < this.vector.length; i++) {
            soma = soma + this.vector[i];

        }
        double media = (double) soma / this.vector.length;
        return media;
    }

    public int getSumOfArrayValues()
    {
        isEmpty();
        int soma =0;
        for (int i = 0; i < vector.length; i++) {
            soma = soma + this.vector[i];
        }
        return soma;
    }


    /**
     * Método para calcular a média dos números pares
     *
     * @return
     */

    public double getAvgOfEvenNumbers() {

        isEmpty();
        int soma = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] % 2 == 0) {
                soma = soma + this.vector[i];
            }
        }
        int number = countEvenNumbers();
        if (number == 0) {
            return 0;
        }
        double evenAvg = (double) soma / number;
        return evenAvg;
    }

    /**
     * Método para calcular o número de números pares
     *
     * @return
     */
    public int countEvenNumbers() {
        if (this.vector.length == 0) {
            return 0;
        }
        int numbOfEven = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % 2 == 0) {
                numbOfEven++;
            }
        }
        return numbOfEven;
    }

    // Alínea l)

    /**
     * Método para calcular o número de números impares
     *
     * @return
     */
    public int countOddNumbers() {

        if (this.vector.length == 0) {
            return 0;
        }
        int numbOfOdds = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] % 2 != 0) {
                numbOfOdds++;
            }
        }
        return numbOfOdds;
    }

    /**
     * Método para obter a média de números impares
     *
     * @return
     */

    public double getAvgOfOddNumbers() {

        isEmpty();
        int soma = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] % 2 != 0) {
                soma = soma + vector[i];
            }
        }
        int number = countOddNumbers();
        if (number == 0) {
            return 0;
        }
        double oddAvg = (double) soma / number;
        return oddAvg;
    }

    /**
     * Método para calcular a média dos múltiplos de um número
     *
     * @param num
     * @return
     */
    public double avgOfMultiplesOfANumber(int num) {

        isEmpty();
        int count = 0;
        int soma = 0;
        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                soma += i;
                count++;
            }
        }
        if (count == 0) {
            return 0;
        }
        double media = (double) soma / count;
        return media;
    }

    /**
     * Método para ordenar o array de forma ascendente
     */

    public void sortByNumbersAsc() {
        isEmpty();


        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (this.vector[i] > this.vector[j]) {
                    int temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
            }
        }
    }

    /**
     * Método para ordenar o array de forma descendente
     */
    public void sortByNumbersDesc() {
        isEmpty();
        if (!isValid(this.vector)) {
            throw new ArrayIndexOutOfBoundsException("Array out of bounds .");
        }
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (this.vector[i] < this.vector[j]) {
                    int temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;

                }
            }
        }

    }

    /**
     * Método para verificar se o vector é vazio
     *
     * @return
     */

    public boolean checkIfIsEmpty() {
        return this.vector.length == 0;
    }

    /**
     * Método para verificar se o vector contém apenas um elemento, retornando true em caso positivo
     */

    public boolean checkIfHasOnlyOneElement() {
        return this.vector.length == 1;

    }

    /**
     * método para verificar se o vetor têm apenas números pares
     *
     * @return
     */

    public boolean checkIfHasOnlyEvenNumbers() {
        if (checkIfIsEmpty()) {
            return false;
        } else return this.vector.length == countEvenNumbers();

    }

    /**
     * MÉTODO PARA VERIFICAR SE O VECTOR POSSUI APENAS NÚMEROS ÍMPARES
     *
     * @return
     */

    public boolean checkIfHasOnlyOddsNumbers() {
        if (checkIfIsEmpty()) {
            return false;
        } else return this.vector.length == countOddNumbers();
    }

    //Alínea t)

    /**
     * Retorna True se o vetor tiver elementos duplicados e False em caso contrário.
     *
     * @return
     */

    public boolean checkIfHasDuplicates() {
        if (vector == null) {
            return false;
        }
        if (checkIfIsEmpty()) {
            return false;
        }
        sortByNumbersAsc();
        boolean flag = false;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (
                        vector[i] == vector[j]) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * Retorna os elementos do vetor cujo número de algarismos é superior ao número médio de
     * algarismos de todos os elementos do vetor.
     *
     * @return
     */

    public int[] getElemWhoseNofDigitsHigherThanTheAvgNumbOfDigits() {
        //isNull();
        isEmpty();
        int[] tempArray = new int[this.vector.length];
        int count = 0;
        double avgOfnumbOfDigits = UsefulOperations.getAvgOfNumberOfDigits(this.vector);
        for (int i = 0; i < this.vector.length; i++) {
            int numbersOfDigits = UsefulOperations.countDigits(this.vector[i]);
            if (numbersOfDigits > avgOfnumbOfDigits) {
                tempArray[count] = this.vector[i];
                count++;
            }
        }

        return copyArray(tempArray, count);
    }

    /**
     * Método para obter a média de algarismos pares de cada elemento do vector
     *
     * @param value
     * @return media
     */

    private double getTheEvenPercentagePerValue(int value) {
        int countTotal = UsefulOperations.countDigits(value);
        int countEven = UsefulOperations.countEvenDigits(value);
        return (double) countEven / countTotal;

    }


    //Alínea u)

    /**
     * Método para obter a média dos algarismos pares do array
     */
    private double getAvgOfEvenPercentage() {
        isEmpty();
        double sum = 0;
        for (int i = 0; i < this.vector.length; i++) {
            sum += getTheEvenPercentagePerValue(vector[i]);
        }
        return sum / this.vector.length;
    }


    //Alínea v)

    /**
     * Retorna os elementos do vetor cuja percentagem de algarismos pares é superior à média da
     * percentagem de algarismos pares de todos os elementos do vetor
     *
     * @return
     */

    public Vector elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfAllEvenElements() {
        isEmpty();
        int[] newArray = new int[this.vector.length];
        int newArrayIndex = 0;

        for (int i = 0; i < this.vector.length; i++) {
            if (getTheEvenPercentagePerValue(this.vector[i]) > getAvgOfEvenPercentage()) {
                newArray[newArrayIndex] = this.vector[i];
                newArrayIndex++;
            }
        }
        int[] resultArray = copyArray(newArray, newArrayIndex);
        Vector result = new Vector(resultArray);
        return result;
    }

    /**
     * Retorne os elementos do vetor compostos exclusivamente por algarismos pares.
     *
     * @return
     */

    public Vector getElemWithAllEvenDigits() {
        isEmpty();
        int[] arrayWithElements = new int[this.vector.length];
        int index = 0;

        for (int i = 0; i < this.vector.length; i++) {
            if (getTheEvenPercentagePerValue(this.vector[i]) == 1) {
                arrayWithElements[index] = this.vector[i];
                index++;
            }
        }
        int[] resultArray = copyArray(arrayWithElements, index);
        Vector result = new Vector(resultArray);
        return result;
    }

    public boolean checkIfHasAnAscSequence(int value) {
        boolean hasAnIncreasingSeq = true;
        int[] array = convertDigitsToArray(value);
        for (int i = 0, j = i + 1; i < array.length - 1 && hasAnIncreasingSeq; i++, j++) {
            if (array[i] >= array[j]) {
                hasAnIncreasingSeq = false;
            }
        }
        return hasAnIncreasingSeq;
    }


    //Alínea w)

    /**
     * Verifica se o array tem elementos com os algarismos numa sequencia crescente e retorna unicamente esses valores
     *
     * @return
     */

    public Vector increasingSequences() {
        isEmpty();
        int[] newArray = new int[this.vector.length];
        int index = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (checkIfHasAnAscSequence(this.vector[i])) {
                newArray[index] = this.vector[i];
            }
        }
        int[] resultArray = copyArray(newArray, index);
        Vector result = new Vector(resultArray);
        return result;
    }

    private boolean isPalindrome(int number) {
        int[] array = convertDigitsToArray(number);
        boolean isPalindrome = false;
        for (int i = 0, j = array.length - 1; i < j; i++, j--) {
            if (array[i] == array[j]) {
                isPalindrome = true;
            }
        }
        return isPalindrome;
    }

    public Vector getpalindromes() {
        isEmpty();
        int index = 0;
        int[] newArray = new int[this.vector.length];
        for (int i = 0; i < this.vector.length; i++) {
            if (isPalindrome(this.vector[i])) {
                newArray[index] = this.vector[i];
                index++;
            }
        }
        int[] resultArray = copyArray(newArray, index);
        Vector result = new Vector(resultArray);
        return result;
    }

    private boolean checkIfIsTheSameDigit(int element) {
        int[] array = convertDigitsToArray(element);
        boolean isSameDigit = true;
        for (int i = 0, j = i + 1; i < array.length - 1 && isSameDigit; i++, j++) {
            {
                if (array[i] != array[j]) {
                    isSameDigit = false;
                }
            }
        }
        return isSameDigit;
    }

    // Alínea y)

    public Vector hasTheSameDigits() {
        Vector sameDigits = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (checkIfIsTheSameDigit(vector[i])) {
                sameDigits.addElem(vector[i]);
            }
        }
        return sameDigits;
    }


    //Alínea z)

    public Vector armstrongNumbers() {
        isEmpty();
        Vector armstrongsValues = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (UsefulOperations.isArmstrong(vector[i])) {
                armstrongsValues.addElem(vector[i]);
            }
        }
        return armstrongsValues;
    }

    public Vector increasingSeqGivenTheSize(int size) {
        Vector increasingSeq = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (checkIfIsIncreasingSeqGivenTheSize(vector[i], size)) {
                increasingSeq.addElem(vector[i]);
            }
        }
        return increasingSeq;
    }


    //Alínea aa)
    public boolean checkIfisMatching(int[] other) {
        isEmpty();
        boolean isMatching = true;
        for (int i = 0; i < this.vector.length && isMatching; i++) {
            if (this.vector[i] != other[i]) {
                isMatching = false;
            }
        }
        return isMatching;
    }

}
