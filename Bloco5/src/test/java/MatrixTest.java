import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void testSetNullMatrix() {
        int[][] m = null;
        assertThrows(NullPointerException.class, () -> {
            Matrix v1 = new Matrix(m);
        });
    }


    @Test
    void testOnetoMatrix() {
        int[][] matrix = {
                {1},
                {2},
                {3}
        };
        Matrix result = new Matrix(matrix);
        int[][] expected = {
                {1},
                {2},
                {3}
        };

        Matrix expectedMatrix = new Matrix(expected);
        assertArrayEquals(expectedMatrix.toMatrix(), result.toMatrix());
    }


    @Test
    void testOneAddElementInRow() {
        int[][] matrixOne = {{}};
        int elem = 1;
        int row = 0;
        int[][] expected = {{1}};
        Matrix result = new Matrix(matrixOne);
        result.addElementInRow(row, elem);
        assertArrayEquals(result.toMatrix(), expected);
    }

    @Test
    void testAddElementToANonExistingRow() {
        int[][] matrixOne = {{}};
        int elem = 1;
        int row = 6;
        Matrix result = new Matrix(matrixOne);
        assertThrows(IndexOutOfBoundsException.class, () -> {
            result.addElementInRow(row, elem);
        });
    }


    @Test
    void testTwoAddElementInRow() {
        int[][] matrixOne = {{1, 2}, {1, 2}, {1, 2}};
        int elem = 6;
        int row = 2;
        int[][] expected = {{1, 2}, {1, 2}, {1, 2, 6}};
        Matrix result = new Matrix(matrixOne);
        result.addElementInRow(row, elem);
        assertArrayEquals(result.toMatrix(), expected);
    }

    @Test
    void testOneRemoveElementFromAnEmptyMatrix() {
        int[][] matrixOne = {{}};
        int valueToRemove = 0;
        Matrix result = new Matrix(matrixOne);
        int[][] expected = {{}};
        assertArrayEquals(result.toMatrix(), expected);
    }

    @Test
    void testTwoRemoveANonExistingElement() {
        int[][] matrixOne = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        int valueToRemove = 7;
        Matrix result = new Matrix(matrixOne);
        int[][] expected = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        result.removeElementInMatrix(valueToRemove);
        assertArrayEquals(result.toMatrix(), expected);

    }

    @Test
    void testThreeRemoveElement() {
        int[][] matrixOne = {{1, 2, 3}, {4, 5, 6}, {7, 7, 8}};
        int valueToRemove = 4;
        int[][] expected = {{1, 2, 3}, {5, 6}, {7, 7, 8}};
        Matrix result = new Matrix(matrixOne);
        result.removeElementInMatrix(valueToRemove);
        assertArrayEquals(expected, result.toMatrix());
    }


    @Test
    void testOneBiggestOnAEmptyMatrix() {
        int[][] matrixOne = {{}};
        Matrix result = new Matrix(matrixOne);
        assertThrows(IllegalArgumentException.class, () -> {
            result.biggestOnMatrix();
        });
    }


    @Test
    void testTwobiggestOnMatrix() {
        int[][] matrixOne = {{1, 2, 3}, {12, 4, 6}, {2, 34, 6}};
        int expected = 34;
        Matrix newMatrix = new Matrix(matrixOne);
        int result = newMatrix.biggestOnMatrix();
        assertEquals(expected, result);
    }


    @Test
    void testOneSmallestOnMatrixFromAnEmptyMatrix() {
        int[][] matrixOne = {{}};
        Matrix result = new Matrix(matrixOne);
        assertThrows(IllegalArgumentException.class, () -> {
            result.smallestOnMatrix();
        });
    }


    @Test
    void testTwoSmallestOnMatrix() {
        int[][] matrixOne = {{1, 2, 3}, {0, 7, 2}, {3, 76, 9}};
        Matrix newMatrix = new Matrix(matrixOne);
        int expected = 0;
        int result = newMatrix.smallestOnMatrix();
        assertEquals(expected, result);
    }


    @Test
    void testOneAvgOfAEmptyMatrix() {
        int[][] matrixOne = {{}};
        Matrix result = new Matrix(matrixOne);
        assertThrows(IllegalArgumentException.class, () -> {
            result.avgOfMatrixValues();
        });
    }

    @Test
    void testTwoAvgOnMatrixValues() {
        int[][] matrixOne = {{1, 2}, {5,8}};
        Matrix newMatrix = new Matrix(matrixOne);
        double expected = 4;
        double result = newMatrix.avgOfMatrixValues();
        assertEquals(expected, result,0.1);
    }


    @Test
    void testGetInternalSumsOfAnEmptyMatrix() {
        int[][] matrixOne = {{}};
        Matrix result = new Matrix(matrixOne);
        assertThrows(IllegalArgumentException.class, () -> {
            result.getInternalSumsOfMatrixRows();
        });
    }


    @Test
    void testTwoGetInternalSumsOfMatrixRows() {
        int[][] matrixOne = {{1, 2, 3}, {0, 7, 2}, {3, 76, 9}};
        Matrix newMatrix = new Matrix(matrixOne);
        int [] expected = {6,9,88};
        Vector result = newMatrix.getInternalSumsOfMatrixRows();
        assertArrayEquals(expected, result.toArray());

    }
}