package com.company;

import java.util.Scanner;

public class Bloco1 {

    public static void main(String[] args) {


        //double x = 0,y=0,z=0;
        //exercicio1();
         //double v = exercicio3(x, y);
        //Exercicio4(4);
        //double alt =exercicio6(x,y,z);
        //exercicio7();
        //exercicio8();
    }

    public static void bloco1Ex1() {

        double rapazes, raparigas, total;
        double percentagemDeRapazes, percentagemDeRaparigas;


        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de rapazes");
        rapazes = ler.nextInt();
        System.out.println("Introduza o número de raparigas");
        raparigas = ler.nextInt();

        total = rapazes + raparigas;
        percentagemDeRapazes = rapazes / total;
        percentagemDeRaparigas = raparigas / total;


        System.out.println("Percentagem de rapazes: " + percentagemDeRapazes);
        System.out.println("Percentagem de raparigas: " + percentagemDeRaparigas);


    }

    public static double Exercicio2(int rosas, int tulipas, double precoRosa, double precoTulipa)
    {

            double total = rosas * precoRosa + tulipas * precoTulipa;
            return total;
    }


    public static double Exercicio3(double raio, double altura) {
        double volumeDoCilindro, volumeDoCilindroEmL;
        volumeDoCilindroEmL = (Math.PI * Math.pow(raio, 2) * altura) * 1000;

        // Procurar documentação sobre o printf
        //System.out.printf("'%5.2f'%n", volumeDoCilindroEmL);

        return volumeDoCilindroEmL;

    }

    public static double Exercicio4(double intervaloDeTempo)
    {
        final int velocidadeDoSom = 340;
        return intervaloDeTempo* velocidadeDoSom;
    }


    public static double Exercicio5(double tempo)
    {

        double altura = ((9.8*Math.pow(tempo,2))/2);
        return altura;

    }

     public static double Exercicio6(double alturaPessoa, double sombraPessoa, double sombraEdificio)
     {
        double alturaDoEdificio = (alturaPessoa * sombraEdificio)/sombraPessoa;
        return alturaDoEdificio;
     }


    public static double Exercicio7(double tempoZe)
    {

        double deslocamentoDoManel= 42195;

        //Passar o tempo para segundos
        double tempoDoManel = 4*3600 + 2*60 +10;

        double velocidadeDeReferencia = deslocamentoDoManel/ tempoDoManel;
        // Dá algo proximo de 2.903991741

        double deslocamentoDoZe = velocidadeDeReferencia * tempoZe;
        return deslocamentoDoZe;

    }

    public static double Exercicio8()
    {

        double distancia = Math.sqrt(Math.pow(60,2)+ Math.pow(40,2)- 2*60*40*Math.cos(Math.PI/3));
        return distancia;


    }

    public static double getPerimetro(double a, double b)
    {
        double perimetro = (2*a)+(2*b);
        return perimetro;

    }

    public static double getHipotenusa(double c1, double c2)
    {
        double hipotenusa = Math.sqrt(Math.pow(c1,2) + Math.pow(c2,2));
        return hipotenusa;
    }


    public static double[] formulaResolvente(double a, double b, double c)
    {
        double [] result = new double[2];
        double intervalo1;
        double intervalo2;
        double delta = Math.sqrt(Math.abs(Math.pow(b,2)) - 4 * a *c);

        if(a == 0) {
            System.out.print(" A EQUAÇÃO NÃO É DE SEGUNDO GRAU :");
            //return intervalo1 = -c/b;
        }

        else if (a < 0)
        {
            System.out.println("A Equação não tem soluções reais ");
        }

        else {

            intervalo1 = (-b - delta)/ 2*a;
            intervalo2 = (-b + delta)/2*a;

            result[0]= intervalo1;
            result[1] = intervalo2;

            //System.out.println("A primeira solução é : " + intervalo1);
            //System.out.println("A segunda solução é : " + intervalo2);

        }


        return result;
    }


    public static double converterTemp(double temperatura)
    {
        double Fahr = 32 + (1.8) * temperatura;
        return Fahr;
    }


    public static double minutosDecorridos(int horas, int minutos)
    {
        return horas*60 + minutos;
    }

    









}
