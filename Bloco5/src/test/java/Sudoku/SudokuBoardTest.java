package Sudoku;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuBoardTest {


    @Test
    void TestOneAddAnInvalidSudokuBoard() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0}};

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
            SudokuBoard testMatrix = new SudokuBoard(initialMatrix);
        });
    }

    @Test
    void TestOneAddNumber() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testMatrix = new SudokuBoard(initialMatrix);
        int line = 0;
        int column = 0;
        int number = 7;
        testMatrix.addValueGivenCoordinates(line, column, number);
        int[][] expected = {{7, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testMatrix.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void TestTwoAddAnInvalidNumber() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 0;
        int column = 0;
        int number = 17;

        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        testBoard.addValueGivenCoordinates(line, column, number);
        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void TestThreeAddANumberWithInvalidCoordenates() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 17;
        int column = 0;
        int number = 1;
        testBoard.addValueGivenCoordinates(line, column, number);
        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }

    /**
     * Se tentar adicionar/ modificar um número numa posição que está fixa,
     * o número não é alterado
     */

    @Test
    void TestFourAddANumberToAFixedPosition() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 0;
        int column = 2;
        int number = 5;
        testBoard.addValueGivenCoordinates(line, column, number);

        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void TestFiveAddARepetedNumberOnTheSameRow() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 0;
        int column = 1;
        int number = 1;
        testBoard.addValueGivenCoordinates(line, column, number);

        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void TestSixAddARepetedNumberOnTheSameColumn() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 0;
        int column = 0;
        int number = 3;
        testBoard.addValueGivenCoordinates(line, column, number);

        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {0, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }


    @Test
    void TestSevenAddARepetedNumberOnTheSameBlock() {
        int[][] initialMatrix = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {7, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        int line = 1;
        int column = 1;
        int number = 7;
        testBoard.addValueGivenCoordinates(line, column, number);

        int[][] expected = {{0, 0, 1, 0, 0, 2, 4, 0, 6},
                {6, 0, 0, 0, 0, 5, 0, 1, 8},
                {7, 8, 2, 9, 1, 6, 0, 0, 0},
                {0, 0, 0, 8, 0, 4, 5, 0, 0},
                {1, 0, 8, 0, 0, 0, 6, 0, 9},
                {0, 0, 0, 0, 0, 0, 8, 2, 7},
                {5, 1, 4, 7, 3, 0, 0, 6, 2},
                {3, 0, 7, 2, 4, 0, 0, 0, 0},
                {0, 2, 0, 0, 0, 1, 3, 7, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }


    @Test
    void testEigthAddSeveralNumbers() {
        int[][] initialMatrix = {{0, 8, 7, 6, 0, 3, 2, 4, 0},
                {0, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 0, 0, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        testBoard.addValueGivenCoordinates(0, 0, 5);
        testBoard.addValueGivenCoordinates(1, 0, 2);
        testBoard.addValueGivenCoordinates(8, 6, 5);
        testBoard.addValueGivenCoordinates(8, 6, 6);

        int[][] expected = {{5, 8, 7, 6, 0, 3, 2, 4, 0},
                {2, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 6, 0, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void testClearBoardAfterAddSeveralNumbers() {
        int[][] initialMatrix = {{0, 8, 7, 6, 0, 3, 2, 4, 0},
                {0, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 0, 0, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        testBoard.addValueGivenCoordinates(0, 0, 5);
        testBoard.addValueGivenCoordinates(1, 0, 2);
        testBoard.addValueGivenCoordinates(8, 6, 5);
        testBoard.addValueGivenCoordinates(8, 6, 6);
        testBoard.clearBoard();

        int[][] expected = {{0, 8, 7, 6, 0, 3, 2, 4, 0},
                {0, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 0, 0, 0}};

        int[][] result = testBoard.toArray();
        assertArrayEquals(expected, result);
    }


    @Test
    void testOneCheckFalseIfTheGridIsNotFull() {
        int[][] initialMatrix = {{0, 8, 7, 6, 0, 3, 2, 4, 0},
                {0, 1, 3, 0, 0, 4, 0, 6, 0},
                {4, 0, 0, 0, 2, 0, 5, 0, 0},
                {0, 7, 2, 0, 0, 6, 3, 5, 0},
                {0, 5, 0, 7, 0, 9, 1, 0, 6},
                {0, 6, 0, 0, 0, 8, 7, 9, 0},
                {7, 0, 9, 1, 6, 0, 0, 8, 2},
                {6, 0, 5, 0, 8, 2, 0, 0, 7},
                {0, 0, 0, 9, 4, 0, 0, 0, 0}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        boolean expected = false;
        boolean result = testBoard.checkIfTheGridIsFull();
        assertEquals(expected, result);
    }

    @Test
    void testTwoCheckTrueIfTheGridIsFull() {
        int[][] initialMatrix = {{5, 8, 7, 6, 9, 3, 2, 4, 1},
                {2, 1, 3, 5, 7, 4, 8, 6, 9},
                {4, 9, 6, 8, 2, 1, 5, 7, 3},
                {9, 7, 2, 4, 1, 6, 3, 5, 8},
                {8, 5, 4, 7, 3, 9, 1, 2, 6},
                {3, 6, 1, 2, 4, 8, 7, 9, 4},
                {7, 3, 9, 1, 6, 5, 4, 8, 2},
                {6, 4, 5, 3, 8, 2, 9, 1, 7},
                {1, 2, 8, 9, 4, 7, 6, 3, 5}};

        SudokuBoard testBoard = new SudokuBoard(initialMatrix);
        boolean expected = true;
        boolean result = testBoard.checkIfTheGridIsFull();
        assertEquals(expected, result);
    }


}