import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezoitoTest {

    @Test
    void criarMatrizMascaraTesteCasoNulo() {

        char [][]m1 = null;
        char letter = 'l';
        int [][] expected = null;
        int [][] result = ExercicioDezoito.obterMatrizMascaraParaUmChar(m1,letter);
        assertEquals(expected,result);

    }

    @Test
    void criarMatrizMascaraTesteCasoVazio() {

        char [][]m1 ={{}};
        char letter = 'l';
        int [][] expected = null;
        int [][] result = ExercicioDezoito.obterMatrizMascaraParaUmChar(m1,letter);
        assertEquals(expected,result);

    }

    @Test
    void criarMatrizMascaraTesteCasoDimInvalidas() {

        char [][]m1 = {{'l'},{1},{'f'},
                        {'l'},{'j'},{'h'}};
        char letter = 'l';
        int [][] expected = null;
        int [][] result = ExercicioDezoito.obterMatrizMascaraParaUmChar(m1,letter);
        assertEquals(expected,result);
    }

    @Test
    void criarMatrizMascaraTesteCasoValido() {

        char[][] m1 = { { 'G', 'E', 'E', 'K', 'S', 'F', 'O'},
                        { 'R', 'G', 'E', 'E', 'K', 'S', 'K'},
                        { 'G', 'E', 'E', 'K', 'S', 'Q', 'U'},
                        { 'I', 'Z', 'G', 'E', 'E', 'K', 'J'},
                        { 'A', 'D', 'E', 'Q', 'A', 'P', 'J'},
                        { 'A', 'C', 'T', 'I', 'C', 'E', 'J'},
                        { 'A', 'C', 'T', 'I', 'C', 'E', 'L'}};

        char letter = 'L';
        int [][] expected = {{0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,1},};
        int [][] result = ExercicioDezoito.obterMatrizMascaraParaUmChar(m1,letter);
        assertArrayEquals(expected,result);

    }

    @Test
    void criarMatrizMascaraTesteCasoLetraNaoEncontrada() {

        char[][] m1 = { { 'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                { 'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                { 'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                { 'I', 'Z', 'G', 'R', 'E', 'C', 'L'},
                { 'A', 'D', 'E', 'Q', 'A', 'P', 'L'},
                { 'A', 'C', 'T', 'I', 'C', 'E', 'L'},
                { 'A', 'C', 'T', 'I', 'C', 'E', 'L'}};

        char letter = 'Y';
        int [][] expected = {{0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0},};
        int [][] result = ExercicioDezoito.obterMatrizMascaraParaUmChar(m1,letter);
        assertArrayEquals(expected,result);

    }


    @Test
    void validarMatrizTesteCasoNulo() {

        char[][] m = null;
        boolean expected = false;
         boolean result = ExercicioDezoito.validarMatriz(m);
        assertFalse(result);

    }


    @Test
    void validarMatrizTesteCasoDimInvalidas() {

        char[][] m = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        boolean expected = false;
        boolean result = ExercicioDezoito.validarMatriz(m);
        assertFalse(result);

    }

    @Test
    void validarMatrizTesteCasoDimValidas() {

        char[][] m = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        boolean expected = true;
        boolean result = ExercicioDezoito.validarMatriz(m);
        assertTrue(result);

    }


    @Test
    void charParaArrayTesteCasoVazio() {
        String palavra = "";
        char[] expected={};
        char [] result = ExercicioDezoito.charParaArray(palavra);
        assertArrayEquals(expected,result);
    }

    @Test
    void charParaArrayTeste() {
        String palavra = "alma";
        char[] expected={'a','l','m','a'};
        char [] result = ExercicioDezoito.charParaArray(palavra);
        assertArrayEquals(expected,result);
    }


    @Test
    void verificarSeAPalavraEstaNaMatrizTesteCasoMatrizNula() {

        char [][] matriz = null;
        String palavra = "ABS";
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz,palavra);
        assertEquals(expected,result);
    }


    @Test
    void testVerificarSeAPalavraEstaNaMatrizTesteCasoMatrizComDimInvalidas() {

        char [][] matriz = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "IBZ";
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz,palavra);
        assertEquals(expected,result);

    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizTesteCasoPalavraInexistente() {

        char [][] matriz = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "ZZZ";
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz,palavra);
        assertEquals(expected,result);

    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoHorizontal() {

        char[][] matriz = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "AGZ";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);


    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoVertical() {

        char[][] matriz = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                           {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                           {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                           {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                           {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                           {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                           {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "KSEA";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);


    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoVerticalTesteDois() {

        char[][] matriz = {{'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                           {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                           {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                           {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                           {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                           {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                           {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "GBI";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);


    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoDiagonalAscDireita() {

        char[][] matriz = { {'A', 'E', 'B', 'P', 'S', 'F', 'O'},
                            {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                            {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                            {'I', 'Z', 'O', 'A', 'E', 'C', 'L'},
                            {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                            {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                            {'O', 'G', 'T', 'I', 'C', 'E', 'L'}};

        String palavra = "BIS";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertTrue(result);


    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoDiagonalAscESQ() {

        char[][] matriz = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                            {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                            {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                            {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                            {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                            {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                            {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "QGS";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);

    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoDiagonalDescDireita() {

        char[][] matriz = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "ZEI";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);

    }

    @Test
    void testVerificarSeAPalavraEstaNaMatrizCasoDiagonalDescESQ() {

        char[][] matriz = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        String palavra = "AEC";
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarSeAPalavraEstaNaMatriz(matriz, palavra);
        assertEquals(expected, result);

    }


    @Test
    void verificarDireccoesDoInicioAoFimTesteCasoMatrizNula() {

        char[][] m = null;
        int linhaInicio=0;
        int linhaFim =0;
        int colunaInicio=0;
        int colunaFim =0;
        char[] caracteres = {'A','E','B'};
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,linhaFim,colunaInicio,colunaFim,caracteres);
        assertFalse(result);
    }

    @Test
    void verificarDireccoesDoInicioAoFimTesteCasoMatrizDimInvalidas() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        int linhaInicio=0;
        int linhaFim =0;
        int colunaInicio=0;
        int colunaFim =0;
        char[] caracteres = {'A','E','B'};
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,linhaFim,colunaInicio,colunaFim,caracteres);
        assertFalse(result);
    }

    @Test
    void verificarDireccoesDoInicioAoFimTesteDoisCasoMatrizDimInvalidas() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        int linhaInicio=-1;
        int linhaFim =0;
        int colunaInicio=0;
        int colunaFim =0;
        char[] caracteres = {'A','E','B'};
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,linhaFim,colunaInicio,colunaFim,caracteres);
        assertFalse(result);
    }



    @Test
    void verificarDireccoesDoInicioAoFimTesteTresCasoMatrizDimInvalidas() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}
        };

        int linhaInicio=0;
        int linhaFim =0;
        int colunaInicio=15;
        int colunaFim =0;
        char[] caracteres = {'A','E','B'};
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,linhaFim,colunaInicio,colunaFim,caracteres);
        assertFalse(result);
    }

    @Test
    void verificarDireccoesDoInicioAoFimTesteCasoFalso() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}
        };

        int linhaInicio=0;
        int linhaFim =0;
        int colunaInicio=3;
        int colunaFim =4;
        char[] caracteres = {'E','S','Z'};
        boolean expected = false;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,linhaFim,colunaInicio,colunaFim,caracteres);
        assertFalse(result);
    }


    @Test
    void verificarDireccoesDoInicioAoFimTesteCasoVerdadeiro() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                       {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                       {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                       {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                       {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                       {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                       {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        int linhaInicio=0;
        int linhaFim =2;
        int colunaInicio=1;
        int colunaFim =3;
        char[] caracteres = {'E','I','B'};
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,colunaInicio,linhaFim,colunaFim,caracteres);
        assertTrue(result);
    }

    @Test
    void verificarDireccoesDoInicioAoFimTesteDoisCasoFalso() {

        char[][] m = { {'A', 'E', 'B', 'K', 'S', 'F', 'O'},
                {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                {'I', 'Z', 'G', 'A', 'E', 'C', 'L'},
                {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                {'J', 'C', 'T', 'I', 'B', 'E', 'L'}};

        int linhaInicio=0;
        int linhaFim =2;
        int colunaInicio=1;
        int colunaFim =3;
        char[] caracteres = {'E','I','B'};
        boolean expected = true;
        boolean result = ExercicioDezoito.verificarDireccoesDoInicioAoFim(m,linhaInicio,colunaInicio,linhaFim,colunaFim,caracteres);
        assertTrue(result);
    }












}