public class ExercicioCinco {

    public static void main(String[] args) {

    }
    public static int somaDosElemPares(int n)
    {
        int soma=0;
        if(n<=0){
            soma = -1; // retorna
        }
        else {
            // Passar o numero a um vector algarismo por índice
            int[] v = ExercicioDois.devolveVetor(n);
            // Passar a um vector só de pares
            int[] p = ExercicioQuatro.retonaOsPares(v);
            //soma dos pares
            for (int i = 0; i < p.length; i++) {
                soma += p[i];
            }
        }
        return soma;
    }
    public static int somaDosElemImpares(int n)
    {
        int soma=0;
        if(n<=0){
            soma = -1; // retorna
        }
        else {

            // Passar o numero a um vector algarismo por índice
            int[] v = ExercicioDois.devolveVetor(n);
            // Passar a um vector só de pares
            int[] p = ExercicioQuatro.retornaOsImpares(v);
            //soma dos pares
            for (int i = 0; i < p.length; i++) {
                soma += p[i];
            }
        }
        return soma;
    }

}
