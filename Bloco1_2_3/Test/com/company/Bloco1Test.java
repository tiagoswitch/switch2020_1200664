package com.company;

import org.junit.Assert;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class Bloco1Test {


    @Test
    void exercicio2() {
        double expected = 25;
        double result = Bloco1.Exercicio2(2,1,10,5);
        assertEquals(expected, result,0.01);

    }

    @Test
    public void exercicio3Teste1() {
        double expected = 549778.714;
        double result = Bloco1.Exercicio3(5,7);
        assertEquals(expected,result, 0.001);
    }



    @Test
    public void exercicio3TesteZero() {

        double expected = 0;
        double result = Bloco1.Exercicio3(5, 0);
        assertEquals(expected, result, 0.001);

    }

    @Test
    public void exercicio4Testzero() {
        double expected= 0;
        double time =0;
        double result = Bloco1.Exercicio4(time);
        assertEquals(expected,result);
    }

    @Test
    public void exercicio4Test1()
    {
        double expected = 3400;
        double time =10;
        double result= Bloco1.Exercicio4(time);
        assertEquals(expected,result, 0.1);

    }


    @Test
    void exercicio5()
    {
        double time =2;
        double expected = 19.6;

        double resultado = Bloco1.Exercicio5(time);
        assertEquals(expected,resultado,0.1);

    }


    @Test
    void exercicio6()
    {
        double alturaPessoa= 2;
        double sombraPessoa=4;
        double sombraEdificio = 40;
        double expected = 20;

        double result = Bloco1.Exercicio6(alturaPessoa,sombraPessoa,sombraEdificio);

        assertEquals(expected,result,0.01);
    }


    @Test
    void exercicio7()
    {
        double velocidadeReferencia = 2.903991741;
        double time =10;
        double expected= velocidadeReferencia * time;
        double result = Bloco1.Exercicio7(time);

        assertEquals(expected,result,0.1);
    }


    @Test
    void exercicio8()
    {
        double expected = Math.sqrt(2800);
        double result= Bloco1.Exercicio8();
        assertEquals(expected,result,0.01);

    }


    @Test
    void getPerimetro() {
        double x=2;
        double y=2;
        double expected = 2*2 + 2*2;
        double result = Bloco1.getPerimetro(x,y);

        assertEquals(expected, result,0.01);

    }


    @Test
    void getHipotenusa() {

        double x = 3;
        double y = 4;
        double expected = 5;
        double result = Bloco1.getHipotenusa(x,y);

        assertEquals(expected,result, 0.001);
    }

    @Test
    void formulaResolvente() {
        double a=1;
        double b =5;
        double c=6;

        double [] expected = new double[2];
        expected[0]=-3;
        expected[1]=-2;

        double[] result = Bloco1.formulaResolvente(a,b,c);
        Assert.assertArrayEquals(expected,result,0.01);
    }


    @Test
    void converterTemp() {
        double Celcius = 1;
        double expected = 33.8;

        double result = Bloco1.converterTemp(Celcius);
        assertEquals(expected, result, 0.01);
    }


    @Test
    void converterTempCasoZero()
    {
        double Celcius = 0;
        double expected = 32;

        double result = Bloco1.converterTemp(Celcius);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void minutosDecorridos() {

        int h =1;
        int m = 3;

        int expected = 63;
        int result = (int) Bloco1.minutosDecorridos(h,m);
        assertEquals(expected,result,0.01);
    }


}