import java.util.Arrays;

public class BiVector {

    // Atributos
    int[][] biVector;


    public BiVector() {
        this.biVector = new int[0][];
    }

    // b)
    public BiVector(int[][] initialMatrix) {
        if (initialMatrix == null) {
            throw new NullPointerException("A Matriz não pode ser nula");
        }
        this.biVector = copyArray(initialMatrix);

    }

    private int[][] copyArray(int[][] array) {
        int[][] newArray = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = new int[array[i].length];
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] = array[i][j];
            }
        }
        return array;
    }


    public int[][] toMatrix() {
        int[][] newArr = new int[this.biVector.length][];
        for (int i = 0; i < this.biVector.length; i++) {
            newArr[i] = new int[this.biVector[i].length];
            for (int j = 0; j < this.biVector[i].length; j++) {
                newArr[i][j] = this.biVector[i][j];
            }
        }
        return newArr;
    }

    private void checkIfTheLineIsOutOfBounds(int row) {
        if (row >= this.biVector.length || row < 0) {
            throw new IndexOutOfBoundsException("The line is invalid");
        }
    }


    public boolean equals(Object o) {
        //if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BiVector matrix1 = (BiVector) o;

        if (this.biVector.length != matrix1.biVector.length) {
            return false;
        }
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                if (this.biVector[i][j] != matrix1.biVector[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(biVector);
    }

    /**
     * Método para adicionar um elemento ao vector bidimensional
     * @param elem
     * @param row
     */
    public void addElem(int elem, int row) {

        checkIfTheLineIsOutOfBounds(row);
        int[] tempArrayRow = new int[this.biVector[row].length + 1];
        int index = 0;
        for (int element : this.biVector[row]) {
            tempArrayRow[index] = element;
            index++;
        }
        // Colocar o elemento na linha pretendida
        tempArrayRow[this.biVector[row].length] = elem;
        this.biVector[row] = tempArrayRow;
    }

    private void isEmptyMatrix() {
        if (this.biVector.length == 0) {
            throw new IllegalArgumentException("Empty matrix");
        }

    }


    public boolean checkIfElemIsInBiVector(int element) {
        boolean isPresent = false;
        for (int i = 0; i < this.biVector.length && !isPresent; i++) {
            for (int j = 0; j < this.biVector[i].length && !isPresent; j++) {
                if (this.biVector[i][j] == element) {
                    isPresent = true;
                }
            }
        }
        return isPresent;
    }

    private void removeElemFromMatrix(int elem, int row) {
        int[] tempArrayLine = new int[this.biVector[row].length - 1];
        int newIndex = 0;
        boolean isEqual = false;
        for (int i = 0; i < this.biVector[row].length && !isEqual; i++) {
            if (this.biVector[row][i] == elem) {
                isEqual = true;
            } else {
                tempArrayLine[newIndex] = biVector[row][i];
                newIndex++;
            }
        }
        this.biVector[row] = tempArrayLine;
    }

    public void removeTheFirstElem(int elem) {
        isEmptyMatrix();
        if (!checkIfElemIsInBiVector(elem)) {
            throw new IllegalArgumentException("O elemento não está presente na matriz");
        }
        boolean isfound = false;
        for (int i = 0; i < this.biVector.length && !isfound; i++) {
            for (int j = 0; j < this.biVector[i].length && !isfound; j++) {
                if (this.biVector[i][j] == elem) {
                    removeElemFromMatrix(elem, i);
                    isfound = true;
                }
            }
        }
    }

    /**
     * Verifica se a matriz é vazia
     *
     * @return true se a matriz tem tamanho 0
     */
    public boolean checkIfBiVectorIsEmpty() {
        return this.biVector.length == 0;
    }


    public int getBiggestElemInBiVector() {
        isEmptyMatrix();
        return UsefulOperations.getBiggestValueOfMatrix(this.biVector);
    }

    public int getSmallestElementInBiVector() {
        isEmptyMatrix();
        return UsefulOperations.getSmallestValueOfMatrix(this.biVector);
    }

    public double getAvgFromMatrix() {
        isEmptyMatrix();
        return UsefulOperations.obterValorMedio(this.biVector);
    }

    /**
     * Método para obter as somas de cada linha e devolver as respectivas somas num vector
     *
     * @return vetor com as somas de cada linha
     */
    public Vector getInternalRowSums() {
        isEmptyMatrix();
        int index = 0;
        int[] newArray = new int[this.biVector.length];
        for (int i = 0; i < this.biVector.length; i++) {
            newArray[index] = UsefulOperations.arrayInternalSum(biVector[i]);
            index++;
        }
        return new Vector(newArray);
    }

    private int getTheMaxColumnLength() {
        int columnLength = this.biVector[0].length;
        for (int[] row : this.biVector) {
            if (columnLength < row.length) {
                columnLength = row.length;
            }

        }
        return columnLength;
    }

    public Vector getInternalColumnSums() {
        isEmptyMatrix();
        int[] temp = new int[getTheMaxColumnLength()];
        for (int[] row : this.biVector) {
            for (int i = 0; i < row.length; i++) {
                temp[i] += row[i];
            }
        }
        return new Vector(temp);
    }

    /**
     * Método para retornar o índice da linha do array com  da maior soma dos respectivos elementos
     *
     * @return o índice com a maior
     */
    public Integer getIndexWithTheBiggestSumRow() {
        isEmptyMatrix();
        int index = -1;
        int[] internalSums = getInternalRowSums().toArray();
        for (int i = 0; i < internalSums.length; i++) {
            if (internalSums[i] == UsefulOperations.getBiggestValueOfArray(internalSums)) {
                index = i;
            }
        }
        return index;
    }


    public boolean isSquaredMatrix() {
        boolean result = false;
        result = UsefulOperations.allRowsHaveTheSameNofColumns(this.biVector)
                && (this.biVector.length == this.biVector[0].length);
        return result;
    }

    /**
     * Método para ver se uma matriz é simétrica e quadrada
     * Se uma matriz é igual á sua transposta então a matriz é simética
     *
     * @return true se for quadrada e simética
     */
    public boolean isSquaredAndSimetricMatrix() {
        boolean result = false;
        if (!UsefulOperations.allRowsHaveTheSameNofColumns(this.biVector)) {
            result = false;
        } else result = Arrays.deepEquals(UsefulOperations.obterMatrizTransposta(this.biVector), this.biVector);

        return result;
    }

    /**
     * Método para contar o número de elementos diferentes de zero na diagonal principal da matriz
     *
     * @return -1 se a matriz não for quadrada.
     * Se for quadrada conta os elementos diferentes de zero na diagonal principal
     */
    public int countNotNullElementsOnMainDiagonal() {
        int result = 0;
        if (!isSquaredMatrix()) {
            result = -1;
        } else {
            for (int i = 0; i < this.biVector.length; i++) {
                if (this.biVector[i][i] != 0) {
                    result++;
                }
            }
        }
        return result;
    }


    private Vector getElemFromMainDiagonal() {
        Vector mainDiagonal = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            mainDiagonal.addElem(this.biVector[i][i]);
        }
        return mainDiagonal;
    }

    private Vector getElemFromSecondaryDiagonal() {
        Vector secondaryDiagonal = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            secondaryDiagonal.addElem(this.biVector[i][this.biVector[i].length - i - 1]);
        }
        return secondaryDiagonal;
    }

    /**
     * Método que verifica se a diagonal principal e secundária são iguais
     *
     * @return
     */

    public boolean checkIfTheMainAndSecondDiagonalAreEqual() {
        if (!UsefulOperations.allRowsHaveTheSameNofColumns(this.biVector)) {
            return false;
        } else return getElemFromMainDiagonal().equals(getElemFromSecondaryDiagonal());
    }

    /*
    Retorne um vetor com todos os elementos do array encapsulado
    cujo número de algarismos é
superior ao número médio de algarismos de todos os elementos do array
     */
    private int countElementsOnBiVector() {
        int counter = 0;
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                counter++;
            }
        }
        return counter;
    }

    private int avgOfDigitsInBiVector() {
        int sumOfDigits = 0;
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {

                sumOfDigits += UsefulOperations.countDigits(this.biVector[i][j]);
            }
        }
        return sumOfDigits / countElementsOnBiVector();
    }

    private boolean isBiggerThanTheMeanLength(int numbers) {
        int numberOfDigits = UsefulOperations.countDigits(numbers);
        return Math.abs(numberOfDigits) > Math.abs(avgOfDigitsInBiVector());
    }

    public Vector getElemLargerThanAvgNumberOfDigits() {
        isEmptyMatrix();
        Vector result = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                if (isBiggerThanTheMeanLength(biVector[i][j])) {
                    result.addElem(biVector[i][j]);
                }
            }
        }
        return result;
    }

    /*
    Retorne um vetor com todos os elementos do array cuja percentagem
    de algarismos pares é
    superior à média da percentagem de algarismos pares de
    todos os elementos do array.
     */

    private double getTheEvenPercentagePerElem(int value) {
        int countTotal = UsefulOperations.countDigits(value);
        int countEven = UsefulOperations.countEvenDigits(value);
        return (double) countEven / countTotal;

    }

    /**
     * Método para obter a média dos algarismos pares da matriz
     */
    private double getAvgOfEvenPercentageFromBiVector() {
        isEmptyMatrix();
        double sum = 0;
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                sum += getTheEvenPercentagePerElem(biVector[i][j]);
            }
        }
        return sum / countElementsOnBiVector();
    }


    /**
     * Retorne um vetor com todos os elementos do array cuja percentagem de algarismos pares é
     * superior à média da percentagem de algarismos pares de todos os elementos do array
     *
     * @return
     */

    public Vector getElemWithlargerEvenPercentageThanAvgEvenPercentagem() {
        isEmptyMatrix();
        Vector result = new Vector();
        for (int i = 0; i < this.biVector.length; i++) {
            for (int j = 0; j < this.biVector[i].length; j++) {
                if (getTheEvenPercentagePerElem(this.biVector[i][j]) > getAvgOfEvenPercentageFromBiVector()) {
                    result.addElem(biVector[i][j]);
                }
            }
        }
        return result;
    }


    public void reverseMatrixRows() {
        isEmptyMatrix();
        int[][] reversed = new int[this.biVector.length][];
        for (int i = 0; i < this.biVector.length; i++) {
            reversed[i] = UsefulOperations.reverseRow(this.biVector[i]);
        }
        this.biVector = copyArray(reversed);
    }

    public void reverseMatrixColumns() {
        isEmptyMatrix();
        int[][] matrix = toMatrix();
        int[][] reversed = UsefulOperations.reverseColumnInMatrix(matrix);
        BiVector result = new BiVector(reversed);
        this.biVector = result.toMatrix();
    }


    /**
     * Método para rodar a matriz 90 graus para a direita
     * Só se aplica a matrizes quadradas
     */
    public void rotateLeftNinetyDegrees() {
        isEmptyMatrix();
        int[][] matrix = UsefulOperations.obterMatrizTransposta(this.biVector);
        BiVector transposed = new BiVector(matrix);
        transposed.reverseMatrixColumns();
        this.biVector = transposed.toMatrix();
    }

    /**
     * Método para rodar a matriz 90 graus para a direita
     * Só se aplica a matrizes quadradas
     */
    public void rotateRigthNinetyDegrees() {
        isEmptyMatrix();
        if (isSquaredMatrix()) {
            int[][] matrix = UsefulOperations.obterMatrizTransposta(this.biVector);
            BiVector transposed = new BiVector(matrix);
            transposed.reverseMatrixRows();
            this.biVector = transposed.toMatrix();
        }
    }

    /**
     * Método para rodar a matriz em 180 graus
     * Só é possível ser aplicado para matrizes quadradas
     */
    public void rotateRigthHundredAndEigthyDegrees() {
        isEmptyMatrix();
        if (isSquaredMatrix()) {
            int[][] matrix = toMatrix();
            BiVector finalMatrix = new BiVector(matrix);
            finalMatrix.reverseMatrixRows();
            finalMatrix.reverseMatrixColumns();
            this.biVector = finalMatrix.toMatrix();
        }
    }
}






