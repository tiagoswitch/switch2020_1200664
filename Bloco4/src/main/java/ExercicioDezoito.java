public class ExercicioDezoito {

    // Definiu-se uma matriz de 7x7

    public static final int row = 7;
    public static final int columm = 7;


    public static void main(String[] args) {

        char[][] m1 = {{'A', 'E', 'B', 'P', 'S', 'F', 'O'},
                       {'R', 'B', 'I', 'I', 'K', 'S', 'K'},
                       {'A', 'S', 'B', 'B', 'S', 'Q', 'U'},
                       {'I', 'Z', 'O', 'A', 'E', 'C', 'L'},
                       {'A', 'D', 'E', 'Q', 'A', 'P', 'A'},
                       {'J', 'C', 'T', 'I', 'B', 'E', 'L'},
                       {'O', 'G', 'T', 'I', 'C', 'E', 'L'}};


        String palavra = "";
        char[] pala = {'E','I','B'};
        boolean s = verificarDireccoesDoInicioAoFim(m1,0,1,2,3,pala);
        System.out.println(s);

        /*
        int [][]n = obterMatrizMascara(m1,"BSQ");
        for (int i = 0; i <n.length ; i++) {
            System.out.println();
            for (int j = 0; j <n[i].length ; j++) {
                System.out.print(" "+n[i][j]);
            }
        }
        */

        boolean sec = verificarDireccoesDoInicioAoFim(m1,0,0,3,3,pala);


        boolean sdas = verificarSeDuasPalavrasLetrasComuns(m1,"BSQ","QCP");


    }

    /**
     * Alínea a) Criar uma matriz máscara em que recebe uma máscara e um char
     *
     * @param matrizJogo
     * @param l
     * @return uma matriz com 0s e 1s, onde está presente a letra na matriz é colocado um 1 na matriz mascara o resto é zeros
     */

    public static int[][] obterMatrizMascaraParaUmChar(char[][] matrizJogo, char l) {
        if (matrizJogo == null || matrizJogo.length == 0 || matrizJogo.length != row || matrizJogo[0].length != columm)
            return null;
        else {
            int[][] m2 = new int[row][columm];
            for (int i = 0; i < matrizJogo.length; i++) {
                for (int j = 0; j < matrizJogo[i].length; j++) {
                    if (matrizJogo[i][j] == l || matrizJogo[i][j] == Character.toUpperCase(l) || matrizJogo[i][j] == Character.toLowerCase(l)) {
                        m2[i][j] = 1;
                    } else {
                        m2[i][j] = 0;
                    }
                }
            }
            return m2;
        }
    }

    /**
     * Método para validar uma matriz com chars,
     *
     * @param m1
     * @return retorna false se for diferente de uma matriz de 7 por 7
     */

    public static boolean validarMatriz(char[][] m1) {
        if (m1 == null || m1.length != row || m1[0].length != columm) return false;
        else {
            return true;
        }
    }


    /**
     * Método que recebe uma String com uma palavra e retorna um array de chars
     *
     * @param palavra
     * @return se a palavra for vazia vai retornar um array vazio
     */

    public static char[] charParaArray(String palavra) {
        char[] array = new char[palavra.length()];
        for (int i = 0; i < palavra.length(); i++) {

            array[i] = palavra.charAt(i);
        }
        return array;
    }

    /**
     * Método para verificar se uma palavra está na sopa de letras. Vai procurar a primeira letra dessa palavra,
     * e assim que encontrar vai chamar o metodo verificarDireccoes para verificar todas as direcções possíveis
     * em que a palavra possa estar
     * @param matrizJogo
     * @param palavra
     * @return retorna true se encontrar a palavra escolhida
     */

    public static boolean verificarSeAPalavraEstaNaMatriz(char[][] matrizJogo, String palavra) {

        if (!validarMatriz(matrizJogo)) return false;
        char[] caracteres = charParaArray(palavra);
        if (caracteres.length == 0) {
            return false;
        }
        boolean palavraEncontrada = false;
        for (int i = 0; i < matrizJogo.length && !palavraEncontrada; i++) {
            for (int j = 0; j < matrizJogo[i].length && !palavraEncontrada; j++) {
                if (obterMatrizMascaraParaUmChar(matrizJogo, caracteres[0])[i][j] == 1) {
                    if (verificarDireccoes(matrizJogo, i, j, caracteres)) {
                        palavraEncontrada = true;
                    }
                }
            }
        }
        return palavraEncontrada;
    }

    /**
     * Método para verificar todas as direcções possíveis
     * @param matrizJogo
     * @param linhaInicial
     * @param colunaInicial
     * @param caracteres
     * @return se encontrar a palavra retorna true
     */

    public static boolean verificarDireccoes(char[][] matrizJogo, int linhaInicial, int colunaInicial, char[] caracteres) {
        boolean palavraEncontrada = false;
        if (verificarADireccaoEsqDireita(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoDireitaEsquerda(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoCimaBaixo(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoBaixoCima(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoDiagonalBaixoDireita(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoDiagonalBaixoEsquerda(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoDiagonalCimaDireita(matrizJogo, linhaInicial, colunaInicial, caracteres) ||
                verificarADireccaoDiagonalCimaEsquerda(matrizJogo, linhaInicial, colunaInicial, caracteres)) {
            palavraEncontrada = true;
        }

        return palavraEncontrada;
    }

    /**
     * Verifica se a palavra existe na sopa de letras na horizontal da esquerda para a direita
     *
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return retorna true se encontrar a palavra pretendida
     */

    public static boolean verificarADireccaoEsqDireita(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        if (!validarMatriz(matrizJogo)) return false;
        boolean palavraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = coluna; (i < caracteres.length + coluna && i < matrizJogo.length) && resultado; i++) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[linha][i]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) {
                    resultado = true;
                }
            }
        }
        if (indiceLetra == caracteres.length) {
            palavraEncontrada = true;
        }
        return palavraEncontrada;
    }

    /**
     * Verifica se a palavra existe na sopa de letras na horizontal da direita para a esquerda
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return retorna true se encontrar a palavra na sopa de letras
     */

    public static boolean verificarADireccaoDireitaEsquerda(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean palavraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = coluna; (i > coluna - caracteres.length && i >= 0) && resultado; i--) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[linha][i]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            palavraEncontrada = true;
        }
        return palavraEncontrada;
    }


    /**
     * Verifica se a palavra existe na sopa de letras na vertical de cima para baixo
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return retorna true se encontrar a palavra na sopa de letras
     */

    public static boolean verificarADireccaoCimaBaixo(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean palavraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha; (i < linha + caracteres.length && i < matrizJogo.length) && resultado; i++) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][coluna]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length)
                    resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            palavraEncontrada = true;
        }
        return palavraEncontrada;
    }

    /**
     * Verifica se a palavra existe na sopa de letras na vertical de baixo para cima
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return
     */

    public static boolean verificarADireccaoBaixoCima(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean palavraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha; (i > linha - caracteres.length && i >= 0) && resultado; i--) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][coluna]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            palavraEncontrada = true;
        }
        return palavraEncontrada;


    }


    /**
     * Verifica se a palavra existe na sopa de letras na diagonal de baixo para direita
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return
     */

    public static boolean verificarADireccaoDiagonalBaixoDireita(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean letraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha, j = coluna; (i < linha + caracteres.length && j < coluna + caracteres.length) && (i < matrizJogo.length && j < matrizJogo.length) && resultado; j++, i++) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][j]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) {
                    resultado = true;
                }
            }
        }
        if (indiceLetra == caracteres.length) {
            letraEncontrada = true;
        }
        return letraEncontrada;
    }

    /**
     * Verifica se a palavra existe na sopa de letra na diagonal descendente esquerda
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return
     */

    public static boolean verificarADireccaoDiagonalBaixoEsquerda(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean letraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha, j = coluna; (i < linha + caracteres.length && j > coluna - caracteres.length) && (j >= 0 && i < matrizJogo.length) && resultado; i++, j--) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][j]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            letraEncontrada = true;
        }
        return letraEncontrada;
    }

    /**
     *Verifica se a palavra existe na sopa de letra na diagonal ascendente esquerda
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return
     */

    public static boolean verificarADireccaoDiagonalCimaEsquerda(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean letraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha, j = coluna; (i > linha - caracteres.length && j > coluna - caracteres.length) && (j >= 0 && i >= 0) && resultado; i--, j--) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][j]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            letraEncontrada = true;
        }
        return letraEncontrada;
    }

    /**
     * Verifica se a palavra existe na sopa de letra na diagonal ascendente esquerda
     * @param matrizJogo
     * @param linha
     * @param coluna
     * @param caracteres
     * @return
     */

    public static boolean verificarADireccaoDiagonalCimaDireita(char[][] matrizJogo, int linha, int coluna, char[] caracteres) {
        boolean letraEncontrada = false;
        boolean resultado = true;
        int indiceLetra = 0;
        for (int i = linha, j = coluna; (i > linha - caracteres.length && j < coluna + caracteres.length) && (i >= 0 && j < caracteres.length) && resultado; i--, j++) {
            resultado = false;
            if (caracteres[indiceLetra] == matrizJogo[i][j]) {
                indiceLetra++;
                if (indiceLetra != caracteres.length) resultado = true;
            }
        }
        if (indiceLetra == caracteres.length) {
            letraEncontrada = true;
        }
        return letraEncontrada;
    }

    // Alínea c)
    /** Método para verificar se dado determinadas coordenadas consegue encontrar uma palavra indicada
     *
     * @param matrizJogo
     * @param linhaInicio
     * @param colunaInicio
     * @param linhaFim
     * @param colunaFim
     * @param caracteres
     * @return
     */


    public static boolean verificarDireccoesDoInicioAoFim(char [][] matrizJogo, int linhaInicio, int colunaInicio, int linhaFim, int colunaFim, char [] caracteres)
    {
        if(!validarMatriz(matrizJogo)||!(validarLinhasEColunasInseridas(matrizJogo,linhaInicio,linhaFim,colunaInicio,colunaFim)))return false;
        boolean palavraEncontrada = false;
        if (linhaInicio == linhaFim && colunaInicio < colunaFim) {
            palavraEncontrada = verificarADireccaoEsqDireita(matrizJogo,linhaInicio,colunaInicio,caracteres);
        } else if (linhaInicio == linhaFim && colunaInicio > colunaFim) {
            palavraEncontrada = verificarADireccaoDireitaEsquerda(matrizJogo,linhaInicio,colunaInicio,caracteres);
        } else if (linhaInicio < linhaFim && colunaInicio == colunaFim) {
            palavraEncontrada = verificarADireccaoCimaBaixo(matrizJogo,linhaInicio, colunaInicio,caracteres);
        } else if (linhaInicio > linhaFim && colunaInicio == colunaFim) {
            palavraEncontrada = verificarADireccaoBaixoCima(matrizJogo, linhaInicio, colunaInicio,caracteres);
        } else if (linhaInicio < linhaFim && colunaInicio < colunaFim) {
            palavraEncontrada =  verificarADireccaoDiagonalBaixoDireita(matrizJogo, linhaInicio, colunaInicio, caracteres);
        } else if (linhaInicio < linhaFim && colunaInicio > colunaFim) {
            palavraEncontrada = verificarADireccaoDiagonalBaixoEsquerda(matrizJogo, linhaInicio, colunaInicio,caracteres);
        } else if (linhaInicio > linhaFim && colunaInicio < colunaFim) {
            palavraEncontrada = verificarADireccaoDiagonalCimaDireita(matrizJogo, linhaInicio,colunaInicio,caracteres);
        } else {
            palavraEncontrada = verificarADireccaoDiagonalCimaEsquerda(matrizJogo, linhaInicio, colunaInicio, caracteres);
        }
        return palavraEncontrada;
    }



    public static boolean validarLinhasEColunasInseridas(char [][] matrizJogo, int linhaInicio, int linhaFim, int colunaInicio, int colunaFim)
    {
        if(linhaInicio>0 && linhaInicio< matrizJogo.length
                || linhaFim >0 && linhaFim< matrizJogo.length
                || colunaInicio >0 && colunaInicio< matrizJogo.length
                || colunaFim>0 && colunaFim< matrizJogo.length) return true;

        else return false;
    }


    /** Método que retorna a matriz mascara de uma palavra
     *
     * @param matriz
     * @param palavra
     * @return
     */

    public static int[][] obterMatrizMascara(char[][] matriz, String palavra) {
        if (!(validarMatriz(matriz)) || palavra.length() == 0) return null;
        char[] arrayChars = charParaArray(palavra);
        int[][] matrizMascara = new int[matriz.length][matriz.length];
        int cont =0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                for (int k = 0; k < arrayChars.length; k++) {
                    if (arrayChars[k] == matriz[i][j]) {
                        cont++;
                    } else {
                        matrizMascara[i][j] = 0;
                    }
                }
                if (cont == 1 && verificarSeAPalavraEstaNaMatriz(matriz,palavra))
                {
                    matrizMascara[i][j] = 1;
                    cont = 0;
                }
            }
        }
        return matrizMascara;
    }

    /**
     * Método para verificar se duas palavras tem letras em comum
     * @param matriz
     * @param palavra1
     * @param palavra2
     * @return
     */


    public static boolean verificarSeDuasPalavrasLetrasComuns(char[][] matriz, String palavra1, String palavra2) {

        boolean resultado = false;
        if (!(validarMatriz(matriz)) || palavra1.length() == 0 || palavra2.length() == 0) return false;

        if (!(verificarSeAPalavraEstaNaMatriz(matriz, palavra1)) || !(verificarSeAPalavraEstaNaMatriz(matriz, palavra2))) {
            return false;
        }
        int cont =0;
        int[][] matrizMascara1 = obterMatrizMascara(matriz, palavra1);
        int[][] matrizMascara2 = obterMatrizMascara(matriz, palavra2);
        int[][] soma = obterSomaDeMatrizesMascaras(matrizMascara1, matrizMascara2);

        for (int i = 0; i < soma.length; i++) {
            for (int j = 0; j < soma[i].length; j++) {
                if (soma[i][j] == 2) {
                   cont++;
                } else {
                    resultado = false;
                }
            }
        }
        if(cont>0) resultado=true;

        return resultado;
    }


    /** Método para somar duas matrizes
     *
     * @param m1
     * @param m2
     * @return
     */

    public static int[][] obterSomaDeMatrizesMascaras(int[][] m1, int[][] m2) {
        int[][] resultado = null;
        if (!(ExercicioQuinze.eMatrizValida(m1)) || !(ExercicioQuinze.eMatrizValida(m2))) return null;
        else if (m1.length != m2.length || m1[0].length != m2[0].length) {
            return null;
        } else {
            resultado = new int[m1.length][m1[0].length];
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m1[0].length; j++) {

                    resultado[i][j] = m1[i][j] + m2[i][j];
                }
            }
            return resultado;
        }
    }



}