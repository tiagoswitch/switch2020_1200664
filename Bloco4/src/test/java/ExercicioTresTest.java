import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void somaElementosVetor() {
        int []n = {1,2,4,5,6};
        int expected =18;
        int result = ExercicioTres.somaElementosVetor(n);
        assertEquals(expected,result);

    }

    @Test
    void somaElementosVetorTesteDois() {
        int []n = {0,0,0,0,0};
        int expected =0;
        int result = ExercicioTres.somaElementosVetor(n);
        assertEquals(expected,result);

    }

    @Test
    void somaElementosVetorTesteTres() {
        int []n = {1,3,0,5,6,0,0,23};
        int expected =38;
        int result = ExercicioTres.somaElementosVetor(n);
        assertEquals(expected,result);

    }

    @Test
    void somaElementosVetorTesteQuatro() {
        int []n = {};
        int expected =0;
        int result = ExercicioTres.somaElementosVetor(n);
        assertEquals(expected,result);

    }



}