import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void indiceMenorValorTeste() {
        int [] v ={1,2,3,4,5};
        int expected =1;
        int result = ExercicioDez.indiceMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void indiceMenorValorTesteDois() {
        int [] v ={1,2,-3,4,0};
        int expected =-3;
        int result = ExercicioDez.indiceMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void indiceMenorValorTesteTres() {
        int [] v ={1,2,3,4,0};
        int expected =0;
        int result = ExercicioDez.indiceMenorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void indiceMenorValorTesteQuatro() {
        int [] v ={};
        int expected =0;
        int result = ExercicioDez.indiceMenorValor(v);
        assertEquals(expected,result);

    }


    @Test
    void indiceMaiorValor() {
        int [] v ={1,2,3,4,0};
        int expected =4;
        int result = ExercicioDez.indiceMaiorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void indiceMaiorValorTesteDois() {
        int [] v ={1,2,-2,4,0};
        int expected =4;
        int result = ExercicioDez.indiceMaiorValor(v);
        assertEquals(expected,result);

    }

    @Test
    void indiceMaiorValorTesteTres() {
        int [] v ={};
        int expected =0;
        int result = ExercicioDez.indiceMaiorValor(v);
        assertEquals(expected,result);

    }


    @Test
    void indiceMaiorValorTesteTesteQuatro() {
        int [] v ={1,5,7,9,145};
        int expected =145;
        int result = ExercicioDez.indiceMaiorValor(v);
        assertEquals(expected,result);

    }


    @Test
    void valorMedioElemVetor() {
        int []v = {1,2,3,4,5};
        double expected = 3.0;
        double result = ExercicioDez.obterValorMedioDosElemVetor(v);
        assertEquals(expected,result);

    }


    @Test
    void valorMedioElemVetorTesteDois() {
        int []v = {1,2,0,5,2};
        double expected = 2.0;
        double result = ExercicioDez.obterValorMedioDosElemVetor(v);
        assertEquals(expected,result);

    }


    @Test
    void valorMedioElemVetorTesteTres() {
        int []v = {1,2,3,-1,5};
        double expected = 2.0;
        double result = ExercicioDez.obterValorMedioDosElemVetor(v);
        assertEquals(expected,result);

    }


    @Test
    void valorMedioElemVetorTesteQuatro() {
        int []v = {};
        double expected = 0;
        double result = ExercicioDez.obterValorMedioDosElemVetor(v);
        assertEquals(expected,result);

    }


    @Test
    void produtoDosElemDoVector() {
        int []v = {};
        double expected=0;
        double result = ExercicioDez.obterprodutoDosElemDoVector(v);
        assertEquals(expected,result);
    }

    @Test
    void produtoDosElemDoVectorTesteDois() {
        int []v = {1,2,3,4,5};
        double expected=120.0;
        double result = ExercicioDez.obterprodutoDosElemDoVector(v);
        assertEquals(expected,result);
    }

    @Test
    void produtoDosElemDoVectorTesteTres() {
        int []v = {0,0,0,0};
        double expected=0;
        double result = ExercicioDez.obterprodutoDosElemDoVector(v);
        assertEquals(expected,result);
    }

    @Test
    void produtoDosElemDoVectorTesteQuatro() {
        int []v = {-1,0,2,3};
        double expected=0;
        double result = ExercicioDez.obterprodutoDosElemDoVector(v);
        assertEquals(expected,result);
    }


    @Test
    void getConjuntoDeNaoRep() {
        int[] v = {10,2,10,14,24,3,24,11};
        int[] expected = {2,3,10,11,14,24};
        int [] result = ExercicioDez.obterValoresNaoRepetidos(v);
        assertArrayEquals(expected, result);

    }

    @Test
    void getConjuntoDeNaoRepTesteDois() {
        int []v = {};
        int [] expected = null;
        int [] result = ExercicioDez.obterValoresNaoRepetidos(v);
        assertArrayEquals(expected, result);

    }

    @Test
    void getConjuntoDeNaoRepTesteTres() {
        int [] v = {2,2,2,2,2,2,2};
        int [] expected = {2};
        int [] result = ExercicioDez.obterValoresNaoRepetidos(v);
        assertArrayEquals(expected, result);

    }

    @Test
    void getConjuntoDeNaoRepTesteQuatro() {
        int [] v = {2,2,2,2,2,2,2,1,3};
        int [] expected = {1,2,3};
        int [] result = ExercicioDez.obterValoresNaoRepetidos(v);
        assertArrayEquals(expected, result);

    }


    @Test
    void devoldeInvertido() {
        int []v = {};
        int []expected= {};
        int [] result = ExercicioDez.devolveInvertido(v);
        assertArrayEquals(expected,result);
        assertNotSame(expected,result);

    }

    @Test
    void devoldeInvertidoTesteDois() {
        int []v = {1,2,3};
        int []expected= {3,2,1};
        int [] result = ExercicioDez.devolveInvertido(v);
        assertArrayEquals(expected,result);
        assertNotSame(expected,result);

    }

    @Test
    void devoldeInvertidoTesteTres() {
        int []v = {0};
        int []expected= {0};
        int [] result = ExercicioDez.devolveInvertido(v);
        assertArrayEquals(expected,result);
        assertNotSame(expected,result);

    }

    @Test
    void ePrimo() {
        int n=0;
        boolean expected = false;
        boolean result = ExercicioDez.ePrimo(n);
        assertEquals(expected,result);

    }


    @Test
    void ePrimoTesteDois() {
        int n=-1;
        boolean expected = false;
        boolean result = ExercicioDez.ePrimo(n);
        assertEquals(expected,result);

    }

    @Test
    void ePrimoTesteTres() {
        int n=3;
        boolean expected = true;
        boolean result = ExercicioDez.ePrimo(n);
        assertEquals(expected,result);

    }

    @Test
    void ePrimoTesteQuatro() {
        int n=7;
        boolean expected = true;
        boolean result = ExercicioDez.ePrimo(n);
        assertEquals(expected,result);

    }


    @Test
    void obterElemPrimos() {
        int []v ={};
        int []expected = null;
        int [] result = ExercicioDez.obterElemPrimos(v);
        assertArrayEquals(expected,result);

    }

    @Test
    void obterElemPrimosTesteDois() {
        int []v ={4,4,4,4};
        int []expected = {};
        int [] result = ExercicioDez.obterElemPrimos(v);
        assertArrayEquals(expected,result);


    }

    @Test
    void obterElemPrimosTesteTres() {
        int []v ={6,2,12,10};
        int []expected = {2};
        int [] result = ExercicioDez.obterElemPrimos(v);
        assertArrayEquals(expected,result);

    }



}