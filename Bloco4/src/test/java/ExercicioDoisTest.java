import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void devolveVetor() {

        int n =123;
        int [] expected={1,2,3};
        int[] result = ExercicioDois.devolveVetor(n);
        assertArrayEquals(expected,result);
    }

    @Test
    void devolveVetorTesteDois() {

        int n =123678;
        int [] expected={1,2,3,6,7,8};
        int[] result = ExercicioDois.devolveVetor(n);
        assertArrayEquals(expected,result);
    }

    @Test
    void devolveVetorTesteTres() {

        int n =1;
        int [] expected={1};
        int[] result = ExercicioDois.devolveVetor(n);
        assertArrayEquals(expected,result);
    }
    @Test
    void devolveVetorTesteQuatro() {

        int n =-1;
        int [] expected={};
        int[] result = ExercicioDois.devolveVetor(n);
        assertArrayEquals(expected,result);
    }




}