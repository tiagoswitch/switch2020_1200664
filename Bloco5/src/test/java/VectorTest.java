import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class VectorTest {

    @Test
    void setVectorTestNullCase() {
        int[] array = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Vector v1 = new Vector(array);
        });
    }

    @Test
    void copyArray() {

        int[] array = {4, 3, 2, 1};
        Vector v1 = new Vector(array);
        int[] expected = {4, 3, 2, 1};
        int[] result = v1.copyArray(array, array.length);
        assertNotSame(array, result);
        assertArrayEquals(expected, result);
    }

    @Test
    void CopyArrayTestCaseEmptyVector() {

        int[] array = {1};
        Vector v1 = new Vector(array);
        int[] expected = {1};

        int[] result = v1.copyArray(array, array.length);
        // assert
        assertNotSame(array, result);
        assertArrayEquals(expected, result);
    }

    @Test
    void checkIfElemIsInArray() {

        int[] v1 = {1, 2, 3};
        int elem = 0;
        Vector vector = new Vector(v1);
        boolean expected = false;
        boolean result = vector.checkIfElemIsInArray(elem);
        assertEquals(expected, result);

    }

    @Test
    void removefirstElem() {
        int[] a = {1, 2, 3};
        int elem = 1;
        Vector v1 = new Vector(a);
        v1.removefirstElem(elem);
        int[] expected = {2, 3};
        int[] result = v1.toArray();
        assertArrayEquals(expected, result);

    }

    @Test
    void removefirstElemTestCaseElemNotPresent() {
        int[] a = {1, 2, 3};
        int elem = -4;
        Vector v1 = new Vector(a);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.removefirstElem(elem);
        });

    }

    @Test
    void testRemovefirstElemTestCaseElemNotPresentCaseEmptyArray() {
        int[] a = {};
        int elem =0;
        Vector v1 = new Vector(a);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.removefirstElem(elem);
        });

    }





    @Test
    void addElem() {

        int[] a = {1, 2, 3};
        Vector result = new Vector(a);
        int elem = 1;
        int[] exp = {1, 2, 3, 1};
        Vector expected = new Vector(exp);
        result.addElem(elem);
        assertTrue(expected.equals(result));
        //assertArrayEquals(expected.toArray(),result.toArray());
    }

    @Test
    void getValueFromIndexCaseOutOfBounds() {

        int index = -1;
        Vector v1 = new Vector();

        assertThrows(IllegalArgumentException.class, () -> {
            v1.getValueFromIndex(index);
        });

    }

    @Test
    void getValueFromIndexTest() {

        int index = 1;
        int[] a = {1, 2, 3, 4};
        Vector v1 = new Vector(a);
        int expected = 2;
        int result = v1.getValueFromIndex(index);
        assertEquals(expected, result);
    }


    @Test
    void getNumberOfElemInArrayTestEmptyArray() {

        int[] v = {};
        Vector v1 = new Vector(v);
        int expected = 0;
        int result = v1.getNumberOfElemInArray();
        assertEquals(expected, result);

    }


    @Test
    void getTheBiggestValueTestEmptyMatrix() {
        int[] a = {};
        Vector v1 = new Vector(a);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.getTheBiggestValue();
        });
    }

    @Test
    void getTheBiggestValue() {
        int[] a = {1, 2, 3, 4};
        Vector v1 = new Vector(a);
        int expected = 4;
        int result = v1.getTheBiggestValue();
        assertEquals(expected, result);
    }


    @Test
    void getTheLowestValueTestCaseEmptyArrays() {
        int[] a = {};
        Vector v1 = new Vector(a);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.getTheLowestValue();
        });
    }


    @Test
    void getTheLowestValueTest() {
        int[] a = {1, 2, 3};
        Vector v1 = new Vector(a);
        int expect = 1;
        int result = v1.getTheLowestValue();
        assertEquals(expect, result);

    }


    @Test
    void getAverageFromArray() {
        int[] array = {1, 2, 3, 4};
        Vector v1 = new Vector(array);
        double expected = 2.5;
        double result = v1.getAverageFromArray();
        assertEquals(expected, result);
    }

    @Test
    void getAverageFromArrayTestCaseEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.getAverageFromArray();
        });
    }


    @Test
    void getAvgOfEvenNumbersTestCaseEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {

            v1.getAvgOfEvenNumbers();
        });
    }

    @Test
    void getAvgOfEvenNumbers() {
        int[] array = {1, 2, 3, 4};
        Vector v1 = new Vector(array);
        double expected = 3;
        double result = v1.getAvgOfEvenNumbers();
        assertEquals(expected, result);
    }


    @Test
    void getAvgOfOddNumbersTestEmptyArray() {

        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {

            v1.getAvgOfOddNumbers();

        });
    }

    @Test
    void getAvgOfOddNumbersTest() {

        int[] array = {1, 2, 3, 4};
        Vector v1 = new Vector(array);
        double expected = 2.0;
        double result = v1.getAvgOfOddNumbers();
        assertEquals(expected, result);

    }


    @Test
    void avgOfMultiplesOfANumber() {

        int num = 10;
        int[] array = {1, 1, 2};
        Vector v1 = new Vector(array);
        double expected = 2.6;
        double result = v1.avgOfMultiplesOfANumber(10);
        assertEquals(expected, result, 0.1);

    }

    @Test
    void avgOfMultiplesOfANumberTestEmpty() {

        int num = 10;
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {

            v1.avgOfMultiplesOfANumber(num);
        });
    }


    @Test
    void sortByNumbersAscTestEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.sortByNumbersAsc();
        });
    }

    @Test
    void sortByNumbersAsc() {
        int[] array = {4, 3, 1, 10};
        Vector result = new Vector(array);
        int[] exp = {1, 3, 4, 10};
        Vector expected = new Vector(exp);
        result.sortByNumbersAsc();
        assertArrayEquals(result.toArray(), expected.toArray());
        assertTrue(expected.equals(result));

    }


    @Test
    void sortByNumbersDescTestEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.sortByNumbersDesc();
        });
    }

    @Test
    void sortByNumbersDesc() {
        int[] array = {4, 3, 1, 10};
        Vector result = new Vector(array);
        int[] exp = {10, 4, 3, 1};
        Vector expected= new Vector(exp);
        result.sortByNumbersDesc();
        assertArrayEquals(result.toArray(), expected.toArray());
        assertTrue(result.equals(expected));

    }

    @Test
    void checkIfIsEmptyTestCaseTrue() {
        int[] array = {};
        Vector v1 = new Vector(array);
        boolean expected = true;
        boolean result = v1.checkIfIsEmpty();
        assertEquals(expected, result);

    }

    @Test
    void checkIfIsEmptyTestCaseFalse() {
        int[] array = {1, 2, 3};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfIsEmpty();
        assertEquals(expected, result);

    }


    @Test
    void checkIfHasOnlyOneElementTestCaseFalse() {
        int[] array = {};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfHasOnlyOneElement();
        assertEquals(expected, result);

    }

    @Test
    void checkIfHasOnlyOneElementTestCaseTrue() {
        int[] array = {10};
        Vector v1 = new Vector(array);
        boolean expected = true;
        boolean result = v1.checkIfHasOnlyOneElement();
        assertEquals(expected, result);

    }


    @Test
    void testGetAvgOfEvenNumbers() {

        int[] array = {2, 4, 6};
        Vector v1 = new Vector(array);
        boolean expected = true;
        boolean result = v1.checkIfHasOnlyEvenNumbers();
        assertEquals(expected, result);

    }

    @Test
    void testGetAvgOfEvenNumbersCaseFalse() {

        int[] array = {2, 4, 5};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfHasOnlyEvenNumbers();
        assertEquals(expected, result);

    }


    @Test
    void checkIfHasOnlyOddsNumbers() {
        int[] array = {1, 1, 5};
        Vector v1 = new Vector(array);
        boolean expected = true;
        boolean result = v1.checkIfHasOnlyOddsNumbers();
        assertEquals(expected, result);
    }

    @Test
    void checkIfHasOnlyOddsNumbersCaseFalse() {
        int[] array = {1, 1, 4};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfHasOnlyOddsNumbers();
        assertEquals(expected, result);
    }


    @Test
    void checkIfHasDuplicates() {

        int[] array = {};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfHasDuplicates();
        assertEquals(expected, result);
    }

    @Test
    void checkIfHasDuplicatesTestCaseFalse() {

        int[] array = {1, 2, 3, 4, 5};
        Vector v1 = new Vector(array);
        boolean expected = false;
        boolean result = v1.checkIfHasDuplicates();
        assertEquals(expected, result);
    }

    @Test
    void checkIfHasDuplicatesTestCaseTrue() {

        int[] array = {1, 2, 1, 4, 5};
        Vector v1 = new Vector(array);
        boolean expected = true;
        boolean result = v1.checkIfHasDuplicates();
        assertEquals(expected, result);
    }


    @Test
    void TestgetElemWhoseNofDigitsHigherThanTheAvgNumbOfDigits() {
        int[] array = {10, 2, 123};
        Vector v1 = new Vector(array);
        int[] expected = {123};
        int[] result = v1.getElemWhoseNofDigitsHigherThanTheAvgNumbOfDigits();
        assertArrayEquals(expected, result);


    }

    @Test
    void TestgetElemWhoseNofDigitsHigherThanTheAvgNumbOfDigitsCaseEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.getElemWhoseNofDigitsHigherThanTheAvgNumbOfDigits();
        });

    }


    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfAllEvenElementsCaseEmptyArray() {
        int[] array = {};
        Vector v1 = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            v1.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfAllEvenElements();
        });

    }

    @Test
    void testElementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfAllEvenElements() {
        int[] array = {12,22,11};
        Vector vector = new Vector(array);
        int [] exp = {22};
        Vector expected = new Vector(exp);
        Vector result =vector.elementsWithPercentageOfEvenDigitsGreaterThanTheMeanPercentageOfAllEvenElements();
        //assertTrue(expected.equals(result));
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void getElemWithAllEvenDigits() {

        int [] array = {12,22,44};
        Vector vector = new Vector(array);
        int [] exp = {22,44};
        Vector expected = new Vector(exp);
        Vector result = vector.getElemWithAllEvenDigits();
        assertTrue(expected.equals(result));
        assertArrayEquals(expected.toArray(),result.toArray());
    }

    @Test
    void TestgetElemWithAllEvenDigits() {

        int [] array = {12,21,41};
        Vector vector = new Vector(array);
        int [] exp = {};
        Vector expected = new Vector(exp);
        Vector result = vector.getElemWithAllEvenDigits();
        assertTrue(expected.equals(result));
        assertArrayEquals(expected.toArray(),result.toArray());
    }


    @Test
    void TestgetElemWithAllEvenDigitsCaseEmptyArray() {

        int [] array = {};
        Vector vector = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            vector.getElemWithAllEvenDigits();
        });
    }


    @Test
    void TestincreasingSequencesCaseEmptyArray() {
        int [] array = {};
        Vector vector = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
           vector.increasingSequences();
        });
    }

    @Test
    void TestincreasingSequencesCaseNotAscendingElements() {
        int [] array = {322,121,4321};
        Vector vector = new Vector(array);
        int [] exp = {};
        Vector expected = new Vector (exp);
        Vector result =vector.increasingSequences();
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void TestincreasingSequencesCaseAscendingElements() {
        int [] array = {322,-123,4321};
        Vector vector = new Vector(array);
        int [] exp = {};
        Vector expected = new Vector (exp);
        Vector result =vector.increasingSequences();
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void TestgetpalindromesCaseEmptyArray() {
        int [] array = {};
        Vector vector = new Vector(array);
        assertThrows(IllegalArgumentException.class, () -> {
            vector.getpalindromes();
        });
    }


    @Test
    void TestgetpalindromesCaseNoPalindromes() {
        int [] array = {322,211,4321};
        Vector vector = new Vector(array);
        int [] exp = {};
        Vector expected = new Vector (exp);
        Vector result =vector.getpalindromes();
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.equals(result));

    }
    @Test
    void TestgetpalindromesCaseHasPalindromes() {
        int [] array = {322,11411,4321};
        Vector vector = new Vector(array);
        int [] exp = {11411};
        Vector expected = new Vector (exp);
        Vector result =vector.getpalindromes();
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.equals(result));

    }


    @Test
    void TesthasTheSameDigitsCase() {
        int [] array = {322,111,4321};
        Vector vector = new Vector(array);
        int [] exp = {111};
        Vector expected = new Vector (exp);
        Vector result =vector.hasTheSameDigits();
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.equals(result));


    }

    @Test
    void TesthasNoneElemTheSameDigits() {
        int [] array = {322,11112,4321};
        Vector vector = new Vector(array);
        int [] exp = {};
        Vector expected = new Vector (exp);
        Vector result =vector.hasTheSameDigits();
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.equals(result));

    }


    @Test
    void armstrongNumbers() {

        int [] array = {12,153,456};
        Vector vector = new Vector(array);
        int [] exp = {153};
        Vector expected = new Vector(exp);
        Vector result = vector.armstrongNumbers();
        assertTrue(expected.equals(result));
        assertArrayEquals(expected.toArray(),result.toArray());
    }


    @Test
    void increasingSeqGivenTheSize() {


            int[] array = {16, 15175, 367, 765};
            Vector vector = new Vector(array);
        int[] intExpected = {16, 367};
            Vector expected = new Vector(intExpected);
            Vector result = vector.increasingSeqGivenTheSize(3);
            assertArrayEquals(expected.toArray(), result.toArray());
        }

    @Test
    void testisMatchingEmptyArray() {
        int [] array = {1};
        Vector result = new Vector();
        assertThrows(IllegalArgumentException.class, () -> {
           result.checkIfisMatching(array);
        });
    }

    @Test
    void testIsMatchingFalse() {
        int [] array = {1};
        Vector result = new Vector(array);
        int[] exp ={1,2,3};
        Vector expected = new Vector(exp);
        boolean r = result.checkIfisMatching(exp);
        assertFalse(result.equals(expected));
        assertNotSame(result.toArray(),exp);

    }

    @Test
    void testIsMatchingTrue() {
        int [] array = {1,2,3};
        Vector result = new Vector(array);
        int[] exp ={1,2,3};
        Vector expected = new Vector(exp);
        boolean r= result.checkIfisMatching(expected.toArray());
        boolean e= true;
        assertTrue(r);
        assertTrue(result.equals(expected));
        assertArrayEquals(result.toArray(),expected.toArray());

    }




}