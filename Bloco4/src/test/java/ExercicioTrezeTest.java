import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void eQuadrada() {
        int [][] v = {{1,2},{1,2}};
        boolean expected = true;
        boolean result = ExercicioTreze.eQuadrada(v);
        assertEquals(expected,result);

    }


    @Test
    void eQuadradaTesteDois() {
        int [][] v = {{},{}};
        boolean expected = false;
        boolean result = ExercicioTreze.eQuadrada(v);
        assertEquals(expected,result);

    }


    @Test
    void eQuadradaTesteTres() {
        int [][] v = {{1,2,1},{1,2}};
        boolean expected = false;
        boolean result = ExercicioTreze.eQuadrada(v);
        assertEquals(expected,result);

    }


    @Test
    void eQuadradaTesteQuatro() {
        int [][] v = {{1,2,1},{1,2,3},{1,2,3}};
        boolean expected = true;
        boolean result = ExercicioTreze.eQuadrada(v);
        assertEquals(expected,result);

    }

    @Test
    void eQuadradaTesteCinco() {
        int [][] v = {{1,2,1,4},{1,2,3,3},{1,2,3,2},{1,1,1,1}};
        boolean expected = true;
        boolean result = ExercicioTreze.eQuadrada(v);
        assertEquals(expected,result);

    }











}