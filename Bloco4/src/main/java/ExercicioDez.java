import java.util.Arrays;

public class ExercicioDez {
    public static void main(String[] args) {

    }

    //Alinea a)
    public static int indiceMenorValor(int[] v) {

        if (v.length == 0) {
            return 0; // Se não tiver nada retorna 0
        }

        int minimo = v[0];
        for (int i = 0; i < v.length; i++) {
            // O para haver comparação o minimo tem que ser atribuído ao 1º valor
            if (v[i] < minimo) {
                minimo = v[i];
            }
        }
        return minimo;
    }

    // Alinea b)
    public static int indiceMaiorValor(int[] v) {

        if (v.length == 0) {
            return 0; // Se não tiver nada retorna 0
        }

        int max = v[0];
        for (int i = 0; i < v.length; i++) {
            // O para haver comparação o max tem que ser atribuído ao 1º valor
            if (v[i] > max) {
                max = v[i];
            }

        }
        return max;
    }

// alinea c)

    public static double obterValorMedioDosElemVetor(int[] v) {
        int soma = 0;
        double media = 0;
        if (v.length == 0) {
            return 0; // se tiver size 0 vai retornar 0
        }
        for (int i = 0; i < v.length; i++) {
            soma = soma + v[i];

        }
        media = soma / (v.length);
        return media;
    }


    // alinea d)
    public static double obterprodutoDosElemDoVector(int[] v) {
        double produto = 1;
        if (v.length == 0) {
            return 0;
        }
        for (int i = 0; i < v.length; i++) {
            produto = produto * v[i];
            if (v[i] == 0) {
                return 0;
            }
        }
        return produto;
    }

    /*
    Alínea e) obter os valores não repetidos, ou seja os valores únicos

     */

    public static int[] obterValoresNaoRepetidos(int[] array) {
        if (array.length == 0) {
            return null;
        }
        int k = 0; // iterador para o novo vector
        Arrays.sort(array);
        int[] conjuntoSemRepeticoes = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            boolean areEquals = false;
            for (int j = 0; j < i; j++) {
                if (array[i] == array[j]) { // verificar se iguais
                    areEquals = true;
                }
            }
            if (!areEquals) {
                conjuntoSemRepeticoes[k] = array[i];
                k++;
            }
        }
        // utilizar a função do exercício 6 que retorna um vector com o tamanho desejado
        return ExercicioSeis.copiarNelem(conjuntoSemRepeticoes,k);
    }


    //Alinea f) Método para obter novo array com elementos invertidos
    public static int[] devolveInvertido(int[] v) {
        int[] invertido = new int[v.length];
        if (v.length == 0) {
            return invertido; // Se for de tamanho zero tem que retornar um array com o mesmo tamanho
        }
        int j = 0;
        for (int i = v.length - 1; i >= 0; i--) {
            invertido[j] = v[i];
            j++;
        }
        return invertido;
    }


    // Alínea g) Método para obter primos
    // VÊ SE UM Número é primo
    public static boolean ePrimo(int n)
    {
        if(n<=1)return false;
        for (int i = 2; i < n; i++)
        {
            if(n%i==0)
                return false;
        }
        return true;
    }

    // corre as posições do array e vê uma a uma se é primo
    // coloca num novo array os valores que são primos
    public static int[] obterElemPrimos(int[] v) {
        if (v.length == 0) {
            return null;
        }
        int k = 0;   // iterador para o novo array
        int[] vetorPrimos = new int[v.length];
        for (int i = 0; i < v.length; i++) {
                if(ePrimo(v[i]))
                {
                    vetorPrimos[k] = v[i];
                    k++;
                }
            }
        return ExercicioSeis.copiarNelem(vetorPrimos, k);
    }


}