import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void mesmoTamanho() {
        int [] v1 ={};
        int [] v2 = {};

        boolean expected = true;
        boolean result = ExercicioOnze.mesmoTamanho(v1,v2);
        assertEquals(expected,result);

    }

    @Test
    void mesmoTamanhoTesteDois() {

        int [] v1 ={1,2};
        int [] v2 = {};

        boolean expected = false;
        boolean result = ExercicioOnze.mesmoTamanho(v1,v2);
        assertEquals(expected,result);

    }

    @Test
    void mesmoTamanhoTesteTres() {
        int [] v1 ={1,2,3};
        int [] v2 = {1,1,1};

        boolean expected = true;
        boolean result = ExercicioOnze.mesmoTamanho(v1,v2);
        assertEquals(expected,result);

    }


    @Test
    void obterProdutoEscalar() {
        int [] v1 = {};
        int [] v2 = {2,3};
        double expected = -1;
        double result = ExercicioOnze.obterProdutoEscalar(v1,v2);
        assertEquals(expected,result);
    }

    @Test
    void obterProdutoEscalarTesteDois() {
        int [] v1 = {1,3,4};
        int [] v2 = {2,3};
        double expected = -1;
        double result = ExercicioOnze.obterProdutoEscalar(v1,v2);
        assertEquals(expected,result);
    }

    @Test
    void obterProdutoEscalarTesteTres() {
        int [] v1 = {1,2};
        int [] v2 = {2,3};
        double expected = 8.0;
        double result = ExercicioOnze.obterProdutoEscalar(v1,v2);
        assertEquals(expected,result);
    }

    @Test
    void obterProdutoEscalarTesteQuatro() {
        int [] v1 = {1,2,3};
        int [] v2 = {2,3,4};
        double expected = 20.0;
        double result = ExercicioOnze.obterProdutoEscalar(v1,v2);
        assertEquals(expected,result);
    }



}