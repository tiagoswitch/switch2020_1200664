package com.company;

public class Bloco3Ex6 {


    public static void main(String[] args) {

        long n = 1112;
        double x = mediaDeAlgarismosImpares(n);
        System.out.println(x);


    }

    public static int nAlgarismos(long n)
    {
        int cont =1;
        while(n >9)
        {
            n=n/10;
            cont++;
        }
        return cont;

    }

    public static int nAlgarismosPares(long n)
    {
        int cont =0;
        while(n >0)
        {

             if((n%10)%2==0)cont++;
              n=(n/10);
        }
        return cont;

    }

    public static int nAlgarismosImpares(long n)
    {
        int cont =0;
        while(n >0)
        {

            if((n%10)%2!=0)cont++;
            n=(n/10);

        }
        return cont;

    }

    public static int somaAlgarismos(long n)
    {
        long alg;
        long soma =0;
        while (n>0)
        {
            soma= soma + n%10;
            n = n/10;
        }
        return (int) soma;
    }

    public static int somaAlgarismosPares(long n)
    {
        long soma =0;
        while (n>0)
        {
            if((n%10)%2==0)soma = soma + n%10;
            n= n/10;
        }
        return (int) soma;
    }

    public static int somaAlgarismosImpares(long n)
    {
        long alg;
        long r;
        long soma =0;
        while (n>0)
        {

            if((n%10)%2!=0)soma = soma + n%10;
            n= n/10;
        }
        return (int) soma;
    }


    public static double mediaDeAlgarismos(long n)
    {
        long alg;
        long soma =0;
        int cont=0;
        while (n>0)
        {
            soma= soma + n%10;
            n = n/10;
            cont++;
        }
        return soma/(double)cont;
    }

    public static double mediaDeAlgarismosPares(long n)
    {
        long alg;
        long soma =0;
        int cont=0;
        double media =0;
        while (n>0)
        {
            if((n%10)%2==0) {
                soma = soma + n % 10;
                cont++;
            }
                n = n/10;
        }
        media = soma/(double)cont;
        if(cont<1) media =0;
        return media;
    }

    public static double mediaDeAlgarismosImpares(long n)
    {

        long soma =0;
        int cont=0;
        double media =0;
        while (n>0)
        {
            if((n%10)%2!=0) {

                soma = soma + n%10;
                cont++;
            }
            n = n/10;
        }
        media =soma/(double)cont;
        if(cont<1)media =0;
        return media;
    }


    public static double ordemInversa(long n)
    {
        //123
        double reverse =0;
        while(n>0)
        {
            long d = (n%10);// sacar o ultimo;
            reverse= reverse * 10 + d;
            n= n/10; // cortando;
        }
        return reverse;
    }




















}
