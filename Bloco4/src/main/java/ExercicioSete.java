public class ExercicioSete {

    public static void main(String[] args) {
    }

    public static int nDeMultiplosNumIntervalo(int inicio,int fim,int n)
    {
        int temp=0; int cont=0;
        if(inicio> fim)
        {
            temp=fim;
            fim=inicio;
            inicio=temp;
        }
        if(inicio <0 || fim<0||n<=0) return 0;
        for (int i = inicio; i <=fim ; i++) {
            if(i%n==0)cont++;
        }
        return cont;
    }


    public static int[] multiplosDeTresNumInt(int inicio,int fim)
    {
        int dim = nDeMultiplosNumIntervalo(inicio,fim,3);
        //criar um índice para o novo array
        int temp=0;
        if(inicio> fim)
        {
            temp=fim;
            fim=inicio;
            inicio=temp;
        }
        int k=0;
        int []vector = new int[dim];
        for (int j = inicio; j <=fim ; j++) {
            if(j%3==0)
            {
                vector[k]=j;
                k++;
            }
        }
        return vector;
    }


    public static int[] multiplosDeNnumInt(int inicio,int fim,int n)
    {
        int dim = nDeMultiplosNumIntervalo(inicio,fim,n);
        //criar um índice para o novo array
        int k=0;int temp=0;
        if(inicio> fim)
        {
            temp=fim;
            fim=inicio;
            inicio=temp;
        }
        int []vector = new int[dim];
        if(dim==0 || n ==0)
        {
            return vector;
        }
        for (int j = inicio; j <=fim ; j++) {
            if(j%n==0)
            {
                vector[k]=j;
                k++;
            }
        }
        return vector;
    }


}
