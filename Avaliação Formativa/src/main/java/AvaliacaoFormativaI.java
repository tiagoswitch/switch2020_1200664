public class AvaliacaoFormativaI {

    public static void main(String[] args) {

        String s1= "abarcar";
        String s2="amarrar";
        double janela = calcularJanelaParaCorrespondencia(s1,s2);
        System.out.println("A janela é"+ janela);
        int n= calcularNumDeCaracteresCorrespondentes(s1,s2);
        System.out.println("numeros de caracteres "+ n);
        int t = calcularNumeroDeTransposicoes(s1,s2);
        System.out.println(" número de transposições "+ t);

    }

    // Função para passar uma string a um array de chars
    public static char[] stringParaArrayDeChars(String palavra) {

        char[] array = new char[palavra.length()];
        if(array.length==0) return array;
        for (int i = 0; i < palavra.length(); i++) {

            array[i] = palavra.charAt(i);
        }
        return array;
    }

    public static double calcularJanelaParaCorrespondencia(String palavra1, String palavra2)
    {
        //Vai retornar 0 caso o tamanho não seja válido
        if(palavra1.length()==0 || palavra2.length()==0) return 0;
        char[] s1= stringParaArrayDeChars(palavra1);
        char[] s2 = stringParaArrayDeChars(palavra2);

        double janela = (int)(Math.max(s1.length,s2.length)/2) - 1;

        return janela;

    }


    public static boolean validarMedidasDeProcura(int valorProcura, char[] palavra1, char[] palavra2)
    {
        if(valorProcura<0|| palavra1.length==0 || palavra2.length==0) return false;
        else if(valorProcura>palavra2.length || valorProcura>palavra1.length) return false;
        else return true;
    }


    public static int calcularNumDeCaracteresCorrespondentes(String s1, String s2)
    {
        int resultado =0;
        boolean correspondido=false;
        char [] palavra1 = stringParaArrayDeChars(s1);
        char [] palavra2= stringParaArrayDeChars(s2);
        double intDeValores = calcularJanelaParaCorrespondencia(s1,s2);
        if(palavra1.length ==0 || palavra2.length==0|| intDeValores<0) {
            resultado =-1;
            return resultado;
        }
        else
        {
            int charVerificados=0;
            int k1=0; // iterador para o novo vector c1;
            for (int i = 0; i <palavra1.length ; i++) {
                for (int j = 0; j <palavra2.length ; j++) {
                    for (int k = 0; k <=intDeValores; k++) {
                        if((validarMedidasDeProcura(i+k,palavra1,palavra2)  && i+k <palavra1.length && palavra2[j]== palavra1[i+k]
                                ||validarMedidasDeProcura(i-k,palavra1,palavra2)) && i-k>=0 && palavra2[j] == palavra1[i-k])
                        {
                            if(i==0 || charVerificados-1 >=0 && palavra2[charVerificados-1]== palavra2[j]){
                                resultado++;
                                charVerificados++;
                            }

                        }
                    }
                    i++;

                }
            }

        }

       return resultado;
    }


    public static int calcularNumeroDeTransposicoes(String s1, String s2)
    {

        int resultado =0;
        char [] palavra1 = stringParaArrayDeChars(s1);
        char [] palavra2= stringParaArrayDeChars(s2);
        double intDeValores = calcularJanelaParaCorrespondencia(s1,s2);
        if(palavra1.length ==0 || palavra2.length==0|| intDeValores<0) {
            resultado =-1;
            return resultado;
        }
        else {
            int charVerificados=0;
            char[] temporarioc1 = new char[palavra1.length+1];
            char[] temporarioc2 = new char[palavra2.length+1];
            int k1 = 0; // iterador para o novo vector c1;
            int k2=0;
            for (int i = 0; i < palavra1.length; i++) {
                for (int j = 0; j < palavra2.length; j++) {
                    for (int k = 0; k < intDeValores; k++) {
                        if (validarMedidasDeProcura(i + k, palavra1, palavra2) && i+k <palavra1.length && palavra2[j] == palavra1[i+k]
                                || validarMedidasDeProcura(i - k, palavra1, palavra2) && i-k>=0 && palavra2[j] == palavra1[i - k])  {

                            if(i==0 || charVerificados-1 >=0 && palavra2[charVerificados-1]== palavra2[j]) {

                                resultado++;
                                charVerificados++;
                                temporarioc1[k1] = palavra1[i];
                                temporarioc2[k2] = palavra2[j];
                                k1++;
                                k2++;
                            }
                        }

                    }
                }
            i++;
            }

            for (int l = 0; l <temporarioc1.length ; l++) {
                    if(temporarioc1[l]!=temporarioc2[l])
                    {
                        resultado++;
                    }

            }

        }
        return resultado/2;

    }

    public static float sim(String palavra1, String palavra2)
    {
        float resultado =0;
        int numeroDeCorrespondentes = calcularNumDeCaracteresCorrespondentes(palavra1,palavra2);
        int numeroDeTransposicoes = calcularNumeroDeTransposicoes(palavra1,palavra2);
        if(numeroDeCorrespondentes ==-1 || numeroDeTransposicoes==-1)
        {
            // Se for inválido retorna -1
            resultado=-1;

        }

        else if (numeroDeCorrespondentes==0)
        {
            resultado =0;
        }

        else {

            int nCharDaPalavra1= palavra1.length();
            int nCharDaPalavra2= palavra2.length();

            resultado = 1/3 * (numeroDeCorrespondentes/nCharDaPalavra1  + numeroDeCorrespondentes/nCharDaPalavra2 + (numeroDeCorrespondentes-numeroDeTransposicoes)/numeroDeCorrespondentes);

        }

        return resultado;

    }





}
