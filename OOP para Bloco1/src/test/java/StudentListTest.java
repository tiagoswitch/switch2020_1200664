import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentListTest {


    @Test
    void toArray() {
        Student st1 = new Student(1000005, "Silva");
        Student st2 = new Student(1000001, "Manel");
        Student st3 = new Student(1000006, "Silva");
        Student[] students = {st1, st2, st3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {st1, st2, st3};
        Student[] result = studentList.toArray();
        assertArrayEquals(expected, result);
    }

    @Test
    void sortByNumberAsc() {

        Student st1 = new Student(1000005, "Silva");
        Student st2 = new Student(1000001, "Manel");
        Student st3 = new Student(1000006, "Silva");

        Student[] students = {st1, st2, st3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {st2, st1, st3};
        studentList.sortByNumberAsc();
        assertArrayEquals(expected, studentList.toArray());
    }

    @Test
    void sortByNumberDesc() {
        Student st1 = new Student(1000005, "Silva");
        Student st2 = new Student(1000001, "Manel");
        Student st3 = new Student(1000006, "Silva");

        Student[] students = {st1, st2, st3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {st3, st1, st2};
        studentList.sortByNumberDesc();
        assertArrayEquals(expected, studentList.toArray());
    }


    @Test
    void sortByGradeDesc() {

        Student st1 = new Student(1000005, "Silva");
        st1.doEvaluation(12);
        Student st2 = new Student(1000001, "Manel");
        st2.doEvaluation(11);
        Student st3 = new Student(1000006, "Silva");
        st3.doEvaluation(13);

        Student[] students = {st1, st2, st3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {st3, st1, st2};
        studentList.sortByGradeDesc();
        assertArrayEquals(expected, studentList.toArray());
    }

}