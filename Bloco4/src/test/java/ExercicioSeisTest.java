import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void copiarNelem() {
        int [] v ={1,2,4,5,6,3,6,10};
        int n =4;
        int [] expected ={1,2,4,5};
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);

    }

    @Test
    void copiarNelemTesteDois() {
        int [] v ={1,2,4,5,6,3,6,10,3,6};
        int n =6;
        int [] expected ={1,2,4,5,6,3};
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);

    }

    @Test
    void copiarNelemTesteTres() {
        int [] v ={1,2,4,5,6,3,6,10};
        int n =0;
        int [] expected ={};
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);

    }

    @Test
    void copiarNelemTesteQuatro() {
        int [] v ={};
        int n =2;
        int [] expected =null;
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);
    }

    @Test
    void copiarNelemTesteCinco() {
        int [] v ={1,2,3};
        int n =4;
        int [] expected =null;
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);

    }

    @Test
    void copiarNelemTesteSeis() {
        int [] v ={1,2,3,4,5,6};
        int n =-22;
        int [] expected =null;
        int [] result = ExercicioSeis.copiarNelem(v,n);
        assertArrayEquals(expected,result);

    }

}