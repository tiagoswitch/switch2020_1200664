import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void obterDeterminanteDeMatriz() {

        int[][] v = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double expected = 0;
        double result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteDois() {

        int[][] v = {{1, 0, 3}, {4, 5, 6}, {7, 8, 9}};
        double expected = -12;
        double result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteTres() {

        int[][] v = {{1, 3, 6}, {2, 7, 8}, {3 ,6 ,2}};
        double expected = -28;
        double result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteQuatro() {

        int[][] v = {{1, 2, 3}, {4, 5, 6}, {7, 8}};
        Integer expected = null;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }
    @Test
    void obterDeterminanteDeMatrizTesteCinco() {

        int[][] v = {{1, 2, 3}, {4, 5}, {7, 8}};
        Integer expected = null;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteSeis() {

        int[][] v = {{1, 2, 3}, {4, 5, 6}, {7, 8,4,5}};
        Integer expected = null;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteSete() {

        int[][] v = {{1,2},{4,5}};
        Integer expected = -3;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }

    @Test
    void obterDeterminanteDeMatrizTesteOito() {

        int[][] v =null;
        Integer expected = null;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);

    }
    @Test
    void obterDeterminanteDeMatrizTesteNove() {
        int[][] v ={{}};
        Integer expected = null;
        Integer result = ExercicioDezasseis.obterDeterminantePorLaplace(v);
        assertEquals(expected,result);
    }








}