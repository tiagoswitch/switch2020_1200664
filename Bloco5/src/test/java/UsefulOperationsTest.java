import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class UsefulOperationsTest {



    @Test
    void countDigits() {
        int n = 1234;
        int expected = 4;
        int result = UsefulOperations.countDigits(n);
        assertEquals(expected,result);
    }


    @Test
    void countEvenDigitsTestOne() {
        int n = 1;
        int expected=0;
        int result = UsefulOperations.countEvenDigits(n);

        assertEquals(expected,result);

    }

    @Test
    void countEvenDigitsTestTwo() {
        int n = 124;
        int expected=2;
        int result = UsefulOperations.countEvenDigits(n);
        assertEquals(expected,result);
    }

    @Test
    void countOddDigitsTestOne() {

        int n = 124115;
        int expected=4;
        int result = UsefulOperations.countOddDigits(n);
        assertEquals(expected,result);

    }

    @Test
    void countOddDigitsTestTwo() {
        int n = 224;
        int expected=0;
        int result = UsefulOperations.countOddDigits(n);
        assertEquals(expected,result);
    }

    @Test
    void getAvgOfNumberOfDigitsTestEmptyArray() {
        int [] array= {};
        double expected =0;
        double result = UsefulOperations.getAvgOfNumberOfDigits(array);
        assertEquals(expected,result,0.1);
    }

    @Test
    void getAvgOfNumberOfDigitsTestArrayLengthOne() {
        int [] array= {1};
        double expected =1;
        double result = UsefulOperations.getAvgOfNumberOfDigits(array);
        assertEquals(expected,result,0.1);
    }

    @Test
    void getAvgOfNumberOfDigitsTestOne() {
        int [] array= {1,123,12};
        double expected =2;
        double result = UsefulOperations.getAvgOfNumberOfDigits(array);
        assertEquals(expected,result,0.1);
    }









}