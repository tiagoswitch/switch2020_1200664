package Sudoku;

public class SudokuCell {
    private final boolean isFixed;
    private int value;


    public SudokuCell() {
        this.value = 0;
        this.isFixed = false;
    }

    public SudokuCell(int value, boolean isFixed) {
        if (!isValidNumber(value)) {
            throw new IllegalArgumentException(" Número inválido para jogar sudoku");
        }
        this.value = value;
        this.isFixed = isFixed;
    }


    /**
     * Método para ver se o valor é não editável
     *
     * @return True se é fixo
     */
    public boolean isFixed() {
        return isFixed;
    }


    /**
     * Método para verificar se o número a adicionar é válido
     *
     * @param element
     * @return
     */
    public boolean isValidNumber(int element) {
        return (element >= 0 && element <= 9);
    }

    /**
     * Método para modificar um valor no sudoku
     * Se o número for válido e se não estiver fixo, faz a alteração
     *
     * @param number
     */
    public void changeAValue(int number) {
        if (isValidNumber(number) && !isFixed()) {
            this.value = number;
        }
    }

    public int getValue() {
        return this.value;
    }

    /**
     * Método para limpar o valor, da célula do jogo
     */
    public void clear() {
        if (!isFixed) {
            this.value = 0;
        } else {
            throw new IllegalArgumentException("Unable to clear. The cell is fixed");
        }
    }


}
