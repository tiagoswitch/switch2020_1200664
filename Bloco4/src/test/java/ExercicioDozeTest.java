import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void contarSeLinhaseColunasTemOMesmoTam() {
        int [][]v = {{},{}};
        int expected = 0;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }

    @Test
    void contarSeLinhaseColunasTemOMesmoTamTesteDois() {
        int [][]v = {{1,2},{1}};
        int expected = -2;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }

    @Test
    void contarSeLinhaseColunasTemOMesmoTamTesteTres() {
        int [][]v = {{1,2},{1,2}};
        int expected = 2;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }

    @Test
    void contarSeLinhaseColunasTemOMesmoTamTesteQuatro() {
        int [][]v = {{1,2,3},{1,2,3},{1,2,3}};
        int expected = 3;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }

    @Test
    void contarSeLinhaseColunasTemOMesmoTamTesteCinco() {
        int [][]v = {{1,2,3,4},{1,2,3},{1,2,3}};
        int expected = -2;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }

    @Test
    void contarSeLinhaseColunasTemOMesmoTamTesteSeis() {
        int [][]v = {{1,2,3,4},{1,2,3,4},{1,2,3}};
        int expected = -2;
        int result = ExercicioDoze.contarSeLinhaseColunasTemOMesmoTam(v);
        assertEquals(expected,result);

    }




}