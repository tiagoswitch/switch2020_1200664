package com.company;

public class Bloco3Ex12 {

    public static void main(String[] args) {

    System.out.println(formulaResolvente(1,-5,6));
    }

    public static String formulaResolvente(double a, double b, double c)
    {
        double intervalo1=0;
        double intervalo2=0;
        double delta = Math.abs(Math.pow(b,2)) - 4 * a *c;

        if(a == 0)  return "A equação não é de segundo grau";
        else if (delta < 0 )
        {
             return "A Equação não tem soluções reais";
        }
        else if(delta ==0) return "A equação tem raíz dupla que é "+(-b)/(2*a)+"";
        else {

            intervalo1 = (-b - Math.sqrt(delta))/ 2*a;
            intervalo2 = (-b + Math.sqrt(delta))/2*a;

            return "As soluções são " +intervalo1+ " e "+intervalo2+".";
        }
    }


}
