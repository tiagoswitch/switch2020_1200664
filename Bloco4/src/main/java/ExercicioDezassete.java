public class ExercicioDezassete {

    public static void main(String[] args) {


    }

    public static double [][] obterMatrizMultPorConst(double [][]v , double c)
    {
        if(!(ExercicioQuinze.isValidMatrix(v)) || v.length==0)
        {
            return null;
        }
        else {
            double [][] r = new double[v.length][v[0].length];
            for (int i = 0; i <v.length ; i++) {
                for (int j = 0; j <v[i].length ; j++) {
                    r[i][j] = (v[i][j] * c);
                }

            }
            return r;
        }
    }

    public static double [][] obterSomaDeMatrizes(double [][]m1, double [][]m2)
    {
        double [][] resultado= null;
        if(!(ExercicioQuinze.isValidMatrix(m1))|| !(ExercicioQuinze.isValidMatrix(m2)) || m1.length==0 || m2.length == 0)
        {
            return resultado;
        }
        else if(m1.length != m2.length || m1[0].length!=m2[0].length)
        {
            return resultado;
        }
        else
        {
            resultado = new double [m1.length][m1[0].length];
            for (int i = 0; i <m1.length ; i++) {
                for (int j = 0; j <m1[0].length ; j++) {

                    resultado[i][j]= m1[i][j]+ m2[i][j];
                }
            }
            return resultado;
        }
    }

    public static double [][] obterMultiplicacaoDeMatrizes (double [][]m1, double [][]m2)
    {
        double [][] matrizMultiplicada = null;
        if(!(ExercicioQuinze.isValidMatrix(m1))|| !(ExercicioQuinze.isValidMatrix(m2))){
            return matrizMultiplicada;
        }
        else if(m1.length != m2[0].length || m1[0].length != m2.length|| m1.length==0){
            return matrizMultiplicada;
        }
        else {
            matrizMultiplicada = new double [m1.length][m2[0].length];
            for (int i = 0; i < m1.length; i++) { // iterador para as linhas de m1
                for (int j = 0; j <m2[0].length; j++) { // iterador para as colunas  de m2
                    matrizMultiplicada[i][j]=0;
                    for (int k = 0; k < m2.length ; k++) { // iterador para as linhas de m2
                        matrizMultiplicada[i][j] += m1[i][k] * m2[k][j];
                    }
                }
            }
            return matrizMultiplicada;
        }
    }
}
