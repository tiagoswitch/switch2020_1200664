import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrianguloTest {

    @Test
    void getHipotenusaTesteUmCasoDimInvalidas() {

        int width = -1;
        int height= 6;
        assertThrows(IllegalArgumentException.class, () -> {
            new Rectangulo(width, height);
        });
    }

    @Test
    void getHipotenusaTesteDoisCasoDimInvalidas() {

        int width = 5;
        int height= -1;
        assertThrows(IllegalArgumentException.class, () -> {
            new Rectangulo(width, height);
        });
    }

    @Test
    void getHipotenusaTesteDois() {

        int cateto1 = 3;
        int cateto2 = 4;
        double expected =5;
        Triangulo triangulo1 = new Triangulo(cateto1,cateto2);
        double result = triangulo1.getHipotenusa();
        assertEquals(expected,result);

    }





}