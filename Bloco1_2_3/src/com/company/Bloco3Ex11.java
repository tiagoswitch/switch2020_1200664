package com.company;

public class Bloco3Ex11 {

    public static void main(String[] args) {
        int n = 10;
        String s = devolveManeirasdeObterUmNum(n);
        System.out.println(s);
    }

    public static String devolveManeirasdeObterUmNum(int n)
    {
        int cont = 0;
        String resultado = "";
        if (n < 1 || n > 20) return "Número inválido";
        for (int i = 0; i < 10; i++) {
                for (int j = 10; j >= 0; j--) {
                    if (i + j == n && i <= j) {
                        cont++;
                        resultado = resultado + i + "+" + j + ",";
                    }
                }
            }
            return "Existem " + cont + " maneiras " + resultado + "";
        }

}