import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void isCapicua() {
        int n = 121;
        boolean expected = true;
        boolean result = ExercicioNove.isCapicua(n);
        assertEquals(expected,result);

    }

    @Test
    void isCapicuaTesteDois() {
        int n = -4;
        boolean expected = false;
        boolean result = ExercicioNove.isCapicua(n);
        assertEquals(expected,result);

    }

    @Test
    void isCapicuaTesteTres() {
        int n = 122;
        boolean expected = false;
        boolean result = ExercicioNove.isCapicua(n);
        assertEquals(expected,result);

    }


}