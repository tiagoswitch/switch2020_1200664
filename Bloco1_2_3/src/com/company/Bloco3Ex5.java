package com.company;

public class Bloco3Ex5 {

    public static void main(String[] args) {

        /*
        int inicio =1;
        int fim =7;
        int n=2;
        double s = produtoDeMultiplosDeUmIntervalo(inicio,fim,n);
        System.out.println(s);


         */
    }

    public static double somaDePares(int inicio, int fim) {
        double soma=0;


        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }

        for (int i = inicio; i <= fim; i++) {
            if (i % 2 == 0) {
                soma= soma + i;
            }
        }
        return soma;

    }

    public static double nDePares(int inicio, int fim)
    {
        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }

        double cont =0;
        for (int i = inicio; i <= fim; i++) {
            if (i % 2 == 0) {
                cont++;
            }
        }
        return cont;

    }

    public static double somaDeImpares(int inicio, int fim) {

        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }

        double soma=0;

        for (int i = inicio; i <= fim; i++) {
            if (i % 2 != 0) {
                soma= soma + i;
            }
        }
        return soma;

    }

    public static double nDeImpares(int inicio, int fim)
    {
        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }

        double cont =0;


        for (int i = inicio; i <= fim; i++) {
            if (i % 2 != 0) {
                cont++;
            }
        }
        return cont;

    }

    public static double somaDeMultiplosDeUmIntervalo(int inicio, int fim,int n) {

        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;
        }
        double soma=0;
        for (int i = inicio; i <= fim; i++) {
            if (i % n ==0) {
                soma= soma + i;
            }
        }
        return soma;
    }

    public static double produtoDeMultiplosDeUmIntervalo(int inicio, int fim,int n) {

        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }

        double produto = 1;
        int cont =0;
        for (int i = inicio; i <= fim; i++) {
            if (i % n ==0) {
                cont++;
                produto= produto * i;
            }

        }
        if(cont <1) produto =0;
        return produto;
    }

    //alinea g)
    public static double mediaDeMultiplosDeUmIntervalo(int inicio, int fim,int n) {
        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }
        double soma=0;
        double cont =0;
        double media =0;

        for (int i = inicio; i <= fim; i++) {
            if (i % n ==0) {
                soma = soma +i;
                cont++;
            }
            media = soma/cont;
        }
        if(cont==0) media =0;

        return media;
    }


    public static double mediaDeDeDoisMultiplosNumIntervalo(int inicio, int fim,int n, int m) {

        if(inicio >fim)
        {
            int temp=0;
            // Estou a atribuir á 1º o valor da 2º
            temp=fim;
            fim = inicio;
            inicio =temp;

        }
        double soma=0;
        double cont =0;
        double media =0;

        for (int i = inicio; i <= fim; i++) {
            if (i % n ==0 || i % m ==0) {
                soma = soma +i;
                cont++;
            }
            media = soma/cont;
        }
        if(cont <1) media =0;
        return media;
    }
























}
