package com.company;

public class Bloco3Ex19 {

    public static void main(String[] args) {

        int n= organizarParesADireita(27654);
        System.out.println(n);

    }

    public static int organizarParesADireita(int n) {
        int pares=0, impares=0,digito = 0,cont=0;
        double resultado;
        while (n > 0)
        {
            digito = n%10;
            if (digito % 2 == 0) {
                pares = pares * 10 + n % 10;
                cont++;
            }
            else impares = impares*10 + n%10;
            n=n/10;
        }
        /*  Outra forma de resolver com string para int
        String p =""+pares;
        String imp= ""+impares;
        int r= Integer.parseInt(p+imp);

        */
        if(cont ==0)resultado=impares;
        resultado= impares*(Math.pow(10,cont))+pares;
        return (int)resultado;

    }

}
